setpath;

filename = '..\models\test_angmom.xml';
engines={'havok','ODE'};
ngn(filename, engines);

%%

T = util_parse(filename);

%%
close all
mjplot
figdata = getappdata(findobj('tag','mjplot'));
handles = figdata.handles(1);

overlay = [100 0.97];

animate(T(2), handles, 10, overlay);

util_savepdf(gcf,'-PNG')

