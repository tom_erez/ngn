function outdir = util_outdir(filename)

[~, name, ~] = fileparts(filename);
outdir = ['C:\temp\output\' name '\'];