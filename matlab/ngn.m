function success = ngn(filename, engine)

outdir = util_outdir(filename);

if nargin<2
    engine={[]};
end

if isa(engine,'char')
	engine = {engine};
end

mj('load',which(filename));

if isempty(ls(outdir))
    mkdir(outdir);
end

if ~isempty(dir([outdir '*.dat']))
    system(['del ' outdir '*.dat']);
end

failure = zeros(1, length(engine));
for i=1:length(engine)
	failure(i) = system(['..\bin\main.exe ' which(filename) ' ' engine{i}]);
    %failure(i) = system([' start /affinity 2 ..\bin\main.exe ' filename ' ' outdir ' ' engine{i}]);
	if failure(i)
		warning([engine{i} ' simulation failed']);
	end	
end

disp('finished all simulations')

success =  ~any(failure);