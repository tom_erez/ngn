function fig = plot_timing(T)

set(0, 'DefaultAxesFontName', 'Times New Roman',...
	'DefaultUIControlFontName','Times New Roman')

set(0,'defaultUicontrolFontSize',12,...
'defaultAxesFontSize',14,...
'defaultTextFontSize',14)



[nEng, ~, ~] = size(T);

timing = zeros(nEng,1);
for i=1:nEng
	timing(i) = 1e-3./mean(T(i,1,1).CPUtime);
end

model = mj('getmodel');
name = strrep(model.model_name,'_',' ');
fig = util_newfigure(['timing ' name]);

for i=1:nEng
	h = barh(i,timing(i));
	hold on
	[color, style] = engine_line_style(T(i,1,1).engine);
	
	set(h,'facecolor',color,'linestyle',style);

end
set(gca,'yticklabel',[])
set(gca,'tickdir','out',...
	'LineWidth',.7,'PlotBoxAspectRatio',[1 1 1],...
                    'DataAspectRatioMode','auto',...
					'ActivePositionProperty','position')
				
set(gca,'ylim',[0 nEng+1],'ytick',(1:nEng),...
	'ydir','reverse','xgrid','on','GridLineStyle','-')

xlabel('thousand evaluations/sec (kHz)')
xlim = get(gca,'xlim');
for i=nEng:-1:1
 	[color, style] = engine_line_style(T(i,1,1).engine);
	text(xlim(2)*.95, i, [T(i,1,1).engine ': ' num2str(round(timing(i)*10)/10)], ...
		'edgecolor',color,'linestyle',style,...
		'BackgroundColor','w','fontname','Times New Roman',...
		'horizon','right')
end


title(['\bf' name]);

set(0,'defaultUicontrolFontSize',8,...
'defaultAxesFontSize',10,...
'defaultTextFontSize',10)