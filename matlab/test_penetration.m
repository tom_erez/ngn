setpath;
filename = '..\models\test_penetration.xml';

ngn(filename);

%% parse
T = util_parse(filename);


[nEng, nDt, ~] = size(T);
% vector of dt values (millisecons)
DT = 1000*cellfun(@(x) mean(diff(x,1)), {T(1,:,1).time});

% matrix of dt Values (per engine)
DTM = DT(1:end)'*ones(1,nEng);


% % get Y-axis values
% YY = zeros(nEng, nDt);
% for i=1:nEng
%     for j=1:nDt
% 		Vz  = tt(T(i,j).xpos(:,3,:), 1./T(i,j).xpos(:,3,1));
% 		YY(i,j) = sum(Vz);
%     end
% end
% Y = nanmean(YY,3)';

%% animate


% animateAll(T(:), 1)


%% plot normalized position traces
util_newfigure('4balls')
clf

[nEng, nVals] = size(T);
for i=1:nEng
    subplot(3,3,i);
    cla
    hold on
    for j=1:nVals
		V = T(i,j).xpos(:,3,:);
		V = squeeze(sum(V,1));
        a = (j-1)/(nVals-1);
        plot(T(i,j).time, V,'.','color',[1 0 0]*a +[0 1 0]*(1-a),'markersize',5);
    end
    title(T(i,1).engine)
%     set(gca,'ylim',[0.9 1.1 ])
	set(gca,'xlim',round(T(i,j).time([1 end])))
	grid on
% 	prettify(gca)
end

%% plot penetration traces
% util_newfigure('penetration')
% clf
% 
% [nEng, nVals] = size(T);
% for i=1:nEng
%     subplot(3,3,i);
%     cla
%     hold on
%     for j=nVals
% 		V = -min(0, T(i,j).xpos(:,3,:)-.1);
% 		V = squeeze(sum(V,1));
%  		plot(T(i,j).time, -T(i,j).penetration,'-','color','b','markersize',5);
%     end
%     title(T(i,1).engine)
% %     set(gca,'ylim',[0.9 1.1 ])
% 	set(gca,'xlim',T(i,j).time([1 end]))
% 	grid on
% % 	prettify(gca)
% end