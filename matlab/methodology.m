setpath;

files = util_modelfiles('method');
% edit(files{:})

%% get data
engines = {'mujoco_euler','ode'};

for i=1:length(files)
	success = ngn(files{i},engines);
	if ~success
		warning(['simulation failed for model ' files{i}])
	end
end


%% parse
T = util_parse(files{1});


%% make plots

% change font?
set(0, 'DefaultAxesFontName', 'Times New Roman',...
	'DefaultUIControlFontName','Times New Roman', 'defaultlinelinewidth',1.6)

mj('load',which(files{1}));
model = mj('getmodel');
model_name = model.model_name;

util_newfigure('Etraces');
[nEng, nDt, nSeg] = size(T);
grd = ceil(sqrt(nEng));

DT = 1000*cellfun(@(x) mean(diff(x,1)), {T(1,:,1).time});
ldt =  round(log2(DT));


mjcolor = engine_line_style('mujoco euler');


cla
hold on    

Tj = T(1,1,1).time;
Ejk  = T(1,1,1).energy;
h=[];
plot(Tj([1 end]), 0*Ejk([1 1]),'-','color','k','linewidth',1);

ixdt = [5 7 9];
style = {'-','--',':'};
h=[];
for j=1:length(ixdt)
	Tj = T(1,ixdt(j),1).time;
	a=j/nDt;
	Ejk  = T(1,ixdt(j),1).energy-T(1,ixdt(j),1).energy(1);
	h(end+1) = plot(Tj, Ejk,style{j},'color',mjcolor);
end

odecolor = engine_line_style('ode');
for j=1:length(ixdt)
	Tj = T(2,ixdt(j),1).time;
	a=j/nDt;
	Ejk  = T(2,ixdt(j),1).energy-T(2,ixdt(j),1).energy(1);
	h(end+1) = plot(Tj, Ejk,style{j},'color',odecolor);
end

2.^ldt(ixdt);
dts = {'1/2 ms', '2 ms', '8 ms'};
s1 = cellfun(@(s) ['MuJoCo ' s],dts,'UniformOutput',false);
s2 = cellfun(@(s) ['ODE ' s],dts,'UniformOutput',false);
legend(h,{ s1{:}, s2{:}},'location','SW')

% text

ylabel('energy (Joules)')
xlabel('time (seconds)')
set(gca,'ylim',[-0.1 .1 ],'xlim',[0 5],'xtick',(0:.5:5),...
	'ytick',(-.1:.05:.1))

% prettify
ax=gca;

set(ax,'YLimMode','manual','YTickMode','manual',...
	   'XLimMode','manual','XTickMode','manual')


set(ax,'tickdir','out',...
	'LineWidth',.7,'PlotBoxAspectRatio',[1 1 1],...
                    'DataAspectRatioMode','auto',...
					'ActivePositionProperty','position')

%# capture handle to current figure and axis
hFig = get(ax,'parent');


%# create a second transparent axis, as a copy of the first
ax2 = copyobj(ax,hFig);
delete( get(ax2,'Children') )
set(ax2, 'Color','none', 'Box','on',...
    'XGrid','off', 'YGrid','off')

%# show grid-lines of first axis, style them as desired,
%# but hide its tick marks and axis labels
set(ax, 'XColor',[0 .7 0], 'YColor',[0.9 0.9 0.9], ...
    'XMinorGrid','off', 'YMinorGrid','off', ...
	'Xgrid','on',...
	'GridLineStyle','-','MinorGridLineStyle','-', ...
    'XTickLabel',[], 'YTickLabel',[]);
xlabel(ax, ''), ylabel(ax, ''), title(ax, '')



%% save PDFs

util_savepdf(1)

