setpath;

filename = '..\models\27boxes.xml';

engines = {'Bullet', 'mujoco_euler','MuJoCo_rk',...
	'ode','physx_cartesian','havok'};
%%

ngn(filename,engines);

%%

T = util_parse(filename);

%%

animateAll(T,.5)

%% plot timing
Tr = T([1 3:end]);

fig = plot_timing(Tr);

%% save
util_savepdf(fig)