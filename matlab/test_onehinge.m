setpath;

filename = '..\models\test_onehinge.xml';

%%
ngn(filename);

%% parse
T = util_parse(filename);
% remove last timestep ???WHY???
[nEng, nDt, ~] = size(T);
T = T(:,1:nDt-1,:);
[nEng, nDt, ~] = size(T);

% remove engines
ignoreEngine = [2 4 5];
whichEngine = true(1,nEng);
whichEngine(ignoreEngine) = false;
if ~isempty(ignoreEngine)
	fprintf('ignoring engine: %s \n',T(~whichEngine,1,1).engine)
end
T = T(whichEngine,:,:);
[nEng, nDt, ~] = size(T);
%%


% vector of dt values (millisecons)
DT = 1000*cellfun(@(x) mean(diff(x,1)), {T(1,:,1).time});

% matrix of dt Values (per engine)
DTM = DT(1:end)'*ones(1,nEng);


% get Y-axis values
YY = zeros(nEng, nDt);
for i=1:nEng
    for j=1:nDt
		dE  = T(i,j).xvel(:,3,:)-1;
		YY(i,j) = mean(abs(dE(:)));
    end
end
Y = nanmean(YY,3)';


whichEngine = true(1,nEng);

engList = {T(:,1,1).engine};
%%

figs(1) = util_newfigure('1hinge');
% set(gcf, 'color', 'w', 'pos', 50+[0 50 400 400]);

h= loglog(DTM , Y,...
	'.-','markersize',9);
set(gca,'Ydir','reverse')
hold on
hmarker1 = loglog(DTM([1 1],:),Y([1 1],:) ,'o','markersize',6);
hmarkerend = loglog(DTM([end end],:), Y([end end],:) ,'^','markersize',6);

for i=1:length(h)
	
	[color, style, width, marker] = engine_line_style(T(i,1,1).engine);
	
	set(h(i),'color',color,'linestyle',style);
	set(hmarker1(i),'color',color);
	set(hmarkerend(i),'color',color);
end

hmarker = [hmarker1; hmarkerend];
set(hmarker,'MarkerFaceColor','auto')

axis square

dtrange = round(log2(DTM([1 end],1))); 
title(sprintf('{\\bf %s}, dt = 1/%d .. %d  ms',...
	'one hinge',2^(-dtrange(1)),2^dtrange(2)))

ylabel 'velocity conservation (rad/sec)'
xlabel 'timestep (milliseconds)'
legend(engList{whichEngine}, 'location','N')
grid on
set(gca,'xlim', [DTM(1,1)/2 DTM(end,1)*2])
xt = DTM(:,1);
set(gca,'xtick', xt)
xl1 = arrayfun(@(n)['1/' num2str(1/n)] ,xt(xt<.9),'UniformOutput',false);
xl2 = arrayfun(@(n) num2str(n) ,xt(xt>=.9),'UniformOutput',false);
set(gca,'xticklabel', [xl1;xl2])
% ylim = get(gca,'ylim');
% set(gca,'ylim', [ylim(1) 1+0.02*(1-ylim(1))])

prettify(gca)

drawnow

% plot velocity traces
% figure(11)
% util_newfigure('1hinges')
% clf
% 
% [nEng, nVals] = size(T);
% for i=1:nEng
%     subplot(3,3,i);
%     cla
%     hold on
%     for j=1:nVals
%         V = T(i,j).xvel;
%         V = squeeze(V(:,3,:));
%         a = (j-1)/(nVals-1);
%         plot(T(i,j).time, V,'-','color',[1 0 0]*a +[0 1 0]*(1-a),'markersize',5);
%     end
%     title(T(i,1).engine)
%     set(gca,'ylim',[0.9 1.1 ])
% 	set(gca,'xlim',T(i,j).time([1 end]))
% 	prettify(gca)
% end


