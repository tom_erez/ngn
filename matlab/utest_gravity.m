setpath;

filename = '..\models\utest_gravity.xml';
outdir = 'C:\temp\output\gravity\';

success = ngn(filename, outdir);

%% parse
if success
    T = parseAll(outdir, mj('size'));
    [nE, nDt] = size(T);
end




%% plot zpos
figure(11)
clf

xpos = [T.xpos];
xpos = squeeze(xpos(3,:,:));

plot(T(1).time, xpos','o-')

legend({T.engine})