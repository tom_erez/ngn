setpath;

files = {'..\models\test_planar.xml'
	'..\models\27boxes.xml'
	'..\models\test_grasp.xml'
	'..\models\test_humanoid.xml'};

%% run experiments
for i=1:length(files)
	success = ngn(files{i});
	if ~success
		warning(['simulation failed for model ' files{i}])
	end
end

%% parse
TT = cell(0,0);
for i=1:length(files)
	TT{i} = util_parse(files{i});
end


%% plot timing

for i=1:length(files)
	mj('load',which(files{i}));
	T = TT{i};
	T = T([1 2 3 4 6:end]);
	if i>1
		T = T([1 3:end]);
	end
	plot_timing(T);
end
