figure(3);
clf
for eng=1:size(T,1)
	subplot(size(T,1),1,eng);
	hold on
	for i=1:size(T,2)
		a = (i-1)/(size(T,2)-1);
		plot(T(eng,i).time, T(eng,i).penetration,'.','color',a*[1 0 0]+(1-a)*[0 0 1]);	
	end
	yl = get(gca,'ylim');
	set(gca,'ylim',[max(yl(1),-2) 0])
	title(T(eng,1).engine)
end