setpath;

filename = '..\models\utest_limits.xml';

engines = {'mujoco_euler','bullet_articulated'};

success = ngn(filename, engines);

%% parse
T = util_parse(filename);


%% animate

animateAll(T, 0.1)

