setpath;

filename = '..\models\utest_asteroid.xml';
outdir = 'C:\temp\output\asteroid\';

success = ngn(filename, outdir);

%% parse
if success
    T = parseAll(outdir, mj('size'));
    [nE, nDt] = size(T);
end


%% animate

animateAll(T(:,1), 1)

%% plot angular momentum
figure(14)
clf


for i=1:nE
    subplot(3,3,i);
    cla
    hold on
    for j=1:nDt
        a = (j-1)/(nDt-1);
        plot(T(i,j).time, T(i,j).momentum(1:3,:)','-','color',[0 1 0]*a +[0 0 1]*(1-a));
    end
    title(T(i,1).engine)
    set(gca,'ylim',[0 100],'xlim',[0 5])
end


%% plot energy
figure(15)
clf


for i=1:nE
    subplot(3,3,i);
    cla
    hold on
    for j=1:nDt
        a = (j-1)/(nDt-1);
        plot(T(i,j).time, log(T(i,j).energy)','-','color',[0 1 0]*a +[0 0 1]*(1-a));
    end
    title(T(i,1).engine)
    set(gca,'ylim',[4 8])
end