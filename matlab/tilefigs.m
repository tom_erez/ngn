function tilefigs(figs,monitor)

if nargin == 0
   figs           = sort(get(0,'Children'));   % locate all open figure handles
end

monitors       = get(0,'mon');
if nargin < 2
   [~,m]    = min(monitors(:,1));
   screen   = monitors(m,:);
else
   screen   = monitors(monitor,:);   
end

K              = numel(figs);        % number of figures

screen([2 4])  = screen([2 4])-screen(2)+1; % fix y-coordinate (Matlab bug?)
width          = screen(3)-screen(1);
height         = screen(4)-screen(2);

border  = 0;
space   = 0;

r        = max(1,round(sqrt(K)) + (-2:2));
R        = r'*r;
R(R < K) = inf;
[i,j]    = find(R<inf);

rr    = [r(i); r(j)];

[~,k] = min( max(rr,[],1)*0.75 + min(rr,[],1) );

rows  = min(rr(:,k));
cols  = max(rr(:,k));

side    = floor(min((width-2*border)/cols,(height-2*border)/rows))-space;

step    = side + space;

for j = 1:rows
   for i = 1:cols
      k  = i + (j-1)*cols;
      if k <= K
         xpos  =  border + screen(1) + (i-1)*step;
         ypos  = -border -side + screen(4) - (j-1)*(step);
         figure(figs(k))
         set(figs(k),'Outer',[ xpos ypos side side ]); % move figure
      end
   end
end   


%% make test windows
% for i = 1:14
%    figure(i);
%    set(gcf,'ToolBar','none','menu','none');
% end
