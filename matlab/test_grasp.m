setpath;

filename = '..\models\test_grasp.xml';

engines = {'bullet','bulletMB', 'mujoco_euler'};
% engines = {'mujoco_euler','MuJoCo_rk'};
%%
ngn(filename,engines);


%%

T = util_parse(filename);

%%

animateAll(T,.02)

%% plot timing
Tr = T([1 3:end]);

fig = plot_timing(Tr);

%% save
% util_savepdf(fig)

%% distance between hand and object

util_newfigure('object drop')
clf

[nEng, nVals] = size(T);

G = zeros(nEng,nVals);

for i=1:nEng
    subplot(3,3,i);
    cla
    hold on
    for j=1:nVals
		V = T(i,j).xpos([end-1 end],:,:);
		V = squeeze(sqrt(sum((V(1,:,:)-V(2,:,:)).^2,2)));
        a = (j-1)/(nVals-1);
        plot(T(i,j).time, min(V,1),'.','color',[1 0 0]*a +[0 1 0]*(1-a),'markersize',5);
		if any(V > .2)
			G(i,j) = 1;
		end
		if any(T(i,j).time(2:end) == 0)
			G(i,j) = 2;
		end
    end
    title(T(i,1).engine)
    set(gca,'xlim',[0 2 ])
% 	set(gca,'xlim',round(T(i,j).time([1 end])))
	grid on
% 	prettify(gca)
end
%%
g=[];
for i =1:nEng
	g(i) = DT(find(G(i,:),1,'first'));
	disp([T(i,1).engine '  ' num2str(g(i))])
end




