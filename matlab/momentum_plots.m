function figs = momentum_plots(T, model_name, ignoreEngine, mom_axes)


engList = {T(:,1,1).engine};

[nEng, nDt, nSeg] = size(T);

% remove engines
whichEngine = true(1,nEng);
whichEngine(ignoreEngine) = false;
if ~isempty(ignoreEngine)
	fprintf('ignoring engine: %s \n',T(~whichEngine,1,1).engine)
end
T = T(whichEngine,:,:);

% remove last timestep ???WHY???
T = T(:,1:nDt-1,:);

[nEng, nDt, ~] = size(T);

% vector of dt values (millisecons)
DT = 1000*cellfun(@(x) mean(diff(x,1)), {T(1,:,1).time});

% matrix of dt Values (per engine)
DTM = DT(1:end)'*ones(1,nEng);


% get Y-axis values
LL = zeros(nEng, nDt, nSeg, 2); % 4th dim is momentum type
for i=1:nEng
    for j=1:nDt
        for k=1:nSeg
			mom = T(i,j,k).momentum;
			dmom = mom(:,1)-mom(:,end);
			LL(i,j,k,1)  = sqrt(sum(mom_axes.*dmom(1:3)).^2);
			LL(i,j,k,2)  = sqrt(sum(dmom(4:6).^2));
        end
    end
end
L = nanmean(LL,3);

% get x faster than real time
xFaster = zeros(nEng, nDt, nSeg);
for i=1:nEng
    for j=1:nDt
        for k=1:nSeg
            Dt = T(i,j,k).time(end) - T(i,j,k).time(1);
            xFaster(i,j,k) = Dt / sum(T(i,j,k).CPUtime);
        end
    end
end

X = mean(xFaster,3);

% transpose (for graphics)
L = permute(L,[2 1 3 4]);
X = X';


% figures

type = {'angular', 'linear'};
units = {'N{\cdot}m{\cdot}s', 'N{\cdot}s'};

figs = [0 0 0 0];

for k =1:2
	
% 	figs(2*k-1) = util_newfigure([model_name '_' type{k} '_momentum']);
% 
% 	h = loglog(DTM , L(:,:,:,k),'.-');
% 	hold on
% 	hmarker1 = loglog(DTM([1 1],:),L([1 1],:,:,k) ,'o');
% 	hmarkerend = loglog(DTM([end end],:), L([end end],:,:,k) ,'^');
% 	
% 	for i=1:length(h)
% 		set(h(i),'color',CC(i,:));
% 		set(hmarker1(i),'color',CC(i,:));
% 		set(hmarkerend(i),'color',CC(i,:));
% 	end	
% 	
% 	hmarker = [hmarker1; hmarkerend];
% 	set(hmarker,'MarkerFaceColor','auto')
% 	set(gca,'xlim', [DTM(1,1)/2 DTM(end,1)*2])
% 
% 	dtrange = round(log2(DTM([1 end],1))); 
% 	title(sprintf('%s momentum:{\\bf %s}, dt = 1/%d .. %d  ms',...
% 		type{k}, model_name,2^(-dtrange(1)),2^dtrange(2)))
% 	
% 	ylabel([type{k} ' momentum (' units{k} ')'])
% 	xlabel 'timestep (milliseconds)'
% 	
% 	legend(engList{whichEngine}, 'location','SE')
% 	grid on
% 	set(gca,'xlim', [DTM(1,1)/2 DTM(end,1)*2])
% 	set(gca,'xticklabel', get(gca,'xtick'))
% 
% 	prettify(gca)
% 
% 	drawnow

	figs(k) = util_newfigure([type{k} '_momentum_pareto' '_' model_name]);

	dtrange = round(log2(DT([1 end]))); % integer log-two-milliseconds dt range.

	h = loglog(X, L(:,:,:,k) ,'.-');
	set(gca,'ydir','reverse')
	hold on
	hmarker1 = loglog(X([1 1],:),L([1 1],:,:,k) ,'o');
	hmarkerend = loglog(X([end end],:), L([end end],:,:,k) ,'^');
	
	for i=1:length(h)
		[color, style, width, marker, msize] = engine_line_style(T(i,1,1).engine);
		set(h(i),'color',color,'linestyle',style,'linewidth',width,'marker',marker,'markersize',msize);
		set(hmarker1(i),'color',color);
		set(hmarkerend(i),'color',color);
	end

	
	hmarker = [hmarker1; hmarkerend];
	set(hmarker,'MarkerFaceColor','auto')
	grid on

	ylabel([type{k} ' momentum drift (' units{k} ')'])
	xfasterlabel

	hL=legend(h, {T(:,1,1).engine},	'location','best');
	set(findobj(hL,'-property','fontsize'),'fontsize',8,'fontname','helvetica');
	
% 	title(sprintf('%s momentum:{\\bf %s}, dt = 1/%d .. %d  ms',...
% 		type{k}, model_name,2^(-dtrange(1)),2^dtrange(2)))
	title(sprintf('{\\bf %s}',model_name))	
	
	% ylim = get(gca,'ylim');
	% set(gca,'ylim', [ylim(1) 1+0.02*(1-ylim(1))])

	speedarrow;
	accuracyarrow;	
	
	prettify(gca)


	
	drawnow

end


%% momentum traces
% 
% util_newfigure(['etraces_' filename]);
% 
% grd = ceil(sqrt(nEng));
% for i=1:nEng
%     subplot(grd,grd,i);
%     cla
%     hold on    
%     for j=nDt:-1:1
%         Tj = Tw(i,j,1).time;
%         a=j/nDt;
% 
% 		for k=1:2
% 			mom = cat(3, Tw(i,j,:,k).momentum(:,:));
% 		Ejk  = sqrt(sum(.^2,1));
% 		plot(Tj, Ejk,'.-','color',[1 0 0]*a +[0 0 1]*(1-a));
% 
%     end
%     title(Tw(i,1).engine)
% %     set(gca,'ylim',[0 2 ])
% end