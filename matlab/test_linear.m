setpath;
outdir = 'C:\temp\output\';
filename = '..\models\test_linear.xml';

engines = {'mujoco_euler','bullet_articulated'};

success = ngn(filename, engines);


%% parse
T = util_parse(filename);

animateAll(T, 1)
%% animate5
figure(2)
plot(squeeze(T(1).xvel(3,end,:)))
hold on
plot(squeeze(T(2).xvel(3,end,:)),'r')
title vel
%%
figure(6)
plot(squeeze(T(1).gxmat(end,5,:)))
hold on
plot(squeeze(T(2).gxmat(end,5,:)),'r')
title pos
%%
animateAll(T, 1)