function figs = consistency_plots(T, model_name, ignoreEngine)

engList = {T(:,1,1).engine};

[nEng, nDt, nSeg] = size(T);

% remove engines
whichEngine = true(1,nEng);
whichEngine(ignoreEngine) = false;
if ~isempty(ignoreEngine)
	fprintf('ignoring engine: %s \n',T(~whichEngine,1,1).engine)
end
T = T(whichEngine,:,:);

% remove last timestep ???WHY???
T = T(:,1:nDt-1,:);

[nEng, nDt, ~] = size(T);

% vector of dt values (millisecons)
DT = 1000*cellfun(@(x) mean(diff(x,1)), {T(3,:,1).time});

% matrix of dt Values (per engine)
DTM = DT(1:end)'*ones(1,nEng);


% get Y-axis values
YY = zeros(nEng, nDt, nSeg);
for i=1:nEng
    for j=1:nDt
        for k=1:nSeg	
            dx = T(i,j,k).xpos(:,:,end) - T(i,1,k).xpos(:,:,end);
            YY(i,j,k) = sqrt(sum(mean(dx.^2,1),2));			
        end
    end
end
Y = nanmean(YY,3);

% get x faster than real time
xFaster = zeros(nEng, nDt, nSeg);
for i=1:nEng
    for j=1:nDt
        for k=1:nSeg
            Dt = T(i,j,k).time(end) - T(i,j,k).time(1);
            xFaster(i,j,k) = Dt / sum(T(i,j,k).CPUtime);
        end
    end
end

X = mean(xFaster,3);

% transpose (for graphics)
Y = Y';
X = X';

% remove reference (smallest) timestep
DT = DT(2:end);
DTM = DTM(2:end,:);
X = X(2:end,:);
Y = Y(2:end,:);

% eliminate divergent runs
for i=1:nEng
	for j=1:nDt-2
		if X(j,i)>X(j+1,i)
			X(j+1:end,i)=nan;
		end
	end
end


%{ 
% figure: consistency / timestep

figs(1) = util_newfigure(['consistency_' model_name]);
% set(gcf, 'color', 'w', 'pos', 50+[0 50 400 400]);

h = loglog(DTM , Y,'.-');
set(gca,'ydir','reverse')
hold on
hmarker1 = loglog(DTM([1 1],:),Y([1 1],:) ,'o','markersize',6);
hmarkerend = loglog(DTM([end end],:), Y([end end],:) ,'^','markersize',6);

for i=1:length(h)
	[color, style, width, marker, msize] = engine_line_style(T(i,1,1).engine);
	set(h(i),'color',color,'linestyle',style,'linewidth',width,'marker',marker,'markersize',msize);
	set(hmarker1(i),'color',color);
	set(hmarkerend(i),'color',color);
end

hmarker = [hmarker1; hmarkerend];
set(hmarker,'MarkerFaceColor','auto')

dtrange = round(log2(DTM([1 end],1))); 
title(sprintf('self-consistency:{\\bf %s}, dt = 1/%d .. %d  ms',...
	model_name,2^(-dtrange(1)),2^dtrange(2)))


ylabel 'discrepancy (meters)'
xlabel 'timestep (milliseconds)'
legend(engList{whichEngine}, 'location','SW')
grid on
set(gca,'xlim', [DTM(1,1)/2 DTM(end,1)*2])
xt = DTM(:,1);
set(gca,'xtick', xt)
xl1 = arrayfun(@(n)['1/' num2str(1/n)] ,xt(xt<1),'UniformOutput',false);
xl2 = arrayfun(@(n) num2str(n) ,xt(xt>=1),'UniformOutput',false);
set(gca,'xticklabel', [xl1;xl2])


% ylim = get(gca,'ylim');
% set(gca,'ylim', [ylim(1) 1+0.02*(1-ylim(1))])

prettify(gca)

drawnow

%}

%% pareto
figs = util_newfigure(['consistency_pareto_' model_name]);


dtrange = round(log2(DT([1 end]))); % integer log-two-milliseconds dt range.

h = loglog(X, Y ,'.-');
set(gca,'ydir','reverse')
hold on
hmarker1 = loglog(X([1 1],:),Y([1 1],:) ,'o','markersize',6);
hmarkerend = loglog(X([end end],:), Y([end end],:) ,'^','markersize',6);

for i=1:length(h)
	[color, style, width, marker, msize] = engine_line_style(T(i,1,1).engine);
	set(h(i),'color',color,'linestyle',style,'linewidth',width,'marker',marker,'markersize',msize);
	set(hmarker1(i),'color',color);
	set(hmarkerend(i),'color',color);
end

hmarker = [hmarker1; hmarkerend];
set(hmarker,'MarkerFaceColor','auto')
grid on

ylabel 'discrepancy (meters)'
xfasterlabel;

hL=legend(h, {T(:,1,1).engine},	'location','best');
set(findobj(hL,'-property','fontsize'),'fontsize',8,'fontname','helvetica');

% title(sprintf('{\\bf %s}, dt = 1/%d .. %d  ms',...
% 	model_name,2^(-dtrange(1)),2^dtrange(2)))

title(sprintf('{\\bf %s}',model_name))

ylim = get(gca,'ylim');
set(gca,'ylim', [ylim(1) min(10,ylim(2))])
axis square

speedarrow;
accuracyarrow;	
prettify(gca)

drawnow


%% energy traces
%{
util_newfigure(['etraces_' filename]);

grd = ceil(sqrt(nEng));
for i=1:nEng
    subplot(grd,grd,i);
    cla
    hold on    
    for j=nDt:-1:1
        Tj = T(i,j,1).time;
        a=j/nDt;
        for k=1:nSeg
            Ejk  = T(i,j,k).energy / T(i,j,k).energy(1);
            plot(Tj, Ejk,'.-','color',[1 0 0]*a +[0 0 1]*(1-a));
        end
    end
    title(T(i,1).engine)
    set(gca,'ylim',[0 2 ])
end
%}