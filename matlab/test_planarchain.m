setpath;

filename = '..\models\momentum_planar.xml';


ngn(filename,{'mujoco_euler','mujoco_rk'});

%%

T = util_parse(filename);

%%

animateAll(T,1)


%% momentum traces

util_newfigure('planar chain2');
[nEng, nDt, nSeg] = size(T);
grd = ceil(sqrt(nEng));
for i=1:nEng
    subplot(grd,grd,i);
    cla
    hold on    
    for j=nDt:-1:1
        Tj = T(i,j,1).time;
        a=j/nDt;

		mom = pp(T(i,j).momentum(1:3,:), -T(i,j).momentum(1:3,1));
		Ejk  = sqrt(sum(mom.^2,1));
		plot(Tj, Ejk,'.-','color',[1 0 0]*a +[0 0 1]*(1-a));

    end
    title(T(i,1).engine)
%     set(gca,'ylim',[0 .05 ])
end
%%
for i=1:nEng
    subplot(grd,grd,i);
    cla
    hold on    
    for j=nDt:-1:1
        Tj = T(i,j,1).time;
        a=j/nDt;

		mom = T(i,j).momentum(3,:);
		Ejk  = sqrt(sum(mom.^2,1));
		plot(Tj, Ejk,'.-','color',[1 0 0]*a +[0 0 1]*(1-a));

    end
    title(T(i,1).engine)
%     set(gca,'ylim',[0 .05 ])
end


