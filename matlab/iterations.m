setpath;

files = util_modelfiles('iterations');
% edit(files{:})

%% get data
for i=1:length(files)
	success = ngn(files{i},{'mujoco_euler','mujoco_rk','ODE','Bullet'});
	if ~success
		warning(['simulation failed for model ' files{i}])
	end
end


%% parse
TT = cell(0,0);
for i=1:length(files)
	TT{i} = util_parse(files{i});
end

%% make plots

% change font?
% set(0, 'DefaultAxesFontName', 'Times New Roman',...
% 	'DefaultUIControlFontName','Times New Roman')

% remove engines
% ignoreEngine = [2]; % bullet_A
ignoreEngine = [];

allfigs = [];
for i=1:length(files)
	T = TT{i};
	mj('load',which(files{i}));
	model = mj('getmodel');
	model_name = model.model_name;
	figs = iteration_plots(T, model_name, ignoreEngine);
	allfigs = [allfigs figs];
end


%% save PDFs

% util_savepdf(allfigs)

