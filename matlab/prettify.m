function prettify(ax)

% set(ax,'xticklabel', get(gca,'xtick'))
% set(ax,'yticklabel', get(gca,'ytick'))

set(ax,'YLimMode','manual','YTickMode','manual',...
	   'XLimMode','manual','XTickMode','manual')

   
set(ax,'XMinorGrid','off', 'YMinorGrid','off','XMinorTick','off', 'YMinorTick','off')

set(ax,'tickdir','out',...
	'LineWidth',.7,'PlotBoxAspectRatio',[1 1 1],...
                    'DataAspectRatioMode','auto',...
					'ActivePositionProperty','position')

%# capture handle to current figure and axis
hFig = get(ax,'parent');


%# create a second transparent axis, as a copy of the first
ax2 = copyobj(ax,hFig);
delete( get(ax2,'Children') )
set(ax2, 'Color','none', 'Box','on','XMinorTick','off', 'YMinorTick','off',...
    'XGrid','off', 'YGrid','off')

%# show grid-lines of first axis, style them as desired,
%# but hide its tick marks and axis labels
set(ax, 'XColor',[0.9 0.9 0.9], 'YColor',[0.9 0.9 0.9], ...
    'XMinorGrid','off', 'YMinorGrid','off',...
	'XGrid','on', 'YGrid','on',...
	'GridLineStyle','-','MinorGridLineStyle','-', ...
    'XTickLabel',[], 'YTickLabel',[]);
xlabel(ax, ''), ylabel(ax, ''), title(ax, '')

%# link the two axes to share the same limits on pan/zoom
% linkaxes([ax ax2], 'xy');

%# Note that `gca==hAx1` from this point on...
%# If you want to change the axis labels, explicitly use hAx2 as parameter.