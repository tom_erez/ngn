function T = util_parse(filename)

fprintf('parsing %s\n',filename)

% get sizes
mj('load',filename);
sz = mj('size');

% get files
outdir = util_outdir(filename);
files = dir([ outdir '*.dat']);
files = {files.name}';

if isempty(files)
	error(['no files found in ' outdir])
end


% get dimensions
if strfind(files{1},'-')
    getminus = @(s) s(1:strfind(s,'-')-1);
    getdot = @(s) s(strfind(s,'-')+1:strfind(s,'.')-1);
    engines = unique(cellfun(getminus, files, 'UniformOutput',false));
    nvals = numel(unique(cellfun(getdot, files, 'UniformOutput',false)));
    if numel(files) ~= nvals*numel(engines)
        error('some engines are missing result files')
    end
end

% parse
for i=1:length(files)
	fid = fopen([outdir files{i}], 'r');
	data = fread(fid,inf,'double');
	fclose(fid);
	if numel(data) ~= data(1)*data(2)+2
		error('data file does not conform to expected format')
	end
	data = reshape(data(3:end), [data(1) data(2)])';
    
    row = find(~cellfun('isempty',regexp(files{i},engines)));
	row = row(end); %hack for bullet/ bulletMB
    digits = files{i}(regexp(files{i},'\d'));
    if length(digits) == 2
        col = str2num(digits); %#ok<*ST2NM>
        page = 1;
    elseif length(digits) == 4
        col = str2num(digits(1:2));
        page = str2num(digits(3:4));
    else
       error('either 2 or 4 index digits expected') 
    end
        
    T(row,col,page)=parseTraj(data,sz); %#ok<*AGROW>
    T(row,col,page).engine = strrep(engines{row},'_',' '); % fix names with underscores
end


function T=parseTraj(traj, mjsz)

p12 = @(x) permute(x,[2 1 3 4 5]);

ngeom=mjsz.ngeom;
nbody=mjsz.nbody-1;
time = traj(:,1)';
gxpos = p12(reshape(traj(:,1+(1:ngeom*3))',3,ngeom,[]));
gxmat = p12(reshape(traj(:,1+ngeom*3+(1:ngeom*9))',9,ngeom,[]));
xpos = p12(reshape(traj(:,1+ngeom*12+(1:nbody*3))',3,nbody,[]));
xvel = p12(reshape(traj(:,1+ngeom*12+nbody*3+(1:nbody*6))',6,nbody,[]));
energy = traj(:,1+ngeom*3+ngeom*9+nbody*9+1)';
penetration = traj(:,1+ngeom*12+nbody*9+2)';
jointError  = traj(:,1+ngeom*12+nbody*9+3)';
momentum    = traj(:,1+ngeom*12+nbody*9+3+(1:6))';
CPUtime     = traj(:,1+ngeom*12+nbody*9+3+6+1)';
nc			= traj(:,1+ngeom*12+nbody*9+3+6+2)';

T = struct(...
'engine',		'',...    
'time',         time,...
'CPUtime',      CPUtime,...
'gxpos',		gxpos,...
'gxmat',		gxmat,...
'xpos',			xpos,...
'xvel',			xvel,...
'momentum',		momentum,...
'energy',		energy,...
'penetration',	penetration,...
'jointError',	jointError,...
'nc',	nc);