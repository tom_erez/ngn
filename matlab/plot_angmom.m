setpath;

filename = '..\models\test_angmom.xml';
engines={'havok','ODE'};
ngn(filename, engines);

%%

T = util_parse(filename);

%%
close all
for eng = 1:length(T)

	mjplot
	figdata = getappdata(findobj('tag','mjplot'));
	handles = figdata.handles(1);

	overlay = [450 1];
	
	cam=struct();
	cam.lookat= [2.2223 0.7321 1.1000];
	cam.distance= 7.5463;

	animate(T(eng), handles, 10, overlay, cam);

	set(gcf,'name',[get(gcf,'name') '_' T(eng).engine]);

	set(gcf,'tag',['mjplot' num2str(eng)])

end

%%
util_savepdf([1 2],'-PNG')

