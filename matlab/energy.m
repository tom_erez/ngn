setpath;

files = util_modelfiles('energy');
% edit(files{:})


%% get data
for i=1:length(files)
	success = ngn(files{i});
	if ~success
		warning(['simulation failed for model ' files{i}])
	end
end

%% parse
TT = cell(0,0);
for i=1:length(files)
	TT{i} = util_parse(files{i});
end

%% make plots

% change font?
set(0, 'DefaultAxesFontName', 'Times New Roman',...
	'DefaultUIControlFontName','Times New Roman')

set(0,'defaultUicontrolFontSize',10,...
'defaultAxesFontSize',13.5,...
'defaultTextFontSize',10)

% remove engines
%ignoreEngine = [4 6 ]; % mujocoRK, physX
ignoreEngine = [];

allfigs = [];
for i=1:length(files)
	T = TT{i};
	mj('load',which(files{i}));
	model = mj('getmodel');
	model_name = model.model_name;
	figs = energy_plots(T, model_name, ignoreEngine);
	allfigs = [allfigs figs];
end


%% save PDFs
%set(gcf,'name',[get(gcf,'name') '_inliers'])
% util_savepdf(findobj(0,'type','figure')')



 



