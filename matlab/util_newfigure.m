function fig = util_newfigure(name)

fig = findobj(0,'type','figure','name',name);

if isempty(fig)
	fig = figure;
	set(fig,'name',name,'numbertitle','off','pos',[100 100 470 450]);
end

fig = fig(1);

set(fig, 'color', 'w','menubar','none','toolbar','none');

figure(fig);
clf