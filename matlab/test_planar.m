setpath;

filename = '..\models\test_planar.xml';


%%
ngn(filename);

%%

T = util_parse(filename);

%%

animateAll(T,1)

%% plot timing

fig = plot_timing(T);

%% save
util_savepdf(fig)