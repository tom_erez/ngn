setpath;

filename = '..\models\utest_actuators.xml';
outdir = 'C:\temp\output\actuators\';
engines = {'mujoco','havok'};


success = ngn(filename, outdir, engines);

%% parse
if success
    T = parseAll(outdir, mj('size'));
end



%% animate

animateAll(T, 10)

