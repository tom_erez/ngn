function figs = iteration_plots(T, model_name, ignoreEngine)

engList = {T(:,1,1).engine};

[nEng, nDt, nSeg] = size(T);

% remove engines
whichEngine = true(1,nEng);
whichEngine(ignoreEngine) = false;
if ~isempty(ignoreEngine)
	fprintf('ignoring engine: %s \n',T(~whichEngine,1,1).engine)
end
T = T(whichEngine,:,:);


[nEng, nDt, ~] = size(T);

% vector of iternum
DT = 2.^(1:9);

% matrix of dt Values (per engine)
DTM = DT(1:end)'*ones(1,nEng);


% get Y-axis values
YY = zeros(nEng, nDt, nSeg);
for i=1:nEng
    for j=1:nDt
        for k=1:nSeg	
            dx = T(i,j,k).xpos(:,:,:) - T(i,end,k).xpos(:,:,:);
            YY(i,j,k) = sqrt(mean(mean(sum(dx.^2,1),2),3));			
        end
    end
end
Y = nanmean(YY,3);

% get x faster than real time
xFaster = zeros(nEng, nDt, nSeg);
for i=1:nEng
    for j=1:nDt
        for k=1:nSeg
            Dt = T(i,j,k).time(end) - T(i,j,k).time(1);
            xFaster(i,j,k) = Dt / sum(T(i,j,k).CPUtime);
        end
    end
end

X = mean(xFaster,3);

% transpose (for graphics)
Y = Y';
X = X';

% remove reference (smallest) timestep
DT = DT(1:end-1);
DTM = DTM(1:end-1,:);
X = X(1:end-1,:);
Y = Y(1:end-1,:);


%% figure: consistency / timestep

figs(1) = util_newfigure(['consistency_' model_name]);
% set(gcf, 'color', 'w', 'pos', 50+[0 50 400 400]);

h = loglog(DTM , Y,'.-');
set(gca,'ydir','reverse')
hold on
hmarker1 = loglog(DTM([1 1],:),Y([1 1],:) ,'o','markersize',6);
hmarkerend = loglog(DTM([end end],:), Y([end end],:) ,'^','markersize',6);

for i=1:length(h)
	[color, style, width, marker, msize] = engine_line_style(T(i,1,1).engine);
	set(h(i),'color',color,'linestyle',style,'linewidth',width,'marker',marker,'markersize',msize);
	set(hmarker1(i),'color',color);
	set(hmarkerend(i),'color',color);
end

hmarker = [hmarker1; hmarkerend];
set(hmarker,'MarkerFaceColor','auto')

dtrange = round(log2(DTM([1 end],1))); 
title(sprintf('self-consistency:{\\bf %s}, dt = 1/%d .. %d  ms',...
	model_name,2^(-dtrange(1)),2^dtrange(2)))

ylabel 'discrepancy (meters)'
xlabel 'iterations'
legend(engList{whichEngine}, 'location','SW')
grid on
set(gca,'xlim', [DTM(1,1)/2 DTM(end,1)*2])
xt = DTM(:,1);
set(gca,'xtick', xt)
xl1 = arrayfun(@(n)['1/' num2str(1/n)] ,xt(xt<1),'UniformOutput',false);
xl2 = arrayfun(@(n) num2str(n) ,xt(xt>=1),'UniformOutput',false);
set(gca,'xticklabel', [xl1;xl2])


% ylim = get(gca,'ylim');
% set(gca,'ylim', [ylim(1) 1+0.02*(1-ylim(1))])

prettify(gca)

drawnow



%% pareto
figs(2) = util_newfigure(['consistency_pareto_' model_name]);


dtrange = round(log2(DT([1 end]))); % integer log-two-milliseconds dt range.

h = loglog(X, Y ,'.-');
set(gca,'ydir','reverse')
hold on
hmarker1 = loglog(X([1 1],:),Y([1 1],:) ,'o','markersize',6);
hmarkerend = loglog(X([end end],:), Y([end end],:) ,'^','markersize',6);

for i=1:length(h)
	[color, style, width, marker, msize] = engine_line_style(T(i,1,1).engine);
	set(h(i),'color',color,'linestyle',style,'linewidth',width,'marker',marker,'markersize',msize);
	set(hmarker1(i),'color',color);
	set(hmarkerend(i),'color',color);
end

hmarker = [hmarker1; hmarkerend];
set(hmarker,'MarkerFaceColor','auto')
grid on

ylabel 'discrepancy (meters)'
xfasterlabel;

legend(h, {T(:,1,1).engine},	'location','SW');

title(sprintf('self-consistency:{\\bf %s}, dt = 1/%d .. %d  ms',...
	model_name,2^(-dtrange(1)),2^dtrange(2)))

% ylim = get(gca,'ylim');
% set(gca,'ylim', [ylim(1) 1+0.02*(1-ylim(1))])
axis square
prettify(gca)

drawnow

%% CPUtime / contacts

NC = zeros(nEng, nDt*nSeg*length(T(1,1,1).nc));
TIME = zeros(nEng, nDt*nSeg*length(T(1,1,1).nc));
for i=1:nEng
	NC(i,:) = [T(i,:,:).nc];
	TIME(i,:) = [T(i,:,:).CPUtime];	
end

util_newfigure(['time_contacts_' model_name]);

plot(NC',TIME','.')

%% energy traces
%{
util_newfigure(['etraces_' filename]);

grd = ceil(sqrt(nEng));
for i=1:nEng
    subplot(grd,grd,i);
    cla
    hold on    
    for j=nDt:-1:1
        Tj = T(i,j,1).time;
        a=j/nDt;
        for k=1:nSeg
            Ejk  = T(i,j,k).energy / T(i,j,k).energy(1);
            plot(Tj, Ejk,'.-','color',[1 0 0]*a +[0 0 1]*(1-a));
        end
    end
    title(T(i,1).engine)
    set(gca,'ylim',[0 2 ])
end
%}