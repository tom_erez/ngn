function handles = animate(T, handles, realtime, overlay, cam)

logdt = log2(mean(diff(T.time(1:2)))*1e3);

if logdt<0
	dtstr = sprintf('1/%d',round(2^-logdt));
else
	dtstr = sprintf('%d',round(2^logdt));
end

% if realtime ~=1
% 	string = [T.engine sprintf('\n') dtstr ' ms,  ' num2str(realtime) 'x'];
% else
	string = [T.engine sprintf('\n') dtstr ' ms'];
% end
xpos   = T.gxpos;
xmat   = T.gxmat; 
dt     = mean(diff(T.time(T.time~=0)));

if nargin < 4
	overlay = [0 1];
end

if nargin < 5
	cam = [];
end

if all(isnan(handles.geoms))
    mjplot
    fig = gcf;
    figdata = getappdata(fig);
    handles = figdata.handles(1);
end    

% bring the figure into focus
if ~isempty(findobj(0,'Tag','mjplot'))
       figure(findobj(0,'Tag','mjplot'));
end

if ischar(string)
	set(findobj(gcf,'tag','info_text'),'string',string);
end

tplot	= 0.03;  % initial guess of plotting time = 30ms
tstart	= tic;
tracegeoms = [];
for t=1:size(xpos,3)
	if t~=size(xpos,3)
		if(T.time(t+1) < T.time(t))
			break;
		end
	end
	
	if any(any(abs(T.xvel(:,:,t))>1e4))
		continue;
	end	
	
   % plot if we have time to do so or it's a spashot
   snapshot = (mod(t, overlay(1)) == 2)*(overlay(1)>0);
    if t*dt/realtime > toc(tstart) || snapshot
        tic
        for i=1:size(xpos,1)
            
			if nargin > 3
				if snapshot
					hands = handles.geoms(~isnan(handles.geoms));
					newgeoms = copyobj(hands, gca);
					tracegeoms = [tracegeoms; newgeoms];
					ntr = length(tracegeoms);
					talpha = get(tracegeoms,'FaceAlpha')';
					salpha = cellfun(@(x)overlay(2)*x, talpha);
					for j=1:ntr
						set(tracegeoms(j),'FaceAlpha', salpha(j));	
					end
				end
			end
			
            geom	= handles.geoms(i);
            if isnan(geom) || strcmp(get(geom,'visible'),'off')
                continue;
            end	
            if ~ischar(string)
                set(findobj(gcf,'tag','info_text'),'string',string{t});
            end

            % vertices in local coordinates
            vert		= get(geom,'userdata');

            % extract position and orientation, compute vertices
            pos		= xpos(i,:,t);
            rot		= reshape(xmat(i,:,t), [3, 3]);
            vert	= vert * rot + ones(size(vert,1),1) * pos;

            % update geom, skip world geoms for pose > 1
            set(geom,'vertices', vert, 'visible', 'on')
            setappdata(geom, 'type', 'geom')
            setappdata(geom, 'id', i)
            setappdata(geom, 'pos', pos)

        end
        
%         % camera following
%         ax          = getappdata(fig,'main_ax');
%         cam         = get(ax, 'userdata');
%         az			= cam.azimuth/180*pi;
%         el			= cam.elevation/180*pi;
%         vec			= [cos(el)*cos(az), cos(el)*sin(az), sin(el)];
%         lookat      = mean(xpos(:,:,t),1);
%         campos		= lookat + cam.distance*vec;
%         campos0     = get(ax,'cameraposition');
%         set(ax,'cameratarget',lookat,'cameraposition',0.5*campos + 0.5*campos0);	
    
		if ~isempty(cam)
			ax          = getappdata(gcf,'main_ax');
			axcam       = get(ax, 'userdata');
			axcam.lookat = cam.lookat;
			axcam.distance = cam.distance;
			set(ax, 'userdata', axcam);
		end

        drawnow;
        tplot = 0.8*tplot + 0.2*toc;
    end
    
    % pause if we're ahead of the clock by more than the plot time
	p = t*dt/realtime - toc(tstart) - tplot;
    if p > 0
        pause(p);
    end
end
