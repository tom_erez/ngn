setpath;

files = util_modelfiles('momentum');
% edit(files{:})

%% run experiments
for i=1:length(files)
	success = ngn(files{i});
	if ~success
		warning(['simulation failed for model ' files{i}])
	end
end

%% parse
TT = cell(0,0);
for i=1:length(files)
	TT{i} = util_parse(files{i});
end

%% make plots

% change font?
 set(0, 'DefaultAxesFontName', 'Times New Roman',...
 	'DefaultUIControlFontName','Times New Roman')

set(0,'defaultUicontrolFontSize',10,...
'defaultAxesFontSize',13.5,...
'defaultTextFontSize',10)

% remove engines
ignoreEngine = []; % bullet_A, havok

% close all
allfigs = [];
ma = [1 1 1;0 0 1];
for i=1:length(files)
	T = TT{i};
	mj('load',which(files{i}));
	model = mj('getmodel');
	model_name = model.model_name;
	figs = momentum_plots(T, model_name, ignoreEngine, ma(i,:)');
	allfigs = [allfigs figs];
end

% tilefigs

%% save PDFs

% util_savepdf(findobj(0,'type','figure')')


