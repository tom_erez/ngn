setpath;

files = util_modelfiles('consistency');
% edit(files{:})

%% get data
for i=1:length(files)
	success = ngn(files{i});
	if ~success
		warning(['simulation failed for model ' files{i}])
	end
end


%% parse
TT = cell(0,0);
for i=1:length(files)
	TT{i} = util_parse(files{i});
end

%% make plots

% change font?
set(0, 'DefaultAxesFontName', 'Times New Roman',...
	'DefaultUIControlFontName','Times New Roman')

set(0,'defaultUicontrolFontSize',10,...
'defaultAxesFontSize',13.5,...
'defaultTextFontSize',10)

% remove engines
% ignoreEngine = [2]; % bullet_A
ignoreEngine = [];

allfigs = [];
for i=1:length(files)
 	T = TT{i};
	mj('load',which(files{i}));
	model = mj('getmodel');
	model_name = model.model_name;
	switch model_name
		case 'planar chain'
			ignoreEngine = [];		
		case 'grasp'
			ignoreEngine = [2 3];
		otherwise
			ignoreEngine = [2];
	end
	consistency_plots(T, model_name, ignoreEngine);
end


%% save PDFs

% util_savepdf

