function animateAll(T, realtime)

if nargin == 1
    realtime = 1;
end

mjplot
figdata = getappdata(findobj('tag','mjplot'));
handles = figdata.handles(1);

for i=1:size(T,1)
	for j=1:size(T,2)
		animate(T(i,j), handles, realtime);
		pause(.5)
	end

end

%%

% O = findobj(gcf,'type','patch');
% vis = get(O,'visible')