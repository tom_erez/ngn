N = 3;

K = N^3;

edge = .4*(0:N-1);

[x,y,z] = meshgrid(edge,edge,0.3+edge);

Q = randn(4,K);
Q = Q * diag(1./sqrt(sum(Q.^2,1)));

Q = [x(:)'; y(:)'; z(:)'; Q]'