function setpath

if ~isempty(strfind(cd,'Yuval'))
    addpath('C:\Users\Yuval\Documents\ngn\models');
elseif ~isempty(strfind(cd,'etom'))
    run ../../SVN/merge08/extensions/MATLAB/set_path.m
else
   error('setpath: knock knock. who''s there?') 
end

addpath('../models/')