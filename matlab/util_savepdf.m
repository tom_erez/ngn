function util_savepdf(figs,format)

if nargin == 0
	figs = findobj(0,'type','figure')';
end

if nargin <= 1
	format = '-pdf';
end

addpath ../../../Matlab/export_fig/
for f=figs
	set(0,'currentFigure',f);
	name = strrep(get(gcf,'name'),' ','_');
	figname = ['../figures/' name];
	disp(['compiling ' figname])
	export_fig(figname,format,f)
	saveas(f,figname,'fig');
end