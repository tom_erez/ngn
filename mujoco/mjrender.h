//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2015 Roboti LLC.  //
//-----------------------------------//

#pragma once


typedef enum _mjtGridPos				// grid position for overlay
{
	mjGRID_TOPLEFT = 0,					// top left
	mjGRID_TOPRIGHT,					// top right
	mjGRID_BOTTOMLEFT,					// bottom left
	mjGRID_BOTTOMRIGHT					// bottom right
} mjtGridPos;


typedef enum _mjtStereo					// kind of stereo
{
	mjSTEREO_NONE = 0,					// no stereo
	mjSTEREO_HARDWARE,					// quad-buffered
	mjSTEREO_SIDEBYSIDE					// side-by-side
} mjtStereo;


typedef enum _mjtRndFlag				// flags enabling rendering effects
{
	mjRND_SHADOW,						// shadows
	mjRND_FLIP,							// flip left/right
	mjRND_REFLECTION,					// reflections
	mjRND_WIREFRAME,					// wireframe mode

	mjNRNDFLAG							// number of rendering flags
} mjtRndFlag;


struct _mjrContext						// custom OpenGL context
{
	unsigned int shadowsize;			// size of shadow map texture
	int skybox;							// skybox texture; -1: none
	float linewidth;					// line width for wireframe rendering

	unsigned int shadowFBO;				// shadow map framebuffer object
	unsigned int shadowTex;				// shadow map texture

	unsigned int baseTexture;			// displaylist starting positions
	unsigned int baseMesh;
	unsigned int baseHField;
	unsigned int baseBuiltin;
	unsigned int baseFontNormal;
	unsigned int baseFontBack;
	unsigned int baseFontBig;

	int		rangeTexture;				// displaylist ranges
	int		rangeMesh;
	int		rangeHField;
	int		rangeBuiltin;
	int		rangeFont;

	int		charWidth[127];				// character sizes
	int		charWidthBig[127];
	int		charHeight;
	int		charHeightBig;

	int     ready;						// context ready to be used
};
typedef struct _mjrContext mjrContext;


struct _mjrOption						// OpenGL options, window-specific
{
	int		stereo;						// type of stereo (mjtStereo)
	mjtByte flags[mjNRNDFLAG];			// rendering flags (indexed by mjtRndFlag)
};
typedef struct _mjrOption mjrOption;