//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2015 Roboti LLC.  //
//-----------------------------------//

#pragma once


// cross-platform import
#ifdef _WIN32
//	#define MJAPI __declspec(dllimport)
	#define MJAPI
#else
	#define MJAPI
#endif


// this is a C-API
#ifdef __cplusplus
extern "C"
{
#endif


// type definitions
#include "mjmodel.h"
#include "mjdata.h"
#include "mjvisualize.h"
#include "mjrender.h"


// useful macros
#define mjMARKSTACK   int _mark = d->pstack;
#define mjFREESTACK   d->pstack = _mark;
#define mjDISABLED(x) (m->opt.disableflags & (x))


// user error and memory handlers
MJAPI extern void  (*mju_user_error)(const char*);
MJAPI extern void  (*mju_user_warning)(const char*);
MJAPI extern void* (*mju_user_malloc)(unsigned int, unsigned int);
MJAPI extern void  (*mju_user_free)(void*);


// callbacks extending computation pipeline
MJAPI extern mjfCallback mjcb_endstep;
MJAPI extern mjfCallback mjcb_passive;
MJAPI extern mjfCallback mjcb_control;
MJAPI extern mjfCallback mjcb_control_framework;
MJAPI extern mjfTime     mjcb_time;
MJAPI extern mjfActBias  mjcb_act_bias;
MJAPI extern mjfActGain  mjcb_act_gain;
MJAPI extern mjfActDyn   mjcb_act_dyn;


// string names
MJAPI extern const char* mjVISSTRING[mjNVISFLAG][3];
MJAPI extern const char* mjRNDSTRING[mjNRNDFLAG][3];
MJAPI extern const char* mjDISABLESTRING[mjNDISABLE];
MJAPI extern const char* mjTIMERSTRING[mjNTIMER];


//---------------------- Access to XML parser and C++ compiler --------------------------

// parse XML file in MJCF or URDF format, compile it, return low-level model
MJAPI mjModel* mj_loadXML(const char* filename, char* error);

// update XML data structures with info from low-level model, save as MJCF
MJAPI int mj_saveXML(const char* filename, const mjModel* m, char* error);

// print internal XML schema as plain text or HTML
MJAPI int mj_printSchema(const char* filename, char* buffer, int buffer_sz, int flg_html);


//---------------------- Main entry points ----------------------------------------------

// forward dynamics: compute qacc, act_dot
MJAPI void mj_forward(const mjModel* m, mjData* d);

// inverse dynamics; flg_full: run position-dependent computations
MJAPI void mj_inverse(const mjModel* m, mjData* d, int flg_full);

// advance simulation: use control callback, no external force, RK4 available
MJAPI void mj_step(const mjModel* m, mjData* d);

// advance simulation in two steps: before external force/control is set by user
MJAPI void mj_step1(const mjModel* m, mjData* d);

// advance simulation in two steps: after external force/control is set by user
MJAPI void mj_step2(const mjModel* m, mjData* d);

// parallel step, part 1: serial
MJAPI void mj_parstepStart(const mjModel* m, mjData* d);

// parallel step, part 2: three parallel branches
MJAPI void mj_parstepBranch(const mjModel* m, mjData* d, int branch);

// parallel step, part 3: serial
MJAPI void mj_parstepFinish(const mjModel* m, mjData* d);


//---------------------- Model and data initialization ----------------------------------

// set physics options to default values
MJAPI void mj_defaultOption(mjOption* opt);

// set visual options to default values
MJAPI void mj_defaultVisual(mjVisual* vis);

// copy mjModel; allocate new if dest is NULL ???
MJAPI mjModel* mj_copyModel(mjModel* dest, const mjModel* src);

// save model to binary file or memory buffer
MJAPI void mj_saveModel(const mjModel* m, const char* filename, int szbuf, void* buf);

// load model from binary file or memory buffer
MJAPI mjModel* mj_loadModel(const char* filename, int szbuf, void* buf);

// de-allocate model
MJAPI void mj_deleteModel(mjModel* m);

// size of buffer needed to hold model
MJAPI int mj_sizeModel(const mjModel* m);

// allocate mjData correponding to given model
MJAPI mjData* mj_makeData(const mjModel* m);

// copy mjData
MJAPI mjData* mj_copyData(mjData* dest, const mjModel* m, const mjData* src);

// set data to defaults
MJAPI void mj_resetData(const mjModel* m, mjData* d);

// mjData stack allocate
MJAPI mjtNum* mj_stackAlloc(mjData* d, int size);

// de-allocate data
MJAPI void mj_deleteData(mjData* d);

// reset callbacks to defaults
MJAPI void mj_resetCallbacks(void);

// set constant fields of mjModel
MJAPI void mj_setConst(mjModel* m, mjData* d, int flg_actrange);


//---------------------- Printing -------------------------------------------------------

// print model to text file
MJAPI void mj_printModel(const mjModel* m, const char* filename); 

// print data to text file
MJAPI void mj_printData(const mjModel* m, mjData* d, const char* filename); 

// print matrix to screen
MJAPI void mju_printMat(const mjtNum* mat, int nr, int nc);

// allow screen / file options in both ???


//---------------------- Components of the computation pipeline -------------------------

// computations that depend only on qpos
MJAPI void mj_fwdPosition(const mjModel* m, mjData* d);

// computations that depend only on qpos and qvel
MJAPI void mj_fwdVelocity(const mjModel* m, mjData* d);

// call control callbacks... is this actually needed ???
MJAPI void mj_getControl(const mjModel* m, mjData* d);

// compute actuator force
MJAPI void mj_fwdActuation(const mjModel* m, mjData* d);

// add up all non-constraint forces, compute qacc_unc
MJAPI void mj_fwdAcceleration(const mjModel* m, mjData* d);

// forward constraint
MJAPI void mj_fwdConstraint(const mjModel* m, mjData* d);

// sensor data
MJAPI void mj_fwdSensor(const mjModel* m, mjData* d);

// compute energy
MJAPI void mj_energy(const mjModel* m, mjData* d);

// Euler integrator, semi-implicit in velocity
MJAPI void mj_Euler(const mjModel* m, mjData* d);

// Runge-Kutta explicit order-N integrator
MJAPI void mj_RungeKutta(const mjModel* m, mjData* d, int N);


//---------------------- Sub-components of the computation pipeline ---------------------

// check positions; reset if bad
MJAPI void mj_checkPos(const mjModel* m, mjData* d);

// check velocities; reset if bad
MJAPI void mj_checkVel(const mjModel* m, mjData* d);

// check accelerations; reset if bad
MJAPI void mj_checkAcc(const mjModel* m, mjData* d);

// forward kinematics
MJAPI void mj_kinematics(const mjModel* m, mjData* d);

// map inertias and motion dofs to global frame centered at CoM
MJAPI void mj_com(const mjModel* m, mjData* d);

// compute tendon lengths, velocities and moment arms
MJAPI void mj_tendon(const mjModel* m, mjData* d);

// compute actuator transmission lengths and moments
MJAPI void mj_transmission(const mjModel* m, mjData* d);

// collision detection
MJAPI void mj_collision(const mjModel* m, mjData* d);

// recursive Newton-Euler algorithm
MJAPI void mj_rne(const mjModel* m, mjData* d, const mjtNum* qacc);

// rne with complete data: compute cacc, cfrc_ext, cfrc_int
MJAPI void mj_rnePost(const mjModel* m, mjData* d);

// composite rigid body inertia algorithm
MJAPI void mj_crb(const mjModel* m, mjData* d);

// sparse L'*D*L factorizaton of the inertia matrix
MJAPI void mj_factorM(const mjModel* m, mjData* d);

// sparse backsubstitution:  x = inv(L'*D*L)*y
MJAPI void mj_backsubM(const mjModel* m, mjData* d, mjtNum* x, const mjtNum* y, int n);

// half of sparse backsubstitution:  x = sqrt(inv(D))*inv(L')*y
MJAPI void mj_backsubM2(const mjModel* m, mjData* d, mjtNum* x, const mjtNum* y, int n);

// spring-dampers and body viscosity
MJAPI void mj_passive(const mjModel* m, mjData* d);

// construct constraints
MJAPI void mj_makeConstraint(const mjModel* m, mjData* d);

// compute dense matrices: efc_AR, e_ARchol, fc_half, fc_AR
MJAPI void mj_projectConstraint(const mjModel* m, mjData* d);

// compute efc_vel, efc_aref
MJAPI void mj_referenceConstraint(const mjModel* m, mjData* d);


//---------------------- Support functions ----------------------------------------------

// determine type of friction cone
MJAPI int mj_isPyramid(const mjModel* m);

// determine type of constraint Jacobian
MJAPI int mj_isSparse(const mjModel* m);

// multiply Jacobian by vector
MJAPI void mj_mulJacVec(const mjModel* m, mjData* d, 
				        mjtNum* res, const mjtNum* vec);

// multiply JacobianT by vector
MJAPI void mj_mulJacTVec(const mjModel* m, mjData* d, mjtNum* res, const mjtNum* vec);

// compute 3/6-by-nv Jacobian of global point attached to given body
MJAPI void mj_jac(const mjModel* m, const mjData* d, 
			      mjtNum* jacp, mjtNum* jacr, const mjtNum* point, int body);

// compute body frame Jacobian
MJAPI void mj_jacBody(const mjModel* m, const mjData* d, 
				      mjtNum* jacp, mjtNum* jacr, int body);

// compute body center-of-mass Jacobian
MJAPI void mj_jacBodyCom(const mjModel* m, const mjData* d, 
				         mjtNum* jacp, mjtNum* jacr, int body);

// compute geom Jacobian
MJAPI void mj_jacGeom(const mjModel* m, const mjData* d, 
				      mjtNum* jacp, mjtNum* jacr, int geom);

// compute site Jacobian
MJAPI void mj_jacSite(const mjModel* m, const mjData* d, 
				      mjtNum* jacp, mjtNum* jacr, int site);

// compute translation Jacobian of point, and rotation Jacobian of axis
MJAPI void mj_jacPointAxis(const mjModel* m, mjData* d, 
					       mjtNum* jacPoint, mjtNum* jacAxis, 
					       const mjtNum* point, const mjtNum* axis, int body);

// get id of object with specified name; -1: not found
MJAPI int mj_name2Id(const mjModel* m, mjtObj type, const char* name);

// get name of object with specified id; 0: invalid type or id
MJAPI const char* mj_id2Name(const mjModel* m, mjtObj type, int id);

// convert sparse inertia matrix M into full matrix
MJAPI void mj_fullM(const mjModel* m, mjtNum* dst, const mjtNum* M);

// multiply vector by inertia matrix
MJAPI void mj_mulM(const mjModel* m, const mjData* d, mjtNum* res, const mjtNum* vec);

// apply cartesian force and torque (outside xfrc_applied mechanism)
MJAPI void mj_applyFT(const mjModel* m, mjData* d, 
				      const mjtNum* force, const mjtNum* torque, 
				      const mjtNum* point, int body, mjtNum* qfrc_target);

// get object 6D velocity/acceleration, object-centered, world/local orientation
MJAPI void mj_objectMotion(const mjModel* m, const mjData* d, 
					       int objtype, int objid, mjtNum* res, 
					       mjtByte flg_acc, mjtByte flg_local, const mjtNum* data);

// compute velocity by finite-differencing two positions
MJAPI void mj_differentiatePos(const mjModel* m, mjtNum* qvel, mjtNum dt,
					           const mjtNum* qpos1, const mjtNum* qpos2);

// integrate position with given velocity
MJAPI void mj_integratePos(const mjModel* m, mjtNum* qpos, const mjtNum* qvel, mjtNum dt);

// normalize all quaterions in qpos and mocap_quat
MJAPI void mj_normalizeQuat(const mjModel* m, mjData* d);

// map from body local to global Cartesian coordinates
MJAPI void mj_local2Global(mjData* d, mjtNum* xpos, mjtNum* xmat, 
					       const mjtNum* pos, const mjtNum* quat, int body);

// sum all body masses
MJAPI mjtNum mj_getTotalmass(const mjModel* m);

// scale body masses and inertias to achieve specified total mass
MJAPI void mj_setTotalmass(mjModel* m, mjtNum newmass);


//---------------------- Test functions -------------------------------------------------

// compare forward and inverse dynamics, without changing results of forward dynamics
MJAPI void mj_compareFwdInv(const mjModel* m, mjData* d, mjtNum* fwdinv);

// more test functions ???


//---------------------- Asbtract visualization: 2D --------------------------------------

// set default line, clear buffer
MJAPI void mjv_defaultLine(mjvLineSpec* ln, int i);

// set default Render2
MJAPI void mjv_defaultRender2(mjvRender2* r, int margin);

// reset data in 2D plot
MJAPI void mjv_resetLineData(mjvLineData* data);

// add one point to each line in 2D plot; size of y-array assumed to be data->nline
MJAPI void mjv_addLineData(mjvLineData* data, const float x, const float* y);


//---------------------- Asbtract visualization: 3D --------------------------------------

// set window-specific render options to default values
MJAPI void mjv_defaultOption(mjvOption* vopt);

// set default camera pose
MJAPI void mjv_defaultCameraPose(mjvCameraPose* pose);

// set default camera
MJAPI void mjv_defaultCamera(mjvCamera* cam);

// set high-level camera info, or pose for fixed camera
MJAPI void mjv_setCamera(const mjModel* m, const mjData* d, mjvCamera* cam);

// update camera pose given high-level info
MJAPI void mjv_updateCameraPose(mjvCamera* cam, mjtNum aspect);

// convert 3D vector to z-aligned world coordinates
MJAPI void mjv_convert3D(mjtNum* res, const mjtNum* vec,
						 mjtNum scale, const mjvCameraPose* campose);

// convert 2D mouse motion to z-aligned 3D world coordinates
MJAPI void mjv_convert2D(mjtNum* res, mjtMouse mode, mjtNum dx, mjtNum dy,
						 mjtNum scale, const mjvCameraPose* campose);

// move camera
MJAPI void mjv_moveCamera(mjtMouse action, float dx, float dy, mjvCamera* cam, 
						  float width, float height);

// translate or rotate object
MJAPI void mjv_moveObject(mjtMouse action, float dx, float dy, const mjvCameraPose* campose,
						  float width, float height, mjtNum* pos, mjtNum* quat);

// add mouse perturbation to xfrc_applied
MJAPI void mjv_mousePerturb(const mjModel* m, mjData* d, int select,
							int perturb, const mjtNum* refpos, const mjtNum* refquat);

// move selected subtree or fixed body
MJAPI void mjv_mouseEdit(mjModel* m, mjData* d, int select,
				         int perturb, const mjtNum* refpos, const mjtNum* refquat);

// make list of abstract geoms
MJAPI int mjv_makeGeoms(const mjModel* m, mjData* d, mjvGeom* buf, int bufsz,
						const mjvOption* vopt, int catmask, int select, 
						const mjtNum* refpos, const mjtNum* refquat, const mjtNum* localpos);

// make list of abstract lights
MJAPI int mjv_makeLights(const mjModel* m, mjData* d, mjvLight* buf, int bufsz);


//---------------------- OpenGL rendering: 2D -------------------------------------------

// 2D rendering
MJAPI void mjv_renderGL2(int width, int height, const mjvLineSpec* spec,
				         const mjvLineData* data, mjvRender2* r, const char* title,
				         const mjrContext* con);

// text overlay
MJAPI void mjr_overlay(int width, int height, mjtGridPos gridpos, int big,
					   const char* overlay, const char* overlay2, const mjrContext* con);


//---------------------- OpenGL rendering: 3D -------------------------------------------

// render text (normal or big)
MJAPI void mjr_text(const char* txt, const mjrContext* con, int big,
			        float x, float y, float z, float r, float g, float b);

// render text with background clear (always normal)
MJAPI void mjr_textback(const char* txt, const mjrContext* con,
				        float x, float y, float z, float r, float g, float b);

// compute text width (normal or big)
MJAPI int mjr_textWidth(const char* txt, const mjrContext* con, int big);

// set default mjrOption
MJAPI void mjr_defaultOption(mjrOption* ropt);

// set default mjrContext
MJAPI void mjr_defaultContext(mjrContext* con);

// allocate resources in custom OpenGL context; fontscale = 100, 150, 200 (%)
MJAPI void mjr_makeContext(const mjModel* m, mjrContext* con, int fontscale);

// free resources in custom OpenGL context
MJAPI void mjr_freeContext(mjrContext* con);

// 3D rendering
MJAPI void mjr_render(int ngeom, mjvGeom* geom, int* sortbuf,
					  int nlight, mjvLight* light,
					  const mjrOption* ropt, mjvCameraPose* campose,
					  int width, int height, const mjrContext* con);

// 3D selection
MJAPI int mjr_select(int ngeom, const mjvGeom* geom, 
					 const mjrOption* ropt, mjvCameraPose* campose,
					 int width, int height, const mjrContext* con, 
					 int mousex, int mousey, mjtNum* pos, mjtNum* depth);


//---------------------- Utility functions: error and memory ----------------------------

// main error function; does not return to caller
MJAPI void mju_error(const char* msg);

// error function with int argument; msg is a printf format string
MJAPI void mju_error_i(const char* msg, int i);

// error function with string argument
MJAPI void mju_error_s(const char* msg, const char* text);

// main warning function; returns to caller
MJAPI void mju_warning(const char* msg);

// warning function with int argument
MJAPI void mju_warning_i(const char* msg, int i);

// warning function with string argument
MJAPI void mju_warning_s(const char* msg, const char* text);

// clear user error and memory handlers
MJAPI void mju_clearHandlers(void);

// allocate memory (with malloc() by default); byte-align if align > 0
MJAPI void* mju_malloc(unsigned int sz, unsigned int align);

// free memory (with free() by default)
MJAPI void mju_free(void* ptr);

// high-level warning function: count warnings in mjData, print only the first time
MJAPI void mj_warning(mjData* d, int warning);


//---------------------- Utility functions: basic math ----------------------------------

#define mjMAX(a,b) (((a) > (b)) ? (a) : (b))
#define mjMIN(a,b) (((a) < (b)) ? (a) : (b))

#ifdef mjUSEDOUBLE
	#define mju_sqrt	sqrt
	#define mju_exp		exp
	#define mju_sin		sin
	#define mju_tan		tan
	#define mju_cos		cos
	#define mju_acos	acos
	#define mju_asin	asin
	#define mju_atan2	atan2
	#define mju_tanh	tanh
	#define mju_pow		pow
	#define mju_abs		fabs
	#define mju_log		logf
	#define mju_log10	log10
	#define mju_floor	floor
	#define mju_ceil	ceil

#else
	#define mju_sqrt	sqrtf
	#define mju_exp		expf
	#define mju_sin		sinf
	#define mju_cos		cosf
	#define mju_tan		tanf
	#define mju_acos	acosf
	#define mju_asin	asinf
	#define mju_atan2	atan2f
	#define mju_tanh	tanhf
	#define mju_pow		powf
	#define mju_abs		fabs
	#define mju_log		logf
	#define mju_log10	log10f
	#define mju_floor	floor
	#define mju_ceil	ceil
#endif

// set vector to zero
MJAPI void mju_zero3(mjtNum* res);

// copy vector
MJAPI void mju_copy3(mjtNum* res, const mjtNum* data);

// scale vector
MJAPI void mju_scl3(mjtNum* res, const mjtNum* vec, mjtNum scl);

// add vectors
MJAPI void mju_add3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2);

// subtract vectors
MJAPI void mju_sub3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2);

// add to vector
MJAPI void mju_addTo3(mjtNum* res, const mjtNum* vec);

// add scaled to vector
MJAPI void mju_addToScl3(mjtNum* res, const mjtNum* vec, mjtNum scl);

// res = vec1 + scl*vec2
MJAPI void mju_addScl3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, mjtNum scl);

// normalize vector, return length before normalization
MJAPI mjtNum mju_normalize3(mjtNum* res);

// compute vector length (without normalizing)
MJAPI mjtNum mju_norm3(const mjtNum* res);

// vector dot-product
MJAPI mjtNum mju_dot3(const mjtNum* vec1, const mjtNum* vec2);

// Cartesian distance between 3D vectors
MJAPI mjtNum mju_dist3(const mjtNum* pos1, const mjtNum* pos2);

// multiply vector by 3D rotation matrix
MJAPI void mju_rotVecMat(mjtNum* res, const mjtNum* vec, const mjtNum* mat);

// multiply vector by transposed 3D rotation matrix
MJAPI void mju_rotVecMatT(mjtNum* res, const mjtNum* vec, const mjtNum* mat);

// vector cross-product, 3D
MJAPI void mju_cross(mjtNum* res, const mjtNum* a, const mjtNum* b);

// set vector to zero
MJAPI void mju_zero(mjtNum* res, int n);

// copy vector
MJAPI void mju_copy(mjtNum* res, const mjtNum* data, int n);

// scale vector
MJAPI void mju_scl(mjtNum* res, const mjtNum* vec, mjtNum scl, int n);

// add vectors
MJAPI void mju_add(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, int n);

// subtract vectors
MJAPI void mju_sub(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, int n);

// add to vector
MJAPI void mju_addTo(mjtNum* res, const mjtNum* vec, int n);

// add scaled to vector
MJAPI void mju_addToScl(mjtNum* res, const mjtNum* vec, mjtNum scl, int n);

// res = vec1 + scl*vec2
MJAPI void mju_addScl(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, 
					  mjtNum scl, int n);

// normalize vector, return length before normalization
MJAPI mjtNum mju_normalize(mjtNum* res, int n);

// compute vector length (without normalizing)
MJAPI mjtNum mju_norm(const mjtNum* res, int n);

// vector dot-product
MJAPI mjtNum mju_dot(const mjtNum* vec1, const mjtNum* vec2, const int n);

// multiply matrix and vector
MJAPI void mju_mulMatVec(mjtNum* res, const mjtNum* mat, const mjtNum* vec,
						 int nr, int nc);

// multiply transposed matrix and vector
MJAPI void mju_mulMatTVec(mjtNum* res, const mjtNum* mat, const mjtNum* vec,
						  int nr, int nc);

// transpose matrix
MJAPI void mju_transpose(mjtNum* res, const mjtNum* mat, int r, int c);

// multiply matrices
MJAPI void mju_mulMatMat(mjtNum* res, const mjtNum* mat1, const mjtNum* mat2,
						 int r1, int c1, int c2);

// multiply matrices, second argument transposed
MJAPI void mju_mulMatMatT(mjtNum* res, const mjtNum* mat1, const mjtNum* mat2,
						  int r1, int c1, int r2);

// compute M*M'; needs r*c space
MJAPI void mju_sqrMat(mjtNum* res, const mjtNum* mat, int r, int c, mjtNum* scratch);

// multiply matrices, first argument transposed
MJAPI void mju_mulMatTMat(mjtNum* res, const mjtNum* mat1, const mjtNum* mat2,
						  int r1, int c1, int c2);


//---------------------- Utility functions: quaternions ---------------------------------

// rotate vector by quaternion
MJAPI void mju_rotVecQuat(mjtNum* res, const mjtNum* vec, const mjtNum* quat);

// negate quaternion
MJAPI void mju_negQuat(mjtNum* res, const mjtNum* quat);

// muiltiply quaternions
MJAPI void mju_mulQuat(mjtNum* res, const mjtNum* quat1, const mjtNum* quat2);

// muiltiply quaternion and axis
MJAPI void mju_mulQuatAxis(mjtNum* res, const mjtNum* quat, const mjtNum* axis);

// convert axisAngle to quaternion
MJAPI void mju_axisAngle2Quat(mjtNum* res, const mjtNum* axis, mjtNum angle);

// convert quaternion (corresponding to orientation difference) to 3D velocity
MJAPI void mju_quat2Vel(mjtNum* res, const mjtNum* quat, mjtNum dt);

// convert quaternion to 3D rotation matrix
MJAPI void mju_quat2Mat(mjtNum* res, const mjtNum* quat);

// convert 3D rotation matrix to quaterion
MJAPI void mju_mat2Quat(mjtNum* quat, const mjtNum* mat);

// time-derivative of quaternion, given 3D rotational velocity
MJAPI void mju_derivQuat(mjtNum* res, const mjtNum* quat, const mjtNum* vel);

// integrate quaterion given 3D angular velocity
MJAPI void mju_quatIntegrate(mjtNum* quat, const mjtNum* vel, mjtNum scale);

// compute quaternion performing rotation from given vector to z-axis
MJAPI void mju_quatZ2Vec(mjtNum* quat, const mjtNum* vec);


//---------------------- Utility functions: matrix decomposition ------------------------

// Cholesky decomposition
MJAPI int mju_cholFactor(mjtNum* mat, mjtNum* diag, int n,
						 mjtNum minabs, mjtNum minrel, mjtNum* correct);

// Cholesky backsubstitution: phase&i enables forward(i=1), backward(i=2) pass
MJAPI void mju_cholBacksub(mjtNum* res, const mjtNum* mat, const mjtNum* vec,
						   int n, int nvec, int phase);

// eigenvalue decomposition of symmetric 3x3 matrix
MJAPI int mju_eig3(mjtNum* eigval, mjtNum* eigvec, mjtNum* quat, const mjtNum* mat);


//---------------------- Utility functions: miscellaneous -------------------------------

// muscle FVL curve: prm = (lminrel, lmaxrel, widthrel, vmaxrel, fmax, fvsat)
MJAPI mjtNum mju_muscleFVL(mjtNum len, mjtNum vel, mjtNum lmin, mjtNum lmax, mjtNum* prm);

// muscle passive force: prm = (lminrel, lmaxrel, fpassive)
MJAPI mjtNum mju_musclePassive(mjtNum len, mjtNum lmin, mjtNum lmax, mjtNum* prm);

// pneumatic cylinder dynamics
MJAPI mjtNum mju_pneumatic(mjtNum len, mjtNum len0, mjtNum vel, mjtNum* prm,
						   mjtNum act, mjtNum ctrl, mjtNum timestep, mjtNum* jac);

// convert contact force to pyramid representation
MJAPI void mju_encodePyramid(mjtNum* pyramid, const mjtNum* force,
							 const mjtNum* mu, int dim);

// convert pyramid representation to contact force
MJAPI void mju_decodePyramid(mjtNum* force, const mjtNum* pyramid,
							 const mjtNum* mu, int dim);

// min function, single evaluation of a and b
MJAPI mjtNum mju_min(mjtNum a, mjtNum b);

// max function, single evaluation of a and b
MJAPI mjtNum mju_max(mjtNum a, mjtNum b);

// sign function
MJAPI mjtNum mju_sign(mjtNum x);

// round to nearest integer
MJAPI int mju_round(mjtNum x);

// convert type id to type name
MJAPI const char* mju_type2Str(int type);

// convert type id to type name
MJAPI mjtObj mju_str2Type(const char* str);

// warning text
MJAPI const char* mju_warningText(int warning);

// return 1 if nan or abs(x)>mjMAXVAL, 0 otherwise
MJAPI int mju_isBad(mjtNum x);

// return 1 if all elements are 0
MJAPI int mju_isZero(mjtNum* vec, int n);


#ifdef __cplusplus
}
#endif
