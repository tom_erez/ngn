#ifndef __CCD_CONFIG_H__
#define __CCD_CONFIG_H__

#include "engine/engine_config.h"

#ifdef mjUSEDOUBLE
	#define CCD_DOUBLE
#else
	#define CCD_SINGLE
#endif

#endif /* __CCD_CONFIG_H__ */
