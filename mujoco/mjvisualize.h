//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2015 Roboti LLC.  //
//-----------------------------------//

#pragma once

//---------------------------- global constants -----------------------------------------

#define mjNGROUP		5				// number of geom and site groups with visflags
#define mjLINEBUF		401				// size of circular buffer for line points
#define mjMAXLINE		100				// maximum number of lines per plot
#define mjMAXOVERLAY	500				// maximum number of characters in overlay text


//------------------------------- 3D visualization --------------------------------------

typedef enum _mjtCatBit					// bitflags for mjvGeom category
{
	mjCAT_STATIC		= 1,			// model elements in body 0
	mjCAT_DYNAMIC		= 2,			// model elements in all other bodies
	mjCAT_DECOR			= 4,			// decorative geoms
	mjCAT_ALL			= 7				// select all categories
} mjtCatBit;


typedef enum _mjtMouse					// mouse interaction mode
{
	mjMOUSE_ROTATE_V = 0,				// rotate, vertical plane
	mjMOUSE_ROTATE_H,					// rotate, horizontal plane
	mjMOUSE_MOVE_V,						// move, vertical plane
	mjMOUSE_MOVE_H,						// move, horizontal plane
	mjMOUSE_ZOOM,						// zoom
	mjMOUSE_SELECT						// selection
} mjtMouse;


typedef enum _mjtPertBit				// mouse perturbations
{
	mjPERT_TRANSLATE    = 1,			// translation
	mjPERT_ROTATE       = 2				// rotation
} mjtPertBit;


typedef enum _mjtLabel					// object labeling
{
	mjLABEL_NONE = 0,					// nothing
	mjLABEL_BODY,						// body labels
	mjLABEL_JOINT,						// joint labels
	mjLABEL_GEOM,						// geom labels
	mjLABEL_SITE,						// site labels
	mjLABEL_TENDON,						// tendon labels
	mjLABEL_ACTUATOR,					// actuator labels
	mjLABEL_CONSTRAINT,					// constraint labels
	mjLABEL_SELECTION,					// selected object
	mjLABEL_SELPNT						// coordinates of selection point
} mjtLabel;


typedef enum _mjtFrame					// frame visualization
{
	mjFRAME_NONE = 0,					// no frames
	mjFRAME_BODY,						// body frames
	mjFRAME_GEOM,						// geom frames
	mjFRAME_SITE,						// site frames
	mjFRAME_WORLD						// world frame
} mjtFrame;


typedef enum _mjtVisFlag				// flags enabling model element visualization
{
	mjVIS_CONVEXHULL,					// mesh convex hull
	mjVIS_TEXTURE,						// textures
	mjVIS_JOINT,						// joints
	mjVIS_ACTUATOR,						// actuators
	mjVIS_CONSTRAINT,					// point constraints
	mjVIS_INERTIA,						// equivalent inertia boxes
	mjVIS_PERTFORCE,					// perturbation force
	mjVIS_PERTOBJ,						// perturbation object
	mjVIS_CONTACTPOINT,					// contact points
	mjVIS_CONTACTFORCE,					// contact force
	mjVIS_CONTACTSPLIT,					// split contact force into normal and tanget
	mjVIS_TRANSPARENT,					// make dynamic geoms more transparent
	mjVIS_AUTOCONNECT,					// auto connect joints and body coms
	mjVIS_COM,							// center of mass
	mjVIS_SELECT,						// selection point
	mjVIS_STATIC,						// static bodies

	mjNVISFLAG							// number of visualization flags
} mjtVisFlag;


struct _mjvGeom							// all info needed to specify one abstract geom
{
	// type info
	int		type;						// geom type (mjtGeom)
	int		dataid;						// mesh, hfield or texture id; -1: none
	int		objtype;					// mujoco object type; mjOBJ_UNKNOWN for decor
	int		objid;						// mujoco object id; -1 for decor
	int		category;					// visual category
	int     texid;						// texture id; -1: no texture

	// OpenGL info
	float	size[3];					// size parameters (plane, capsule: special ???)
	float	pos[3];						// Cartesian position
	float	mat[9];						// Cartesian orientation
	float	rgba[4];					// color and transparency
	float	emission;					// emission coef
	float	specular;					// specular coef
	float   shininess;					// shininess coef
	float   reflectance;				// reflectance coef
	char	label[100];					// text label

	// transparency rendering (set internally)
	float	camdist;					// distance to camera (used by sorter)
	float	rbound;						// rbound if known, otherwise 0
	mjtByte	transparent;				// treat geom as transparent
};
typedef struct _mjvGeom mjvGeom;


struct _mjvOption						// visualization options, window-specific
{
	int		label;						// what objects to label (mjtLabel)
	int		frame;						// which frame to show (mjtFrame)
	mjtByte	geomgroup[mjNGROUP];		// geom visualization by group
	mjtByte	sitegroup[mjNGROUP];		// site visualization by group
	mjtByte flags[mjNVISFLAG];			// visualization flags (indexed by mjtVisFlag)
};
typedef struct _mjvOption mjvOption;


struct _mjvCameraPose					// low-level camera parameters
{
	mjtNum	head_pos[3];				// head position
	mjtNum	head_right[3];				// head right axis
	mjtNum	window_pos[3];				// window center position
	mjtNum	window_right[3];			// window/monitor right axis
	mjtNum	window_up[3];				// window/monitor up axis
	mjtNum	window_normal[3];			// window/monitor normal axis
	mjtNum  window_size[2];				// physical window size
	mjtNum  scale;						// uniform model scaling rel.to origin
	mjtNum	ipd;						// inter-pupilary distance
};
typedef struct _mjvCameraPose mjvCameraPose;


struct _mjvCamera						// camera control, window-specific
{
	// constant parameters
	mjtNum	fovy;						// y-field of view (deg)

	// camera id, trackbody id
	int		camid;						// fixed camera id; -1: free
	int		trackbodyid;				// body id to track; -1: no tracking

	// free camera parameters, used to compute camera pose
	mjtNum	lookat[3];					// where the camera is looking
	mjtNum	azimuth;					// camera azimuth (in DEG)
	mjtNum	elevation;					// camera elevation (in DEG)
	mjtNum	distance;					// distance to lookat point

	// physical parameters that determine actual rendering
	mjvCameraPose pose;					// head, window, scale, ipd

	mjtByte VR;							// VR mode: use pose directly
};
typedef struct _mjvCamera mjvCamera;


struct _mjvLight						// light
{
	float   pos[3];						// position rel. to body frame				
	float   dir[3];						// direction rel. to body frame				
	float   attenuation[3];				// OpenGL attenuation (quadratic model)		
	float   cutoff;						// OpenGL cutoff							
	float   exponent;					// OpenGL exponent							
	float   ambient[3];					// ambient rgb (alpha=1)					
	float   diffuse[3];					// diffuse rgb (alpha=1)					
	float   specular[3];				// specular rgb (alpha=1)
	mjtByte headlight;					// headlight
	mjtByte directional;				// directional light						
	mjtByte castshadow;					// does light cast shadows					
};
typedef struct _mjvLight mjvLight;


//------------------------------- 2D visualization --------------------------------------

typedef enum _mjtLineStyle				// line styles
{
	mjLINE_SOLID = 0,					// solid line
	mjLINE_DOT							// dotted line
} mjtLineStyle;


struct _mjvLineSpec						// style specification of one line
{
	mjtByte show;						// show/hide this line
	int		lineStyle;					// line style (mjtLineStyle)
	float	linewidth;					// line width
	float	rgba[4];					// color and transparency
	char	label[100];					// text label
};
typedef struct _mjvLineSpec mjvLineSpec;


struct _mjvLineData						// data for all lines in one figure plot
{
	int		nline;						// number of currently used lines
	int		ndata;						// number of points in circular buffer
	int		offset;						// offset of first point in circular buffer
	float	x[mjLINEBUF];				// x coordinates (common to all lines)
	float	y[mjMAXLINE][mjLINEBUF];	// y coordinates
};
typedef struct _mjvLineData mjvLineData;


struct _mjvRender2						// rendering options, window-specific
{
	// parameters set by user
	mjtByte pause;						// ask source process to stop changing data
	mjtByte	group;						// group all lines in one plot
	mjtByte	normalize;					// normalize y values to (0,1) when grouping
	mjtByte serial;						// ignore x-data, plot in serial order
	mjtByte legend;						// draw legend
	mjtByte transp;						// make legend transparent
	mjtByte label;						// print x and y tick labels
	mjtByte yauto;						// automatic yrange
	mjtByte xgrid;						// show grid lines for x ticks
	mjtByte ygrid;						// show grid lines for y ticks
	int		xticks;						// number of x ticks (uniform spacing); 0: disable
	int		yticks;						// number of y ticks (uniform spacing)
	int		margin;						// margin size in pixels
	int		digits;						// number of decimal digits in labels
	float	rgbaLegend[4];				// legend background
	float	rgbaFigure[4];				// figure background
	float	rgbaMark[4];				// marking color (axes, ticks, labels)
	float	rgbaMargin[4];				// margin color
	int		nrange[2];					// range of points to draw
	float	yrange[2];					// current range for y-axis

	// legend parameters set by renderer; used for mouse selection
	int		left;						// left edge of legend
	int		top;						// top edge of legend
	int		right;						// right edge of legend
	int		bottom;						// bottom edge of legend
	int		nline;						// number of lines shown
	int		height;						// character height
	int		gap;						// gap between legend lines
};
typedef struct _mjvRender2 mjvRender2;
