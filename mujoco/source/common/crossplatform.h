//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once


// number of pre-allocated syncrhonization objects
#define mjMAXPAR 16


//------------------------- OS-specific include, libraries, defines --------------------

#ifdef _WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#include <mmsystem.h>
	#include <commdlg.h>
	#include <winsock2.h>
	#include <ws2tcpip.h>
	
	#define PATHSYMBOL '\\'

#else
	#include <pthread.h>
	#include <semaphore.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netinet/tcp.h>
	#include <arpa/inet.h>
	#include <errno.h>
	#include <netdb.h>
	#include <unistd.h>
	#include <X11/Xlib.h>

	#define SOCKET int
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	#define WSAEINPROGRESS EINPROGRESS
	#define WSAEWOULDBLOCK EWOULDBLOCK
	#define closesocket close

	#define PATHSYMBOL '/'
#endif



//------------------------- Socket functions -------------------------------------------

// last socket error
int mjSocketError(void);

// init sockets (needed in Windows only)
void mjInitSockets(void);

// clean sockets (needed in Windows only)
void mjClearSockets(void);

// set blocking mode for socket
void mjSetBlocking(SOCKET s, bool block);


//---------------------------- Critical Section -----------------------------------------

// section object
class mjCriticalSection
{
public:
	mjCriticalSection();
	~mjCriticalSection();

	void Enter(void);
	void Leave(void);
	bool TryEnter(void);

private:
#ifdef _WIN32
	CRITICAL_SECTION cs;
#else
	pthread_mutex_t mtx;
#endif
};


// lock critical section
class mjCriticalSectionLock
{
public:
	mjCriticalSectionLock(mjCriticalSection* pcs);
	~mjCriticalSectionLock();

private:
	mjCriticalSection* pcs;
};



//---------------------------- Events -------------------------------------------------

// single event
class mjEvent
{
public:
	mjEvent();
	~mjEvent();

	bool Get(void);
	void Set(void);
	void Reset(void);
	bool Wait(int msec = -1);

private:
#ifdef _WIN32
	HANDLE ev;
#else
	pthread_mutex_t mtx;
	pthread_cond_t cnd;
	struct timespec tmout;
	bool state;
#endif
};


// multiple events, waitall functionality
class mjEventMulti
{
public:
	// initialization
	mjEventMulti();
	~mjEventMulti();
	void Init(int n);

	// user api
	bool Get(int i);
	void Set(int i);
	void ResetAll(void);
	void WaitAll(void);

private:
	bool initialized;			// is everything initialized by init()
	int n;						// number of events
#ifdef _WIN32
	HANDLE ev[mjMAXPAR];
#else
	pthread_mutex_t mtx;
	pthread_cond_t cnd;
	bool state[mjMAXPAR];
	int numset;
#endif
};



//---------------------------- Semaphores -----------------------------------------------

class mjSemaphore
{
public:
	mjSemaphore(int initCount = 0);
	~mjSemaphore();

	bool Wait(int msec = -1);		// -1: infinite wait; 0: return immediately
	void Increase(int count = 1);

private:
#ifdef _WIN32
	HANDLE sem;
#else
	sem_t sem;
	struct timespec tmout;
#endif
};



//---------------------------- Threads --------------------------------------------------

// thread function pointer type
typedef void (*mjtThreadFuncPtr) (int id, void* userdata);

// thread class
class mjThread
{
public:
	// constructor
	mjThread();

	// run user-supplied function; true: no error
	bool Start(mjtThreadFuncPtr function, void* userdata, 
			   int id = 0, const char* name = 0);

	// wait for thread to terminate; true: no error
	bool Wait(void);

	// check if thread is running
	bool IsStarted(void)	{return started;};

private:
	bool started;						// thread has been started
	int id;								// thread id specified by user
	char name[17];						// thread name specified by user
	void* pptr[4];						// pointers passed to thread func
#ifdef _WIN32
	HANDLE handle;						// thread handle
#else
	pthread_t thread;					// thread object
#endif
};


// exit current thread
void mjThreadExit(void);


// get the number of processor cores on the local machine
int mjGetNCores(void);


//---------------------------- Timing ---------------------------------------------------

// initialize 1 msec timer (on Windows), record base time
void mjBeginTime(void);

// close timer (on Windows)
void mjEndTime(void);

// get time in milliseconds scince mjBeginTime
int mjGetTime(void);

// get time in microseconds since mjBeginTime
int mjGetTimeHR(void);

// sleep for given number of milliseconds
void mjSleep(unsigned int msec);
