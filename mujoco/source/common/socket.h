//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once

#include "common/crossplatform.h"


typedef enum _mjtSoc		// results from socket send/receive
{
	mjSOC_OK = 0,			// success
	mjSOC_TIMEOUT,			// timeout
	mjSOC_CLOSED			// socket is in closed (or otherwise bad) state
} mjtSoc;


class mjSocket				// socket functionality
{
public:
	mjSocket();
	~mjSocket();

	// active: try to connect
	bool connectClient(int tmout = -1, const char* host = 0); 

	// passive: listen for connections
	bool connectServer(int tmout = -1, bool print = false, 
					   const char* hostServer = 0);

	// clear socket
	void clear(void);

	// flush input
	void flushInput(void);

	// send buffer; return mjtSoc or socket error if negative
	int sendBuffer(const char* buf, int len, int tmout = -1);

	// receive buffer; return mjtSoc or socket error if negative
	int recvBuffer(char* buf, int len, int tmout = -1);

	// read-only access
	SOCKET getSoc(void)	{return soc;}
	bool getState(void)	{return state;}

	//---------------- utility functions

	// getaddrinfo for host (default: this machine)
	struct addrinfo* getHost(bool passive, const char* port, const char* host = 0);

	// make connection
	bool tryConnect(const char* port, const char* host, int tmout);

	// use select() to wait for socket read/write, true if not timeout
	bool waitSocket(SOCKET s, bool read, int tmout);

	//---------------- settings

	char portListen[100];			// port for listening (user can change the default)
	bool verbose;					// print diagnostics
	int tmoutTryConnect;			// timeout for each connect attempt
	bool (*userexit)(void);			// check if user wants to exit before listen timeout

private:
	SOCKET	soc;					// socket object
	bool	state;					// last known state (true: connected)
	char	dummy[1000];			// used to flush input
};
