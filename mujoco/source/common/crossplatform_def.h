//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once

// Windows
#ifdef _WIN32
//	#define isnan _isnan
	#define strcasecmp _stricmp

	#define aligned_malloc(ptr, size, align) ptr = _aligned_malloc(size, align)
	#define aligned_free(ptr) _aligned_free(ptr)

	#define mjQUICKSORT(buf, elnum, elsz, func, context) \
		qsort_s(buf, elnum, elsz, func, context)
	#define quicksortfunc(name, context, el1, el2) \
		static int name(void* context, const void* el1, const void* el2)

// Unix-common
#else
	#define aligned_malloc(ptr, size, align) posix_memalign(&ptr, align, size)
	#define aligned_free(ptr) free(ptr)

// Apple
#ifdef __APPLE__
	#define mjQUICKSORT(buf, elnum, elsz, func, context) \
		qsort_r(buf, elnum, elsz, context, func)
	#define quicksortfunc(name, context, el1, el2) \
		static int name(void* context, const void* el1, const void* el2)

// non-Apple
#else
	#define mjQUICKSORT(buf, elnum, elsz, func, context) \
		qsort_r(buf, elnum, elsz, func, context)
	#define quicksortfunc(name, context, el1, el2) \
		static int name(const void* el1, const void* el2, void* context)
#endif
#endif

