//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


//------------------------- class mjCMesh -----------------------------------------------
// Describes a mesh

class mjCMesh : public mjCBase
{
	friend class mjCGeom;
	friend class mjCBody;
	friend class mjCModel;
	friend class mjXWriter;

public:
	void GetPos(double* pos);					// get position
	void GetQuat(double* quat);					// get orientation
	void FitGeom(mjCGeom* geom);				// approximate mesh with simple geom

	double scale[3];					// rescale mesh
	bool center;						// subtract mesh CoM from vertex data
	bool align;							// rotate vertex data into SVD frame
	int fittype;						// type of fitted geom; mjGEOM_NONE: no fit
	int fitcontype;						// contype of fitted geom
	int fitconaffinity;					// conaffinity of fitted geom
	int fitgroup;						// group of fitted geom
	bool fitaabb;						// fit to AABB or to inertial box
	double fitscale;					// rescale fitted geom

	string filename;					// mesh filename
	mjCGeom* pgeomfit;					// pointer to geom that will be fitted

private:
	mjCMesh(mjCModel* model, mjCDef* def);		// constructor
	~mjCMesh();									// destructor
	void Compile(void);							// compiler
	void LoadSTL(void);							// load mesh in STL BIN format
	void MakeGraph(void);						// make graph of convex hull
	void Process(void);							// apply transformations

	// mesh properties computed by Compile
	double pos[3];						// CoM position
	double quat[4];						// inertia orientation
	double boxsz[3];					// half-sizes of equivalent inertia box
	double aabb[3];						// half-sizes of axis-aligned bounding box
	bool isvisual;						// no convex hull for visual meshes

	// mesh data to be copied into mjModel
	int nvert;							// number of vertices
	int nface;							// number of faces
	int szgraph;						// size of graph data in ints
	float* vert;						// vertex data (3*nvert), relative to (pos, quat)
	int* face;							// face vertex indices (3*nface)
	int* graph;							// convex graph data

	bool used;							// keep track of used meshes in mesh removal
};


//------------------------- class mjCHField ---------------------------------------------
// Describes a height field

class mjCHField : public mjCBase
{
	friend class mjCModel;
	friend class mjXWriter;

public:
	bool dir;									// 0: (x,y) to (x+1,y+1); 1: opposite

	string datafile;							// data file: contains elevation data
												//  nrow, ncol, <elevation data>
	int nrowuser;								// user-defined size, when datafile is missing
	int ncoluser;

private:
	mjCHField(mjCModel* model); 				// constructor
	~mjCHField();								// destructor
	void Compile(void);							// compiler

	// hfield data to be copied into mjModel
	int nrow;							// number of rows
	int ncol;							// number of columns
	float* data;						// elevation data, row-major format
};
