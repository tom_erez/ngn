//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once


// disable floating point truncation/conversion warnings
#pragma warning (disable: 4244)
#pragma warning (disable: 4305)


// lower level
#include "engine/engine.h"
#include "common/crossplatform_def.h"

// external
#include <string>
#include <vector>
using namespace std;

// user module
#include "user/user_util.h"
#include "user/user_objects.h"
#include "user/user_mesh.h"
#include "user/user_model.h"
