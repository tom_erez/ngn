//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


// forward declarations of all mjC classes
class mjCError;
class mjCAlternative;
class mjCBase;
class mjCBody;
class mjCJoint;
class mjCGeom;
class mjCSite;
class mjCCollision;
class mjCConstraint;
class mjCTendon;
class mjCWrap;
class mjCActuator;
class mjCNumeric;
class mjCMesh;						// defined in user_mesh
class mjCModel;						// defined in user_model

class mjXWriter;					// defined in xml_native
class mjXURDF;						// defined in xml_urdf


//------------------------- helper classes and constants --------------------------------

// number of size parameters for each geom type
const int mjGEOMINFO[mjNGEOMTYPES] = {3, 3, 1, 2, 3, 2, 3, 0};


// error information
class mjCError
{
public:
	mjCError(const mjCBase* obj = 0,
			 const char* msg = 0, 
			 const char* str = 0, 
			 int pos1 = 0,
			 int pos2 = 0);

	char message[500];				// error message
	bool warning;					// is this a warning instead of error
};


// alternative specifications of frame orientation
class mjCAlternative
{
public:
	mjCAlternative();								// constuctor
	const char* Set(double* quat, double* inertia,	// set frame quat and diag. inertia
				    bool degree,					//  angle format: degree/radian
					const char* sequence);			//  euler sequence format: "xyz"	

	double axisangle[4];			// rotation axis and angle
	double xyaxes[6];				// x and y axes
	double zaxis[3];				// z axis (use minimal rotation)
	double euler[3];				// euler rotations
	double fullinertia[6];			// non-axis-aligned inertia matrix
};


class mjCDef						// default settings class
{
public:
	mjCDef();

	// identifiers
	string name;					// class name
	int parentid;					// id of parent class
	vector<int> childid;			// ids of child classes

	// joint defaults
	bool joint_islimited;			// does joint have limits
	bool joint_isfrictional;		// does joint have friction
	double joint_range[2];			// joint limits
	double joint_compliance;		// epsilon parameter
	double joint_timeconst;			// kappa parameter
	double joint_mindist;			// mindist value for joint limit detection
	double joint_stiffness;			// joint stiffness coefficient
	double joint_armature;			// joint armature
	double joint_damping;	 		// joint damping coefficient
	double joint_frictionloss;		// friction loss
	double joint_maxvel;			// maximum velocity
	bool joint_makeactuator;		// add default actuator automatically
	string joint_actuatorclass;		// default class for make actuator

	// geom defaults
	int geom_contype;				// contact type
	int geom_conaffinity;			// contact affinity
	int geom_condim;				// contact dimensionality
	int geom_group;					// group for visualization
	double geom_friction[3];		// one-sided contact friction
	double geom_compliance;			// epsilon parameter
	double geom_timeconst;			// kappa parameter
	double geom_mindist;			// mindist value for contact detection
	double geom_density;			// geom density
	float geom_rgba[4];				// geom color

	// mesh defaults (specific to meshes)
	double mesh_scale[3];			// rescale mesh
	bool mesh_center;				// subtract mesh CoM from vertex data
	bool mesh_align;				// rotate vertex data into SVD frame
	int mesh_fittype;				// type of fitted geom; mjGEOM_NONE: none
	int mesh_fitcontype;			// contype of fitted geom
	int mesh_fitconaffinity;		// conaffinity of fitted geom
	int mesh_fitgroup;				// group of fitted geom
	bool mesh_fitaabb;				// fit to AABB or to inertial box
	double mesh_fitscale;			// rescale fitted geom

	// constraint defaults
	bool eq_isactive;				// is constraint active
	double eq_compliance;			// epsilon parameter
	double eq_timeconst;			// kappa parameter

	// tendon defaults
	bool tendon_islimited;			// does tendon have limits
	double tendon_range[2];			// length limits
	double tendon_compliance;		// epsilon parameter
	double tendon_timeconst;		// kappa parameter
	double tendon_mindist;			// mindist value for tendon limit detection
	double tendon_stiffness;		// stiffness coefficient
	double tendon_damping;			// damping coefficient
	bool tendon_makeactuator;		// add default actuator automatically
	string tendon_actuatorclass;	// default class for make actuator

	// actuator defaults
	bool actuator_isctrllimited;	// does actuator have control limits
	bool actuator_isforcelimited;	// does actuator have force limits
	mjtDyn actuator_dyntype;		// dynamics type
	mjtTrn actuator_trntype;		// transmission type
	mjtGain actuator_gaintype;		// gain type
	mjtBias actuator_biastype;		// bias type
	double actuator_dynprm[mjNDYN];	// dynamics parameters
	double actuator_trnprm[mjNTRN];	// transmission parameters
	double actuator_gainprm[mjNGAIN];// gain
	double actuator_biasprm[mjNBIAS];// bias
	double actuator_ctrlrange[2];	// force range
	double actuator_forcerange[2];	// force range
};



//------------------------- class mjCBase -----------------------------------------------
// Generic functionality for all derived classes

class mjCBase
{
public:
	string name;					// object name
	int id;							// object id
	int xmlpos[2];					// row and column in xml file

	mjCDef* def;					// defaults class used to init this object
	bool explicitdef;				// defaults class was specified explicitly

protected:
	mjCBase();							// constructor
	mjCModel* model;				// pointer to model that created object
};



//------------------------- class mjCBody -----------------------------------------------
// Describes a rigid body

class mjCBody : public mjCBase
{
	friend class mjCJoint;
	friend class mjCGeom;
	friend class mjCSite;
	friend class mjCConstraint;
	friend class mjCCollision;
	friend class mjCModel;
	friend class mjXWriter;
	friend class mjXURDF;

public:
	// API for adding objects to body
	mjCBody* AddBody(mjCDef* childdef, string name = "");
	mjCJoint* AddJoint(mjCDef* def, string name = "", mjtJoint type = mjJNT_NONE);
	mjCGeom* AddGeom(mjCDef* def, string name = "", mjtGeom type = mjGEOM_NONE);
	mjCSite* AddSite(string name = "");

	// API for accessing objects
	int NumObjects(mjtObj type);
	mjCBase* GetObject(mjtObj type, int id);
	mjCBase* FindObject(mjtObj type, string name, bool recursive = true);

	// setup child local frame, take into account change
	void MakeLocal(double* locpos, double* locquat, 
		const double* pos, const double* quat);

	// variables set by user or 'Compile'
	double pos[3];					// frame position
	double quat[4];					// frame orientation
	double ipos[3];					// inertial frame position
	double iquat[4];				// inertial frame orientation
	double mass;					// mass
	double inertia[3];				// diagonal inertia (in i-frame)
	vector<double> userdata;		// user data
	mjCAlternative alt;				// alternative orientation specification
	mjCAlternative ialt;			// alternative for inertial frame

	// variables computed by 'Compile' and 'AddXXX'
private:
	mjCBody(mjCModel*);					// constructor
	~mjCBody();							// destructor
	void Compile(void);					// compiler

	void GeomFrame(void);				// get inertial info from geoms

	double locpos[3];				// position relative to parent
	double locquat[4];				// orientation relative to parent
	double locipos[3];				// inertial position frame, rel. to local frame
	double lociquat[4];				// inertial frame orientation
	int parentid;					// parent index in global array
	int weldid;						// top index of body we are welded to
	int dofnum;						// number of motion dofs for body

	int lastdof;					// id of last dof (used by compiler)

	// objects allocated by Add functions
	vector<mjCBody*> bodies;		// child bodies
	vector<mjCGeom*> geoms;			// geoms attached to this body
	vector<mjCJoint*> joints;		// joints allowing motion relative to parent
	vector<mjCSite*> sites;			// sites attached to this body
};



//------------------------- class mjCJoint ----------------------------------------------
// Describes a motion degree of freedom of a body relative to its parent

class mjCJoint : public mjCBase
{
	friend class mjCConstraint;
	friend class mjCBody;
	friend class mjCModel;
	friend class mjXWriter;
	friend class mjXURDF;

public:
	// variables set by user: joint properties
	mjtJoint type;					// type of Joint
	bool islimited;					// does joint have limits
	bool isfrictional;				// does joint dofs have friction
	double pos[3];					// anchor position
	double axis[3];					// joint axis
	double stiffness;				// stiffness coefficient
	double range[2];				// joint limits
	double compliance;				// epsilon parameter
	double timeconst;				// kappa parameter
	double mindist;					// mindist value for joint limit detection
	double ref;						// value at reference configuration: qpos_spring
	double springref;				// spring reference point:qpos_spring
	vector<double> userdata;		// user data
	bool makeactuator;				// auto-make a default actuator

	// variables set by user: dof properties
	double armature;				// armature inertia (mass for slider)
	double damping;					// damping coefficient
	double frictionloss;			// friction loss
	double maxvel;					// max abs velocity; negative- no maximum

	double urdfeffort;				// store effort field from urdf

private:
	mjCJoint(mjCModel*, mjCDef* def);	// constructor
	int Compile(void);					// compiler; return dofnum

	mjCBody* body;					// joint's body
	double locpos[3];				// anchor position in child or parent
	double locaxis[3];				// joint axis in child or parent
};



//------------------------- class mjCGeom -----------------------------------------------
// Describes a geometric shape belonging to a body

class mjCGeom : public mjCBase
{
	friend class mjCMesh;
	friend class mjCCollision;
	friend class mjCBody;
	friend class mjCModel;
	friend class mjXWriter;
	friend class mjXURDF;

public:
	double GetVolume(void);				// compute geom volume
	void SetInertia(void);				// compute and set geom inertia

	// variables set by user and copied into mjModel
	mjtGeom type;					// geom type
	int contype;					// contact type
	int conaffinity;				// contact affinity
	int condim;						// contact dimensionality
	int meshid;						// id of geom's mesh (-1: no mesh)
	int hfieldid;					// id of geom's hfield (-1: no hfield)
	int group;						// used for rendering
	double size[3];					// geom-specific size parameters
	float  rgba[4];					// RGB color
	double friction[3];				// one-sided friction coefficients: slide, roll, spin
	double compliance;				// epsilon parameter
	double timeconst;				// kappa parameter
	double mindist;					// mindist value for contact detection
	vector<double> userdata;		// user data

	// variables set by user and used during compilation
	double _mass;					// used to compute density
	double density;					// used to compute mass and inertia (from volume)
	double fromto[6];				// alternative for capsule and cylinder
	mjCAlternative alt;				// alternative orientation specifications

	// variables set by user or 'Compile1'
	double pos[3];					// position
	double quat[4];					// orientation

private:
	mjCGeom(mjCModel*, mjCDef* def);	// constructor
	void Compile(void);					// compiler
	double GetRBound(void);				// compute bounding sphere radius

	double mass;					// mass
	double inertia[3];				// local diagonal inertia
	double locpos[3];				// local position
	double locquat[4];				// local orientation
	mjCBody* body;					// geom's body
};



//------------------------- class mjCSite -----------------------------------------------
// Describes a site on a body

class mjCSite : public mjCBase
{
	friend class mjCBody;
	friend class mjCModel;
	friend class mjXWriter;
	friend class mjXURDF;

public:
	// variables set by user
	int group;						// group id, used for visualization
	double pos[3];					// position
	double quat[4];					// orientation
	vector<double> userdata;		// user data
	mjCAlternative alt;				// alternative orientation specification

	// variables computed by 'compile' and 'mjCBody::addSite'
private:
	mjCSite(mjCModel*);				// constructor
	void Compile(void);				// compiler

	mjCBody* body;					// site's body
	double locpos[3];				// local position
	double locquat[4];				// local orientation
};



//------------------------- class mjCCamera ---------------------------------------------
// Describes a camera, attached to a body, geom or site

class mjCCamera : public mjCBase
{
	friend class mjCModel;
	friend class mjXWriter;

public:
	// variables set by user
	mjtObj objtype;					// type of attachment object
	string objname;					// name of attachment object
	int resolution[2];				// resolution
	double fov[2];					// field of view
	double ipd;						// inter pupilary distance

	// variables computed by 'compile'
private:
	mjCCamera(mjCModel*);			// constructor
	void Compile(void);				// compiler

	int objid;						// id of attachment object
};



//------------------------- class mjCCollision ------------------------------------------
// Enables and disables pairwise collisions between geoms beyond the default

class mjCCollision : public mjCBase
{
	friend class mjCBody;
	friend class mjCModel;

public:
	// variables set by user
	string name1;					// name of geom 1
	string name2;					// name of geom 2
	bool enable;					// enable or disable

	int condim;						// contact dimensionality
	double compliance;				// epsilon parameter
	double timeconst;				// kappa parameter
	double mindist;					// mindist value for contact detection
	double friction[5];				// full contact friction

private:
	mjCCollision(mjCModel*);			// constructor
	void Compile(void);					// compiler

	int geom1;						// id of geom1
	int geom2;						// id of geom2
};



//------------------------- class mjCConstraint -----------------------------------------
// Describes an equality constraint

class mjCConstraint : public mjCBase
{
	friend class mjCBody;
	friend class mjCModel;
	friend class mjXWriter;

public:
	// variables set by user
	mjtEq type;						// constraint type
	string type1;					// type name of object 1
	string type2;					// type name of object 2
	string name1;					// name of object 1
	string name2;					// name of object 2
	bool isactive;					// initial activation state
	double compliance;				// epsilon parameter
	double timeconst;				// kappa parameter
	double data[6];					// type-dependent data  
	vector<double> userdata;		// user data

	// variables computed by 'compile' or set by user (for custom type)
	int size;						// number of scalar constraints (3, 1, custom)
	int ndata;						// size of data array

private:
	mjCConstraint(mjCModel*, mjCDef* def);	// constructor
	void Compile(void);						// compiler

	int obj1type;					// type of object 1
	int obj2type;					// type of object 2
	int obj1id;						// id of object 1
	int obj2id;						// id of object 2
};



//------------------------- class mjCTendon ---------------------------------------------
// Describes a tendon

class mjCTendon : public mjCBase
{
	friend class mjCModel;
	friend class mjXWriter;
	
public:
	// API for adding wrapping objects
	void WrapSite(string name);					// site
	void WrapGeom(string name, string side);	// geom
	void WrapJoint(string name, double coef);	// joint
	void WrapPulley(double divisor);			// pulley

	// API for access to wrapping objects
	int NumWraps(void);							// number of wraps
	mjCWrap* GetWrap(int);						// pointer to wrap

	// variables set by user
	bool islimited;					// does tendon have limits
	double compliance;				// epsilon parameter
	double timeconst;				// kappa parameter
	double range[2];				// length limits
	double mindist;					// mindist value for tendon limit detection
	double stiffness;				// stiffness coefficient
	double damping;					// damping coefficient
	vector<double> userdata;		// user data
	bool makeactuator;				// auto-make a default actuator
	
private:
	mjCTendon(mjCModel*, mjCDef* def);			// constructor
	~mjCTendon();								// destructor
	void Compile(void);							// compiler

	vector<mjCWrap*> path;			// wrapping objects
};



//------------------------- class mjCWrap -----------------------------------------------
// Describes a tendon wrap object

class mjCWrap : public mjCBase
{
	friend class mjCTendon;
	friend class mjCModel;

public:
	mjtWrap type;					// wrap object type
	int objid;						// wrap object id (in array corresponding to type)
	int sideid;						// side site id; -1 if not applicable
	double prm;						// parameter: divisor, coefficient
	string sidesite;				// name of side site

private:
	mjCWrap(mjCModel*, mjCTendon*);		// constructor
	void Compile(void);					// compiler

	mjCTendon* tendon;				// tendon owning this wrap
};



//------------------------- class mjCActuator -------------------------------------------
// Describes an actuator

class mjCActuator : public mjCBase
{
	friend class mjCModel;
	friend class mjXWriter;

public:
	// API for instantiation of default actuator types
	void SetMotor(string joint, double gear);
	void SetMuscle(string tendon, double tau, double flv);
	void SetPneumatic(string tendon, double tau);
	void SetSliderCrank(string joint, double tau,
		double rod, double crank, double offset, double angle, bool reverse);

	// variables set by user or API
	bool isctrllimited;				// are control limits defined
	bool isforcelimited;			// are force limits defined
	mjtDyn dyntype;					// dynamics type
	mjtTrn trntype;					// transmission type
	mjtGain gaintype;				// gain type
	mjtBias biastype;				// bias type
	double dynprm[mjNDYN];			// dynamics parameters
	double trnprm[mjNTRN];			// transmission parameters
	double gainprm[mjNGAIN];		// gain parameters
	double biasprm[mjNGAIN];		// bias parameters
	double ctrlrange[2];			// control range
	double forcerange[2];			// force range
	vector<double> userdata;		// user data
	string target;					// transmission target name (e.g. tendon name)
	string slidersite;				// site defining cylinder, for slidercrank only

private:
	mjCActuator(mjCModel*, mjCDef* def);// constructor
	void Compile(void);					// compiler

	int trnid[2];					// id of transmission target
};



//------------------------- class mjCNumeric ---------------------------------------------
// Describes a custom data field

class mjCNumeric : public mjCBase
{
	friend class mjCModel;

public:
	// variables set by user
	vector<double> data;			// initialization data
	int size;						// array size, can be bigger than data.size()

private:
	mjCNumeric(mjCModel*);				// constructor
	~mjCNumeric();						// destructor
	void Compile(void);					// compiler
};



//------------------------- class mjCText -----------------------------------------------
// Describes a custom text field

class mjCText : public mjCBase
{
	friend class mjCModel;

public:
	// variables set by user
	string data;					// string

private:
	mjCText(mjCModel*);					// constructor
	~mjCText();							// destructor
	void Compile(void);					// compiler
};
