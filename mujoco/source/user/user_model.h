//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


struct _mjContactPair				// predefined contact pair
{
	int dim;						// same as in mjContact
	int geom1;						// id of geom1
	int geom2;						// id of geom2
	mjtNum compliance;				// epsilon parameter
	mjtNum timeconst;				// kappa parameter
	mjtNum mindist;					// safety distance for collision detection
	mjtNum friction[5];				// tangent1, 2, spin, roll1, 2
};

typedef struct _mjContactPair mjContactPair;


typedef enum mjtInertiaFromGeom
{
	mjINERTIAFROMGEOM_FALSE = 0,	// do not use; inertial element required
	mjINERTIAFROMGEOM_TRUE,			// always use; overwrite inertial element
	mjINERTIAFROMGEOM_AUTO			// use only if inertial element is missing
};



//------------------------- class mjCModel --------------------------------------
// mjCModel contains everything needed to generate the low-level model.
// It can be constructed manually by calling 'Add' functions and setting
// the public fields of the various objects.  Alternatively it can constructed
// by loading an XML file via mjCXML.  Once an mjCModel object is
// constructed, 'Compile' can be called to generate the corresponding mjModel object
// (which is the low-level model).  The mjCModel object can then be deleted.

class mjCModel
{
	friend class mjCBody;
	friend class mjCJoint;
	friend class mjCGeom;
	friend class mjCMesh;
	friend class mjCHField;
	friend class mjCCollision;
	friend class mjCSite;
	friend class mjCConstraint;
	friend class mjCTendon;
	friend class mjCWrap;
	friend class mjCActuator;
	friend class mjCNumeric;
	friend class mjXWriter;

public:
	mjCModel();									// constructor
	~mjCModel();								// destructor

	CLASS_DECLSPEC mjModel* Compile(void);						// COMPILER: construct mjModel
	bool CopyBack(const mjModel*, const mjOption*); // DECOMPILER: copy numeric data back

	//------------------------ API for adding model elements
	mjCCamera* AddCamera(string = "");			// add camera
	mjCMesh* AddMesh(mjCDef* def, string file);	// add mesh
	mjCHField* AddHField(string file);			// add heightfield
	mjCCollision* AddCollision(void);			// add collision
	mjCConstraint* AddConstraint(mjCDef* def, string = "");	// add constraint
	mjCTendon* AddTendon(mjCDef* def, string = "");			// add tendon
	mjCActuator* AddActuator(mjCDef* def, string = "");		// add actuator
	mjCNumeric* AddNumeric(string = "");			// add custom
	mjCText* AddText(string = "");				// add text

	//------------------------ API for access to model elements (outside tree)
	int NumObjects(mjtObj type);				// number of objects in specified list
	mjCBase* GetObject(mjtObj type, int id);	// pointer to specified object

	//------------------------ API for access to other variables
	bool IsCompiled(void);						// is model already compiled
	int GetFixed(void);							// number of fixed massless bodies
	CLASS_DECLSPEC mjCError GetError(void);					// copy of error object
	mjCBody* GetWorld(void);					// pointer to world body
	mjCDef* FindDef(string name);				// find default class name
	mjCDef* AddDef(string name, int parendit);	// add default class to array
	mjCBase* FindObject(mjtObj type, string name); // find object given type and name

	//------------------------ global data
	string modelfiledir;			// path to model file
	vector<mjCDef*> defaults;		// settings for each defaults class

	//------------------------ compiler settings
	double boundmass;				// enfore minimum body mass
	double boundinertia;			// enfore minimum body diagonal inertia
	double settotalmass;			// rescale masses and inertias; <=0: ignore
	bool balanceinertia;			// automatically impose A + B >= C rule
	bool uniquevertex;				// remove repeated mesh vertices
	bool strippath;					// automatically strip paths from mesh files
	bool global;					// local or global coordinates
	bool degree;					// angles in radians or degrees
	char euler[3];					// sequence for euler rotations
	string meshdir;					// mesh directory
	bool discardvisualmesh;			// discard visual meshes (parser only)
	bool discardvisualgeom;			// discard visual geoms (parser only)
	bool convexhull;				// compute mesh convex hulls
	int inertiafromgeom;			// use geom inertias (mjtInertiaFromGeom)

	//------------------------ collision settings
	bool makepair;					// make explicit contact pair array
	bool defaultstate;				// default pair state

	//------------------------ engine data
	string modelname;				// model name
	mjOption option;				// options
	int nlmax;						// max number of limits in mjData
	int ncmax;						// max number of contacts in mjData
	int njmax;						// max number of Jacobian rows in mjData
	int nctotmax;					// max number of total contacts (size of mjContact array)
	int nuser_body;					// number of mjtNums in body_user
	int nuser_jnt;					// number of mjtNums in jnt_user
	int nuser_geom;					// number of mjtNums in geom_user
	int nuser_site;					// number of mjtNums in site_user
	int nuser_eq;					// number of mjtNums in eq_user
	int nuser_tendon;				// number of mjtNums in tendon_user
	int nuser_actuator;				// number of mjtNums in actuator_user
	int nstack;						// number of fields in mjData stack
	int nuserdata;					// number extra fields in mjData

	// keyframe data
	vector<mjtNum> key_qpos[mjNKEY];	// keyframe qpos
	vector<mjtNum> key_qvel[mjNKEY];	// keyframe qvel
	vector<mjtNum> key_act[mjNKEY];		// keyframe act
	
private:
	void Clear(void);				// clear objects allocated by Compile

	template <class T>				// add object of any type,
	T* AddObject(vector<T*>& list, string type, string name);

	template <class T>				// add object of any type, with def parameter
	T* AddObjectDef(vector<T*>& list, string type, mjCDef* def, string name);

	//------------------------ compile phases
	void MakeLists(mjCBody* body);	// make lists of bodies, geoms, joints, sites
	void RemoveUnusedMeshes(void);	// remove meshes that were only used for geom fitting
	void MakePairs(void);			// make collision pair array
	void SetSizes(void);			// compute sizes
	void CopyNames(mjModel*);		// copy names, compute name addresses
	void CopyObjects(mjModel*);		// copy objects outside kinematic tree
	void CopyTree(mjModel*);		// copy objects inside kinematic tree

	//------------------------ sizes
	// sizes set from object list lengths
	int nbody;						// number of bodies
	int njnt;						// number of joints
	int ngeom;						// number of geoms
	int nmesh;						// number of meshes
	int nhfield;					// number of height fields
	int	ncpair;						// number of geom pairs in pair array
	int nsite;						// number of sites
	int ncam;						// number of cameras
	int neq;						// number of equality constraints
	int ntendon;					// number of tendons
	int nnumeric;					// number of custom fields
	int ntext;						// number of text fields

	// sizes computed by Compile
	int nq;							// number of generalized coordinates = dim(qpos)
	int nv;							// number of degrees of freedom = dim(qvel)
	int nu;							// number of actuators/controls
	int na;							// number of activation variables
	int nmeshvert;					// number of vertices in all meshes
	int nmeshface;					// number of triangular faces in all meshes
	int nmeshgraph;					// number of shorts in mesh auxiliary data
	int nhfielddata;				// number of data points in all hfields
	int nwrap;						// number of wrap objects in all tendon paths
	int nnumericdata;				// number of mjtNums in all custom fields
	int ntextdata;					// number of chars in all text fields, including 0
	int nnames;						// number of chars in all names
	int nM;							// number of non-zeros in sparse inertia matrix
	int nemax;						// number of potential (scalar) constraints

	//------------------------ object lists
	// objects created here
	vector<mjCCamera*> cameras;		// list of cameras
	vector<mjCMesh*> meshes;		// list of meshes
	vector<mjCHField*> hfields;		// list of height fields
	vector<mjCCollision*> collisions;   // list of collision statements
	vector<mjCConstraint*> constraints; // list of constraints
	vector<mjCTendon*> tendons;		// list of tendons
	vector<mjCActuator*> actuators;	// list of actuators
	vector<mjCNumeric*> numerics;	// list of numeric fields
	vector<mjCText*> texts;			// list of text fields

	// objects created inside kinematic tree
	vector<mjCBody*> bodies;		// list of bodies
	vector<mjCJoint*> joints;		// joints allowing motion relative to parent
	vector<mjCGeom*> geoms;			// geoms attached to this body
	vector<mjCSite*> sites;			// sites attached to this body

	//------------------------ internal variables
	bool compiled;					// already compiled flag (cannot be compiled again)
	mjCError errInfo;				// last error info
	int fixCount;					// how many bodies have been fixed
	vector<mjContactPair> pairs;	// precomputed collision pairs
	vector<mjtNum> qpos0;			// save qpos0, to recognize changed key_qpos in write
};
