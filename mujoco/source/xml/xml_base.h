//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#define CLASS_DECLSPEC

// Main writer function
bool mjWriteXML(mjCModel* model, string filename, char* error);

// Main parser function
CLASS_DECLSPEC mjCModel* mjParseXML(string filename, char* error);

// XML testing function
string mjTestXML(mjCModel* model, mjModel* m, string oldname);


// max number of attribute fields in schema (plus 3)
#define mjXATTRNUM 33


//----------------------------- Helper classes ------------------------------------------

// XML Error info
class mjXError
{
public:
	mjXError(const TiXmlElement* elem = 0,
			 const char* msg = 0, 
			 const char* str = 0,
			 int pos = 0);

	char message[500];				// error message
};


// Custom XML file validation
class mjXSchema
{
public:
	mjXSchema(const char* schema[][mjXATTRNUM], int nrow, 	// constructor
			  bool checkptr = true);
	~mjXSchema();										// destructor
	string GetError(void);								// return error
	void Print(FILE* fp, int level = 0);				// print schema
	void PrintHTML(FILE* fp, int level = 0);			// print schema as HTML table

	TiXmlElement* Check(TiXmlElement* elem);			// validator

private:
	string name;						// element name
	char type;							// element type: '?', '!', '*', 'R'
	vector<string> attr;				// allowed attributes
	vector<mjXSchema*> child;			// allowed child elements

	int refcnt;							// refcount used for validation
	string error;						// error from constructor or Check
};


// key(string) : value(int) map
typedef struct _mjMap
{
	string key;
	int value;
} mjMap;



//----------------------------- Base XML class ------------------------------------------

class mjXBase
{
public:
	mjXBase();

	// parse: implemented in derived parser classes
	virtual void Parse(TiXmlElement* root, string filename) {};	

	// write: implemented in derived writer class
	virtual void Write(string filename) {};

	// set the model allocated externally
	void SetModel(mjCModel*);

	//---------------------------- static helper functions

	// find key in map, return value (-1: not found)
	static int FindKey(const mjMap* map, int mapsz, string key);

	// find value in map, return key ("": not found)
	static string FindValue(const mjMap* map, int mapsz, int value);

	// read double array from attribute, return number read
	static int ReadAttr(TiXmlElement* elem, const char* attr, const int len,
		double* data, string& text, 
		bool required = false, bool exact = true);

	// read float array from attribute, return number read
	static int ReadAttr(TiXmlElement* elem, const char* attr, const int len,
		float* data, string& text, 
		bool required = false, bool exact = true);

	// read text attribute
	static bool ReadAttrTxt(TiXmlElement* elem, const char* attr, string& text, 
		bool required = false);

	// read int attribute
	static bool ReadAttrInt(TiXmlElement* elem, const char* attr, int* data, 
		bool required = false);

	// read alternative orientation specification
	static void ReadAlternative(TiXmlElement* elem, mjCAlternative& alt);

	// find subelement with given name, make sure it is unique
	static TiXmlElement* FindSubElem(TiXmlElement* elem, string name, bool required = false);

	// find attribute, translate key, return int value
	static bool MapValue(TiXmlElement* elem, const char* attr, int* data,
		const mjMap* map, int mapSz, bool required = false);

	// is directory path absolute
	static bool IsAbsPath(string path);

	// get directory path of file
	static string GetFileDir(string filename);

	// get directory path of file
	static string FullName(string filedir, string meshdir, string filename);

	// Tom's functions
	int ReadContent(TiXmlElement* elem, const int len,
		double* data, string& text, bool required, bool exact);
	int ReadSubElem(TiXmlElement* elem, string name, const int len,
		double* data, string& text, bool required, bool exact);
	bool ReadSubElemTxt(TiXmlElement* elem, const string name,
		string& text, bool required); 

	//---------------------------- helper write functions

	// write attribute- double
	void WriteAttr(TiXmlElement* elem, string name, int n, double* data, 
				   const double* def = 0);

	// write attribute- float
	void WriteAttr(TiXmlElement* elem, string name, int n, float* data, 
				   const float* def = 0);

	// write attribute- string
	void WriteAttrTxt(TiXmlElement* elem, string name, string value);

	// write attribute- single int
	void WriteAttrInt(TiXmlElement* elem, string name, int data, int def = -12345);

	// write attribute- keyword
	void WriteAttrKey(TiXmlElement* elem, string name, 
					  const mjMap* map, int mapsz, int data, int def = -12345);


protected:
	mjCModel* model;					// internally-allocated mjCModel object
};
