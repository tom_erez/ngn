//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


//----------------------------- MujoCo native parser ------------------------------------

class mjXMJCF : public mjXBase
{
public:
	mjXMJCF();											// constructor

	void Parse(TiXmlElement* root);						// parse XML document
	void PrintSchema(bool html);						// print schema in text or HTML

	// XML sections embedded in all formats
	static void Compiler(TiXmlElement* section, mjCModel* mod);	// compiler section	
	static void Option(TiXmlElement* section, mjOption* opt);	// option section
	static void Size(TiXmlElement* section, mjCModel* mod);		// size section

private:
	// XML section speicific to MJCF
	void Default(TiXmlElement* section,	int parentid);	// default section
	void Body(TiXmlElement* section, mjCBody* pbody);	// body/world section
	void Keyframe(TiXmlElement* section);				// keyframe section
	void ContactPair(TiXmlElement* section);			// contactpair section
	void Camera(TiXmlElement* section);					// camera section
	void Constraint(TiXmlElement* section);				// constraint section
	void Tendon(TiXmlElement* section);					// tendon section
	void Actuator(TiXmlElement* section);				// actuator section
	void Custom(TiXmlElement* section);					// custom section
	void Text(TiXmlElement* section);					// text section

	mjCDef* GetClass(TiXmlElement* section);			// get default class name
	mjXSchema schema;									// schema used for validation
};


//----------------------------- Mujoco native writer ------------------------------------

class mjXWriter : public mjXBase
{
public:
	// main API: write mjCModel to XML file
	void Write(string filename);

private:
	// XML section writers
	void Compiler(TiXmlElement* root);					// compiler section
	void Default(TiXmlElement* root, mjCDef* def);		// default section
	void Option(TiXmlElement* root);					// option section
	void Size(TiXmlElement* root);						// size section
	void Body(TiXmlElement* elem, mjCBody* body);		// body/world section
	void Keyframe(TiXmlElement* root);					// keyframe section
	void ContactPair(TiXmlElement* root);				// contactpair section
	void Camera(TiXmlElement* root);					// camera
	void Constraint(TiXmlElement* root);				// constraint section
	void Tendon(TiXmlElement* root);					// tendon section
	void Actuator(TiXmlElement* root);					// actuator section
	void Custom(TiXmlElement* root);					// custom section
	void Text(TiXmlElement* root);						// text section
};
