//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once


// lower level
#include "engine/engine.h"
#include "user/user.h"

// external
#include "tinyxml.h"

// xml module
#include "xml/xml_base.h"
#include "xml/xml_native.h"
#include "xml/xml_urdf.h"
