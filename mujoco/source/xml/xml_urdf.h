//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


// rgb data
typedef struct _mjRGBA
{
	float val[4];
} mjRGBA;



// XML parser for URDF files
class mjXURDF : public mjXBase
{
public:
	mjXURDF();											// constructor
	~mjXURDF();											// destructor

	void Parse(TiXmlElement* root);						// main parser

private:
	int FindName(string name, vector<string>& list);	// find name in list
	void AddName(string name, vector<string>& list);	// add name to list
	void AddBody(string name);							// add body to local table
	void AddToTree(int n);								// add body to mjCModel tree
	void Body(TiXmlElement* elem);						// parse body
	void Joint(TiXmlElement* elem);						// parse joint
	mjCGeom* Geom(TiXmlElement* elem, mjCBody* pbody,	// parse origin and geometry of geom
				  bool collision);	
	void Origin(TiXmlElement* elem,double* pos, double* quat); // parse origin element

	void MakeMaterials(TiXmlElement* elem);				// find all materials recursively
	void Clear(void);									// clear local objects

	// URDF parser variables
	vector<string> urName;					// body name
	vector<int> urParent;					// body parent (index in name vector)
	vector<vector<int> > urChildren;			// body children (incidex in name vector)
	vector<string> urMat;					// material name
	vector<mjRGBA> urRGBA;					// material RBG value
};
