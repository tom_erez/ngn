//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once

#define CLASS_DECLSPEC

// error handling and memory allocation
#include "common/errmem.h"


#ifdef __cplusplus
extern "C"
{
#endif

// compile-time configuration
#include "engine/engine_config.h"

// constant and type definitions
#include "engine/engine_typedef.h"
#include "engine/engine_xmacro.h"

// external libraries
#include "ccd/ccd.h"

// function definitions
#include "engine/engine_util.h"
#include "engine/engine_callback.h"
#include "engine/engine_support.h"
#include "engine/engine_core.h"
#include "engine/engine_solver.h"
#include "engine/engine_inverse.h"
#include "engine/engine_impulse.h"
#include "engine/engine_collision.h"
#include "engine/engine_collision_convex.h"
#include "engine/engine_io.h"
#include "engine/engine_print.h"

#ifdef __cplusplus
}
#endif
