//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


// global callback function pointers
extern mjfCallback mjcb_endstep;
extern mjfCallback mjcb_control;
extern mjfCallback mjcb_control_framework;
extern mjfCallback mjcb_impulse;
extern mjfConstraint mjcb_constraint;
extern	mjfTime mjcb_time;
extern mjfActBias mjcb_act_bias;
extern mjfActGain mjcb_act_gain;
extern mjfActDyn mjcb_act_dyn;


// reset callbacks to defaults
void mj_resetCallbacks(void);