//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


//-------------------------- kinematics -------------------------------------------------

// forward kinematics
void mj_kinematics(const mjModel* m, mjData* d);

// map inertias and motion dofs to global frame centered at CoM
void mj_com(const mjModel* m, mjData* d);


// compute tendon lengths, velocities and moment arms
void mj_tendon(const mjModel* m, mjData* d);

// compute actuator transmission lengths and moments
void mj_transmission(const mjModel* m, mjData* d);


//-------------------------- smooth dynamics --------------------------------------------

// recursive Newton-Euler algorithm
void mj_rne(const mjModel* m, const mjOption* o, mjData* d, const mjtNum* qacc);

// composite rigid body inertia algorithm
void mj_crb(const mjModel* m, const mjOption* o, mjData* d);

// sparse L'*D*L factorizaton of the inertia matrix M, assumed spd
void mj_factorM(const mjModel* m, mjData* d);

// sparse backsubstitution:  x = inv(L'*D*L)*y
void mj_backsubM(const mjModel* m, mjData* d, 
				 mjtNum* x, const mjtNum* y, int n);

// half of sparse backsubstitution:  x = sqrt(inv(D))*inv(L')*y
void mj_backsubM2(const mjModel* m, mjData* d, 
				  mjtNum* x, const mjtNum* y, int n);

// spring-damper forces
void mj_passive(const mjModel* m, const mjOption* o, mjData* d);


//-------------------------- constraints ------------------------------------------------

// equality constraints: compute eq_err and eq_J
void mj_makeConstraint(const mjModel* m, const mjOption* o, mjData* d);

// projections involving constraint Jacobian
void mj_projectConstraint(const mjModel* m, const mjOption* o, mjData* d);

// compute y = M_constrained \ x
void mj_backsubMeq(const mjModel* m, mjData* d, mjtNum* y, const mjtNum* x);

// compute eq_vdes, flc_vdes
void mj_desiredVel(const mjModel* m, const mjOption* o, mjData* d);

//-------------------------- impulses ---------------------------------------------------

// add a contact to mjData (utility function)
//  result: 1=error (too many contacts), 0=success
int mj_addContact(const mjModel* m, mjData* d, const mjContact* con);

// add row to sparse flc_J; return 1 if successful, 0 if out of space
int mj_addJrow(const mjModel* m, mjData* d, const mjtNum* row);

// include frictional dofs in impulse solver data structures
void mj_instantiateFriction(const mjModel* m, const mjOption* o, mjData* d);

// include limits in impulse solver data structures
void mj_instantiateLimits(const mjModel* m, const mjOption* o, mjData* d);

// include contacts in impulse solver data structures
void mj_instantiateContacts(const mjModel* m, const mjOption* o, mjData* d);

// warmstart flc_v, compute new flc_signature
void mj_warmStart(const mjModel* m, const mjOption* o, mjData* d, 
				  int oldnflc, int oldnfl);

// impulses: compute lc_dist, xconpenetration, contact frame, Jacobian
CLASS_DECLSPEC void mj_makeImpulse(const mjModel* m, const mjOption* o, mjData* d);

// compute flc_A;  which: 0- full; 1- exact diagonal; 2- approximate diagonal
void mj_makeA(const mjModel* m, const mjOption* o, mjData* d, mjtAType which);

// compute flc_R
void mj_makeR(const mjModel* m, const mjOption* o, mjData* d);

// projections involving impulse Jacobian
void mj_projectImpulse(const mjModel* m, const mjOption* o, mjData* d);
