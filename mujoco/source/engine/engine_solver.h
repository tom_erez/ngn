//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


//-------------------------------- solver components ---------------------------------

// computations that depend only on qpos
void mj_solvePosition(const mjModel* m, const mjOption* o, mjData* d);

// computations that depend only on qpos and qvel
void mj_solveVelocity(const mjModel* m, const mjOption* o, mjData* d);

// clear control, call user callback if defined
void mj_getControl(const mjModel* m, const mjOption* o, mjData* d);

// compute actuator force
void mj_solveActuation(const mjModel* m, const mjOption* o, mjData* d, mjtNum* anext_jac);

// add all smooth forces, compute qvel_next before impulse
void mj_solveForce(const mjModel* m, const mjOption* o, mjData* d);

// forward impulse solver
void mj_solveImpulse(const mjModel* m, const mjOption* o, mjData* d);

// compute energy
void mj_solveEnergy(const mjModel* m, const mjOption* o, mjData* d);


//-------------------------------- integrators ------------------------------------

// integrate positions
void mj_updatePos(const mjModel* m, mjData* d, mjtNum dt, int mode);

// Runge Kutta stepper (called from mj_step only)
void mj_RungeKutta(const mjModel* m, const mjOption* o, mjData* d);


//-------------------------------- top-level API ----------------------------------

// forward dynamics: compute qvel_next, act_next
CLASS_DECLSPEC void mj_forward(const mjModel* m, const mjOption* o, mjData* d);

// advance simulation: use control callback, no external force
CLASS_DECLSPEC void mj_step(const mjModel* m, const mjOption* o, mjData* d);

// advance simulation in two steps: before external force [and control] is set by user
CLASS_DECLSPEC void mj_step1(const mjModel* m, const mjOption* o, mjData* d);

// advance simulation in two steps: after external force [and control] is set by user
CLASS_DECLSPEC void mj_step2(const mjModel* m, const mjOption* o, mjData* d);
