//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

/*
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
*/

#define mjUSEDOUBLE					// double if defined, float if not

//#define mjUSEAVX					// use AVX instructions to vectorize dot-products
