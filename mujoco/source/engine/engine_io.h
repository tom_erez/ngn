//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


//------------------------------- mjOption ----------------------------------------------

// allocate mjOption, set to defaults
mjOption* mj_makeOption(void);

// de-allocate option
void mj_deleteOption(mjOption* o);

// set model options to default values
void mj_defaultOption(mjOption* opt);


//------------------------------- mjModel -----------------------------------------------

// size of mjModel for serialization
int mj_sizeModel(const mjModel* m);

// allocate mjModel
mjModel* mj_makeModel(int nq, int nv, int nu, int na, int nbody, int njnt, 
					  int ngeom, int nsite, int ncam, int nmesh, int nmeshvert, 
					  int nmeshface, int nmeshgraph, int nhfield, int nhfielddata,
					  int ncpair, int neq, int ntendon, int nwrap, 
					  int nnumeric, int nnumericdata, int ntext, int ntextdata,
					  int nuser_body, int nuser_jnt, int nuser_geom, int nuser_site,
					  int nuser_eq, int nuser_tendon, 
					  int nuser_actuator, int nnames);

// copy mjModel
mjModel* mj_copyModel(mjModel* dest, const mjModel* src);

// set pointers in mjModel buffer
void mj_setPtrModel(mjModel* m);

// save model to binary file
void mj_saveModel(const mjModel* m, const mjOption* o,
				  const char* filename, int szbuf, void* buf);

// load model from binary file
CLASS_DECLSPEC mjModel* mj_loadModel(mjOption** ptro, const char* filename, int szbuf, void* buf);

// de-allocate model
CLASS_DECLSPEC void mj_deleteModel(mjModel* m);


//------------------------------- mjData ------------------------------------------------

// allocate mjData
CLASS_DECLSPEC mjData* mj_makeData(const mjModel* m);

// copy mjData
mjData* mj_copyData(mjData* dest, const mjModel* m, const mjData* src);

// set pointers in mjData buffer
void mj_setPtrData(const mjModel *m, mjData* d);

// set data to defaults
CLASS_DECLSPEC void mj_resetData(const mjModel* m, mjData* d);

// mjData stack allocate
mjtNum* mj_stackAlloc(mjData* d, int size);

// de-allocate data
CLASS_DECLSPEC void mj_deleteData(mjData* d);
