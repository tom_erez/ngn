//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

// smooth computations that depend only on qpos
void mj_invPosition(const mjModel* m, const mjOption* o, mjData* d);

// rne with acceleration
void mj_rneAcc(const mjModel* m, const mjOption* o, mjData* d);

// inverse constraint solver
void mj_invConstraint(const mjModel* m, const mjOption* o, mjData* d);

// inverse impulse solver
void mj_invImpulse(const mjModel* m, const mjOption* o, mjData* d);

// inverse dynamics
void mj_inverse(const mjModel* m, const mjOption* o, mjData* d, int flg_full);

// compare forward and inverse dynamics, without changing results of forward dynamics
void mj_compareFwdInv(const mjModel* m, const mjOption* o, mjData* d, mjtNum* fwdinv);