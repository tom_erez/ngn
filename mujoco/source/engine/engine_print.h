//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


// print model and option to text file
void mj_printModel(const mjModel* m, const mjOption* o, const char* filename); 

// print data to text file
void mj_printData(const mjModel* m, mjData* d, const char* filename); 
