//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once


//---------------------------- global constants -----------------------------------------

#define mjPI			3.14159265358979323846

#define mjMINVAL_F		1E-10		// minimum value allowed in any denominator, float
#define mjMINVAL_D		1E-14		// minimum value allowed in any denominator, double
#define mjMAXVAL		1E+15		// maximum value allowed in qpos, qvel (for autofix)
#define mjMINMU			1E-5		// minimum friction coefficient
#define mjMINREG		1E-10		// minimum diagonal regularier for eq_A and flc_A
#define mjNDISABLE		17			// number of disable flags, see mjtDisableBit
#define mjNGEOMTYPES	8			// number of geom types
#define mjNTIMER		12			// number of timers
#define mjNWARNING		7			// number of warning types
#define mjNSTAT			100			// number of iteration statistics fields
#define mjNTEXTURE		10			// to be deleted ???
#define mjNDYN			10			// number of actuator dynamics parameters
#define mjNTRN			1			// number of actuator transmission parameters
#define mjNGAIN			5			// number of actuator gain parameters
#define mjNBIAS			3			// number of actuator bias parameters
#define mjNKEY			10			// to be deleted ???
#define mjMAXCONPAIR	9			// maximum number of contacts per geom pair


//---------------------------- floating-point definitions -------------------------------

#ifdef mjUSEDOUBLE
	typedef double mjtNum;			// numeric data type (float or double)

	static const double mjMINVAL = mjMINVAL_D;
	#define mju_sqrt sqrt
	#define mju_exp exp
	#define mju_sin sin
	#define mju_tan tan
	#define mju_cos cos
	#define mju_acos acos
	#define mju_asin asin
	#define mju_atan2 atan2
	#define mju_tanh tanh
	#define mju_pow pow
	#define mju_abs fabs
	#define mju_log logf
	#define mju_log10 log10
	#define mju_floor floor
	#define mju_ceil ceil

#else
	typedef float mjtNum;

	static const float mjMINVAL = mjMINVAL_F;
	#define mju_sqrt sqrtf
	#define mju_exp expf
	#define mju_sin sinf
	#define mju_cos cosf
	#define mju_tan tanf
	#define mju_acos acosf
	#define mju_asin asinf
	#define mju_atan2 atan2f
	#define mju_tanh tanhf
	#define mju_pow powf
	#define mju_abs fabs
	#define mju_log logf
	#define mju_log10 log10f
	#define mju_floor floor
	#define mju_ceil ceil
#endif


//---------------------------- primitive types (mjt) ------------------------------------
	
typedef unsigned char mjtByte;		// used for true/false


typedef enum _mjtWarning			// warning types
{
	mjWARN_INERTIA = 0,				// (near) singular inertia matrix
	mjWARN_CONFULL,					// too many contacts
	mjWARN_LIMFULL,					// too many limits
	mjWARN_JACFULL,					// too many Jacobian rows
	mjWARN_VGEOMFULL,				// too many visual geoms
	mjWARN_BADQPOS,					// bad number in qpos
	mjWARN_BADQVEL					// bad number in qvel
} mjtWarning;


typedef enum _mjtTimer
{
	// main api
	mjTIMER_STEP		= 0,		// step
	mjTIMER_INVERSE		= 1,		// inverse

	// solver components
	mjTIMER_POSITION	= 2,		// solvePosition
	mjTIMER_VELOCITY	= 3,		// solveVelocity
	mjTIMER_FORCE		= 4,		// solveForce
	mjTIMER_ACTUATION	= 5,		// solveActuation
	mjTIMER_IMPULSE		= 6,		// solveImpulse

	// breakdown of solvePosition
	mjTIMER_KINEMATICS	= 7,		// kinematics, com, tendon, transmission
	mjTIMER_INERTIA		= 8,		// crb, factor
	mjTIMER_EQ			= 9,		// make and project constraints
	mjTIMER_IMP_MAKE	= 10,		// make impulses
	mjTIMER_IMP_PROJECT	= 11,		// project impulses
} mjtTimer;


typedef enum _mjtDisableBit			// disable bitflags
{
	mjDSBL_IMPULSE		= 1<<0,		// entire impulse solver
	mjDSBL_JNTFRICTION	= 1<<1,		// joint friction
	mjDSBL_LIMIT		= 1<<2,		// joint limits	
	mjDSBL_CONTACT		= 1<<3,		// contacts
	mjDSBL_CONSTRAINT	= 1<<4,		// equality constraints
	mjDSBL_PASSIVE		= 1<<5,		// passive forces
	mjDSBL_GRAVITY		= 1<<6,		// gravitational forces
	mjDSBL_BIAS			= 1<<7,		// bias forces (Coriolis, centrifugal, gravity)
	mjDSBL_ANTICIPATE	= 1<<8,		// apply impulse at end of timestep
	mjDSBL_CLAMPVEL		= 1<<9,		// clamp qvel_next to maxvel
	mjDSBL_WARMSTART	= 1<<10,	// warmstart impulse solver
	mjDSBL_PERTURB		= 1<<11,	// apply end-effector perturbations
	mjDSBL_ENERGY		= 1<<12,	// compute kinetic and potential energy
	mjDSBL_FILTERPARENT	= 1<<13,	// remove collisions with parent body
	mjDSBL_ACTUATION	= 1<<14,	// apply actuation forces
	mjDSBL_CBCTRL		= 1<<15,	// use control callback
	mjDSBL_CBEND		= 1<<16		// use end-step callback
} mjtDisableBit;


typedef enum _mjtJoint				// type of degree of freedom
{
	mjJNT_FREE			= 0,		// global position and orientation (quat)		(7)
	mjJNT_BALL			= 1,		// orientation (quat) relative to parent		(4)
	mjJNT_SLIDE			= 2,		// sliding distance along body-fixed axis		(1)
	mjJNT_HINGE			= 3,		// rotation angle (rad) around body-fixed axis	(1)
	mjJNT_NONE			= 1001		// undefined joint
} mjtJoint;


typedef enum _mjtGeom				// type of geometric shape
{
	// standard geom types
	mjGEOM_PLANE		= 0,		// plane
	mjGEOM_HFIELD		= 1,		// height field
	mjGEOM_SPHERE		= 2,		// sphere
	mjGEOM_CAPSULE		= 3,		// capsule
	mjGEOM_ELLIPSOID	= 4,		// ellipsoid
	mjGEOM_CYLINDER		= 5,		// cylinder
	mjGEOM_BOX			= 6,		// box
	mjGEOM_MESH			= 7,		// mesh

	// rendering-only geom types: not used in mjModel, not counted in mjNGEOMTYPES
	mjGEOM_ARROW		= 100,		// arrow
	mjGEOM_ARROW1		= 101,		// arrow without wedges
	mjGEOM_ARROW2		= 102,		// arrow in both directions
	mjGEOM_TORQUE		= 103,		// torque rendering
	mjGEOM_SCREEN		= 104,		// video screen
	mjGEOM_LABEL		= 105,		// text label
	mjGEOM_TUBE			= 106,		// open-face cylinder
	mjGEOM_NONE			= 1001		// missing geom type
} mjtGeom;


typedef enum _mjtIntegrator			// integrator mode
{
	mjINT_EXPLICIT = 0,				// explicit Euler
	mjINT_MIDPOINT,					// midpoint Euler
	mjINT_IMPLICIT,					// (semi) implicit Euler
	mjINT_RK4						// 4th order Runge Kutta
} mjtIntegrator;


typedef enum _mjtCollision			// collision mode for selecting geom pairs
{
	mjCOL_AUTO = 0,					// test precimputed pairs if available, otherwise all
	mjCOL_PAIR,						// test precomputed pairs
	mjCOL_ALL						// test all pairs
} mjtCollision;


typedef enum _mjtAlgorithm			// algorithm used for impulse computation
{
	mjALG_NONE,						// do not compute impulse
	mjALG_GSPYRAMID,				// projected Gauss-Seidel, pyramid
	mjALG_GSCONE,					// projected Gauss-Seidel, cone
	mjALG_USER						// user-defined solver
} mjtAlgorithm;


typedef enum _mjtAType				// type of A-matrix computation
{
	mjA_FULL,						// exact full
	mjA_EXACTDIAG,					// exact diagonal
	mjA_FASTDIAG					// fast approximation to diagonal
} mjtAType;


typedef enum _mjtEq					// type of equality constraint
{
	mjEQ_POINT = 0,					// force two 3D points to coincide
	mjEQ_JOINT,						// couple two scalar joints with polynomial
	mjEQ_TENDON,					// set tendon length to constant
	mjEQ_USER						// first user-defined constraint type
} mjtEq;


typedef enum _mjtWrap				// type of tendon wrap object
{
	mjWRAP_NONE = 0,				// null object
	mjWRAP_JOINT,					// constant moment arm
	mjWRAP_PULLEY,					// pulley used to split tendon
	mjWRAP_SITE,					// pass through site
	mjWRAP_SPHERE,					// wrap around sphere
	mjWRAP_CYLINDER					// wrap around (infinite) cylinder
} mjtWrap;


typedef enum _mjtTrn				// type of actuator transmission
{
	mjTRN_JOINT = 0,				// apply force on joint
	mjTRN_SLIDERCRANK,				// apply force via slider-crank linkage
	mjTRN_TENDON					// apply force on tendon
} mjtTrn;


typedef enum _mjtDyn				// type of actuator dynamics
{
	mjDYN_NONE = 0,					// no internal dynamics; ctrl specifies force
	mjDYN_INTEGRATOR,				// integrator: da/dt = u
	mjDYN_FILTER,					// linear filter: da/dt = (u-a) / tau
	mjDYN_USER						// frist user-defined dynamics type
} mjtDyn;


typedef enum _mjtGain				// type of actuator gain
{
	mjGAIN_FIXED = 0,				// fixed gain
	mjGAIN_USER						// first user-defined gain type
} mjtGain;


typedef enum _mjtBias				// type of actuator bias
{
	mjBIAS_NONE = 0,				// no bias
	mjBIAS_AFFINE,					// const + kp*length + kv*velocity
	mjBIAS_USER						// first user-defined bias type
} mjtBias;


typedef enum _mjtObj				// type of MujoCo object
{
	mjOBJ_UNKNOWN = 0,				// unknown object type
	mjOBJ_BODY,						// body
	mjOBJ_JOINT,					// joint
	mjOBJ_GEOM,						// geom
	mjOBJ_MESH,						// mesh
	mjOBJ_HFIELD,					// heightfield
	mjOBJ_COLLISION,				// collision
	mjOBJ_SITE,						// site
	mjOBJ_CAMERA,					// camera
	mjOBJ_CONSTRAINT,				// constraint
	mjOBJ_TENDON,					// tendon
	mjOBJ_ACTUATOR,					// actuator
	mjOBJ_NUMERIC,					// numeric
	mjOBJ_TEXT						// text
} mjtObj;


//----------------------- mjContact, mjContactPair, mjOption ----------------------------

struct _mjContact					// result of collision detection functions
{
	int dim;						// contact space dimensionality: 1, 3, 4 or 6
	int geom1;						// id of geom 1
	int geom2;						// id of geom 2
	int flc_address;				// address in impulse Jacobian; -1: not set
	mjtNum compliance;				// epsilon parameter
	mjtNum timeconst;				// kappa parameter
	mjtNum dist;					// distance between nearest points; neg: penetration
	mjtNum mindist;					// mindist used to detect contact
	mjtNum pos[3];					// position of contact point: midpoint between geoms
	mjtNum frame[9];				// normal is in [0-2]
	mjtNum friction[5];				// tangent1, 2, spin, roll1, 2
};

typedef struct _mjContact mjContact;


struct _mjOption					// physics options
{
	// real-valued parameters
	mjtNum timestep;				// simulation timestep
	mjtNum gravity[3];				// gravitational acceleration
	mjtNum wind[3];					// wind (for lift, drag and viscosity)
	mjtNum density;					// density of medium
	mjtNum viscosity;				// viscosity of medium
	mjtNum expdist;					// if dist>0, R += A * (exp(expdist*dist/mindist)-1)

	// global rescaling:  modified = s0 + s1*original
	mjtNum s_mindist[2];			// rescale mindist
	mjtNum s_stiffness[2];			// rescale stiffness
	mjtNum s_damping[2];			// rescale damping
	mjtNum s_armature[2];			// rescale armature inertia
	mjtNum s_friction[2];			// rescale contact friction
	mjtNum s_frictionloss[2];		// rescale dof friction loss
	mjtNum s_compliance[2];			// rescale compliance
	mjtNum s_timeconst[2];			// rescale timeconst

	// discrete parameters
	int disableflags;				// bit flags for disabling features
	int integrator;					// integration mode (mjtIntegrator)
	int collisionmode;				// collision mode (mjtCollision)
	int algorithm;					// impulse algorithm (mjtAlgorithm)
	int iterations;					// number of iterations for GS
	mjtByte eqsoft;					// soft equality constraints
	mjtByte fastdiag;				// use fast diagonal A for computing R
	mjtByte remotecontact;			// contact force from distance
	mjtByte stats;					// compute solver statistics
};

typedef struct _mjOption mjOption;



//---------------------------------- mjModel --------------------------------------------

struct _mjModel
{
	// sizes needed at mjModel construction
	int nq;							// number of generalized coordinates = dim(qpos)
	int nv;							// number of degrees of freedom = dim(qvel)
	int nu;							// number of actuators/controls = dim(ctrl)
	int na;							// number of activation states = dim(act)
	int nbody;						// number of bodies
	int njnt;						// number of joints
	int ngeom;						// number of geoms
	int nsite;						// number of sites
	int ncam;						// number of cameras
	int nmesh;						// number of meshes
	int nmeshvert;					// number of vertices in all meshes
	int nmeshface;					// number of triangular faces in all meshes
	int nmeshgraph;					// number of ints in mesh auxiliary data
	int nhfield;					// number of heightfields
	int nhfielddata;				// number of data points in all heightfields
	int	ncpair;						// number of geom pairs in pair array
	int neq;						// number of equality constraints
	int ntendon;					// number of tendons
	int nwrap;						// number of wrap objects in all tendon paths
	int nnumeric;					// number of numeric user fields
	int nnumericdata;				// number of mjtNums in all numeric fields
	int ntext;						// number of text user fields
	int ntextdata;					// number of chars in all text fields
	int nuser_body;					// number of mjtNums in body_user
	int nuser_jnt;					// number of mjtNums in jnt_user
	int nuser_geom;					// number of mjtNums in geom_user
	int nuser_site;					// number of mjtNums in site_user
	int nuser_eq;					// number of mjtNums in eq_user
	int nuser_tendon;				// number of mjtNums in tendon_user
	int nuser_actuator;				// number of mjtNums in actuator_user
	int nnames;						// number of chars in all names

	// sizes set after mjModel construction (only affect mjData)
	int nM;							// number of non-zeros in sparse inertia matrix
	int nemax;						// number of potential (scalar) constraints
	int	nlmax;						// number of potential limits
	int	ncmax;						// number of potential frictional contacts
	int njmax;						// number of available rows in impulse Jacobian
	int	nctotmax;					// number of potential total contacts
	int nstack;						// number of fields in mjData stack
	int nuserdata;					// number of extra fields in mjData

	// size computed during mjModel construction
	int nbuffer;					// number of bytes in buffer

	// ------------------------------- end of info header

	// buffer
	void*	  buffer;				// main buffer (all pointers point in it)	(nbuffer)

	// default generalized coordinates
	mjtNum*	  qpos0;				// qpos values at default pose				(nq x 1)
	mjtNum*	  qpos_spring;			// reference pose for springs				(nq x 1)

	// bodies
	int*	  body_parentid;		// id of body's parent						(nbody x 1)
	int*  	  body_rootid;			// id of root above body					(nbody x 1)
	int*  	  body_weldid;			// id of body that this body is welded to	(nbody x 1)
	int*	  body_jntnum;			// number of joints for this body			(nbody x 1)
	int*	  body_jntadr;			// start addr of joints; -1: no joints		(nbody x 1)
	int*	  body_dofnum;			// number of motion degrees of freedom		(nbody x 1)
	int*	  body_dofadr;			// start addr of dofs; -1: no dofs			(nbody x 1)
	int*	  body_geomnum;			// number of geoms							(nbody x 1)
	int*	  body_geomadr;			// start addr of geoms; -1: no geoms		(nbody x 1)
	mjtNum*   body_pos;				// position offset rel. to parent body		(nbody x 3)
	mjtNum*   body_quat;			// orientation offset rel. to parent body	(nbody x 4)
	mjtNum*   body_ipos;			// local position of center of mass			(nbody x 3)
	mjtNum*   body_iquat;			// local orientation of inertia ellipsoid	(nbody x 4)
	mjtNum*   body_mass;			// mass										(nbody x 1)
	mjtNum*   body_inertia;			// diagonal inertia in ipos/iquat frame		(nbody x 3)
	mjtNum*   body_invweight0;		// mean inv inert in qpos0 (trn, rot)		(nbody x 2)
	mjtNum*   body_user;			// user data								(nbody x nuser_body)

	// joints
	int*	  jnt_type;				// type of joint (mjtJoint)					(njnt x 1)
	int*	  jnt_qposadr;			// start addr in 'qpos' for joint's data	(njnt x 1)
	int*	  jnt_dofadr;			// start addr in 'qvel' for joint's data	(njnt x 1)
	int*	  jnt_bodyid;			// id of joint's body						(njnt x 1)
	mjtByte*  jnt_islimited;		// does joint have limits					(njnt x 1)
	mjtNum*	  jnt_pos;				// local anchor position					(njnt x 3)
	mjtNum*	  jnt_axis;				// local joint axis							(njnt x 3)
	mjtNum*	  jnt_stiffness;		// stiffness coefficient					(njnt x 1)
	mjtNum*	  jnt_range;			// joint limits								(njnt x 2)
	mjtNum*	  jnt_compliance;		// epsilon parameter						(njnt x 1)
	mjtNum*	  jnt_timeconst;		// kappa parameter							(njnt x 1)
	mjtNum*	  jnt_mindist;			// min distance for limit detection			(njnt x 1)
	mjtNum*	  jnt_user;				// user data								(njnt x nuser_jnt)

	// dofs
	int*	  dof_bodyid;			// id of dof's body							(nv x 1)
	int*	  dof_jntid;			// id of dof's joint						(nv x 1)
	int*	  dof_parentid;			// id of dof's parent; -1: none				(nv x 1)
	int*	  dof_Madr;				// dof address in M-diagonal				(nv x 1)
	mjtByte*  dof_isfrictional;		// does dof have friction					(nv x 1)
	mjtNum*   dof_armature;			// dof armature inertia/mass				(nv x 1)
	mjtNum*	  dof_damping;			// damping coefficient						(nv x 1)
	mjtNum*   dof_frictionloss;		// dof friction loss						(nv x 1)
	mjtNum*   dof_maxvel;			// dof max velocity (neg: undefined)		(nv x 1)
	mjtNum*   dof_invweight0;		// inv. diag. inertia in qpos0				(nv x 1)

	// geoms
	int*	  geom_type;			// geometric type (mjtGeom)					(ngeom x 1)
	int*	  geom_contype;			// geom contact type						(ngeom x 1)
	int*	  geom_conaffinity;		// geom contact affinity					(ngeom x 1)
	int*	  geom_condim;			// contact dimensionality (1, 3, 4, 6)		(ngeom x 1)
	int*	  geom_bodyid;			// id of geom's body						(ngeom x 1)
	int*	  geom_dataid;			// id of geom's mesh or hfield (-1: none)	(ngeom x 1)
	int*	  geom_group;			// used for rendering.. delete ???			(ngeom x 1)
	mjtNum*   geom_size;			// geom-specific size parameters			(ngeom x 3)
	mjtNum*   geom_rbound;			// radius of bounding sphere				(ngeom x 1)
	mjtNum*   geom_pos;				// local position offset rel. to body		(ngeom x 3)
	mjtNum*   geom_quat;			// local orientation offset rel. to body	(ngeom x 4)
	mjtNum*   geom_friction;		// friction for (slide, roll, spin)			(ngeom x 3)
	mjtNum*   geom_compliance;		// epsilon parameter						(ngeom x 1)
	mjtNum*   geom_timeconst;		// kappa parameter							(ngeom x 1)
	mjtNum*   geom_mindist;			// min distance for contact detection		(ngeom x 1)
	mjtNum*   geom_user;			// user data								(ngeom x nuser_geom)
	float*	  geom_rgba;			// RGBA color								(ngeom x 4)

	// sites
	int*	  site_bodyid;			// id of site's body						(nsite x 1)
	int*	  site_group;			// used for rendering.. delete ???			(nsite x 1)
	mjtNum*   site_pos;				// local position offset rel. to body		(nsite x 3)
	mjtNum*   site_quat;			// local orientation offset rel. to body	(nsite x 4)
	mjtNum*   site_user;			// user data								(nsite x nuser_site)

	// meshes
	int*	  mesh_faceadr;			// first face address						(nmesh x 1)
	int*	  mesh_facenum;			// number of faces							(nmesh x 1)
	int*	  mesh_vertadr;			// first vertex address						(nmesh x 1)
	int*	  mesh_vertnum;			// number of vertices						(nmesh x 1)
	int*	  mesh_graphadr;		// graph data address; -1: no graph			(nmesh x 1)
	float*	  mesh_vert;			// vertex data for all meshes				(nmeshvert x 3)
	int*	  mesh_face;			// triangle face data						(nmeshface x 3)
	int*	  mesh_graph;			// convex graph data						(nmeshgraph x 1)

	// height fields
	int*	  hfield_nrow;			// number of rows in grid					(nhfield x 1)
	int*	  hfield_ncol;			// number of columns in grid				(nhfield x 1)
	int*	  hfield_adr;			// address in hfield_data					(nhfield x 1)
	int*	  hfield_geomid;		// owning geomid (needed for size) del  ???	(nhfield x 1)
	mjtByte*  hfield_dir;			// 0: (x,y) to (x+1,y+1); 1: opposite		(nhfield x 1)
	float*	  hfield_data;			// elevation data							(nhfielddata x 1)

	// predefined contact pairs
	int*	  pair_dim;				// contact dimensionality					(ncpair x 1)
	int*	  pair_geom1;			// id of geom1								(ncpair x 1)
	int*	  pair_geom2;			// id of geom2								(ncpair x 1)
	mjtNum*	  pair_compliance;		// epsilon parameter						(ncpair x 1)
	mjtNum*	  pair_timeconst;		// kappa parameter							(ncpair x 1)
	mjtNum*	  pair_mindist;			// distance for collision detection			(ncpair x 1)
	mjtNum*	  pair_friction;		// tangent1, 2, spin, roll1, 2				(ncpair x 5)

	// equality constraints
	int*	  eq_type;				// constraint type (mjtEq)					(neq x 1)
	int*	  eq_obj1type;			// type of object 1 (mjtObj)				(neq x 1)
	int*	  eq_obj2type;			// type of object 2 (mjtObj)				(neq x 1)
	int*	  eq_obj1id;			// id of object 1							(neq x 1)
	int*	  eq_obj2id;			// id of object 2							(neq x 1)
	int*	  eq_size;				// number of scalar constraints				(neq x 1)
	int*	  eq_ndata;				// size of eq_data.. delete ???				(neq x 1)
	mjtByte*  eq_isactive;			// enable/disable constraint.. delete ???	(neq x 1)
	mjtNum*	  eq_compliance;		// epsilon parameter						(neq x 1)
	mjtNum*	  eq_timeconst;			// kappa parameter							(neq x 1)
	mjtNum*	  eq_data;				// numeric data for constraint				(neq x 6)
	mjtNum*	  eq_user;				// user data								(neq x nuser_eq)

	// tendons
	int*      tendon_adr;			// address of first object in tendon's path (ntendon x 1)
	int*	  tendon_num;			// number of objects in tendon's path		(ntendon x 1)
	mjtByte*  tendon_islimited;		// does tendon have length limits			(ntendon x 1)
	mjtNum*	  tendon_compliance;	// epsilon parameter						(ntendon x 1)
	mjtNum*	  tendon_timeconst;		// kappa parameter							(ntendon x 1)
	mjtNum*	  tendon_range;			// tendon length limits						(ntendon x 2)
	mjtNum*	  tendon_mindist;		// min distance for limit detection			(ntendon x 1)
	mjtNum*	  tendon_stiffness;		// stiffness coefficient					(ntendon x 1)
	mjtNum*	  tendon_damping;		// damping coefficient						(ntendon x 1)
	mjtNum*	  tendon_lengthspring;	// tendon length in qpos_spring				(ntendon x 1)
	mjtNum*	  tendon_invweight0;	// inv. weight in qpos0						(ntendon x 1)
	mjtNum*	  tendon_user;			// user data								(ntendon x nuser_tendon)

	// list of all wrap objects in tendon paths
	int*	  wrap_type;			// wrap object type (mjtWrap)				(nwrap x 1)
	int*	  wrap_objid;			// object id: geom, site, joint				(nwrap x 1)
	mjtNum*	  wrap_prm;				// divisor, joint coef, or site id			(nwrap x 1)

	// actuators
	int*	  actuator_dyntype;		// dynamics type (mjtDyn)					(nu x 1)
	int*	  actuator_trntype;		// transmission type (mjtTrn)				(nu x 1)
	int*	  actuator_gaintype;	// gain type (mjtGain)						(nu x 1)
	int*	  actuator_biastype;	// bias type (mjtBias)						(nu x 1)
	int*	  actuator_trnid;		// transmission id: joint, tendon, site		(nu x 2)
	mjtByte*  actuator_isctrllimited;  // is control limited					(nu x 1)
	mjtByte*  actuator_isforcelimited; // is force limited						(nu x 1)
	mjtNum*	  actuator_dynprm;		// dynamics parameters						(nu x mjNDYN)
	mjtNum*	  actuator_trnprm;		// transmission parameters					(nu x mjNTRN)
	mjtNum*	  actuator_gainprm;		// gain parameters							(nu x mjNGAIN)
	mjtNum*	  actuator_biasprm;		// bias parameters							(nu x mjNBIAS)
	mjtNum*	  actuator_ctrlrange;	// range of controls						(nu x 2)
	mjtNum*	  actuator_forcerange;	// range of forces							(nu x 2)
	mjtNum*	  actuator_invweight0;	// inv. weight in qpos0						(nu x 1)
	mjtNum*	  actuator_length0;		// actuator length in qpos0					(nu x 1)
	mjtNum*	  actuator_lengthrange;	// ... not yet implemented					(nu x 2)
	mjtNum*	  actuator_user;		// user data								(nu x nuser_actuator)

	// custom numeric fields
	int*	  numeric_adr;			// address of field in numeric_data			(nnumeric x 1)
	int*	  numeric_size;			// size of numeric field					(nnumeric x 1)
	mjtNum*   numeric_data;			// array of all numeric fields				(nnumericdata x 1)

	// custom text fields
	int*	  text_adr;				// address of text in text_data				(ntext x 1)
	char*	  text_data;			// array of all custom texts				(ntextdata x 1)

	// names
	int*	  name_bodyadr;			// body name pointers					    (nbody x 1)
	int*	  name_jntadr;			// joint name pointers					    (njnt x 1)
	int*	  name_geomadr;			// geom name pointers						(ngeom x 1)
	int*	  name_siteadr;			// site name pointers						(nsite x 1)
	int*	  name_meshadr;			// mesh name pointers						(nmesh x 1)
	int*	  name_hfieldadr;		// hfield name pointers						(nhfield x 1)
	int*	  name_eqadr;			// equality constraint name pointers		(neq x 1)
	int*	  name_tendonadr;		// tendon name pointers					    (ntendon x 1)
	int*	  name_actuatoradr;		// actuator name pointers				    (nu x 1)
	int*	  name_numericadr;		// numeric name pointers				    (nnumeric x 1)
	int*	  name_textadr;			// text name pointers					    (ntext x 1)
	char*	  names;				// names of all objects, 0-terminated		(nnames x 1)

	// keyframes
	mjtNum*	  key_qpos;				// key position								(mjNKEY x nq)
	mjtNum*	  key_qvel;				// key velocity								(mjNKEY x nv)
	mjtNum*	  key_act;				// key activation							(mjNKEY x na)

	// cameras
	int*      cam_objtype;			// attachment object type (mjtObj)			(ncam x 1)
	int*      cam_objid;			// id of attachment site, geom or body		(ncam x 1)
	int*	  cam_resolution;		// resolution; -1: use default				(ncam x 2)
	mjtNum*   cam_fov;				// field of view; -1: use default			(ncam x 2)
	mjtNum*   cam_ipd;				// inter-pupilary distance; -1: use default	(ncam x 1)
};

typedef struct _mjModel mjModel;



//---------------------------------- mjData ---------------------------------------------

struct _mjData
{
	// constant sizes
	int  nstack;					// number of mjtNums that can fit in stack
	int  nbuffer;					// size of main buffer in bytes

	// stack info
	int  pstack;					// first available mjtNum address in stack
	int  maxstackuse;				// keep track of maximum stack allocation

	// variable sizes
	int  ne;						// size of equality constraint Jacobian
	int  nc;						// total number of detected contacts
	int  nfri;						// number of frictional dofs
	int  nlim;						// number of limits and frictionless contacts
	int  ncon;						// number of frictional contacts
	int  nflc;						// total number of impulses (rows in flc_J)
	int  nstat;						// number of solver iterations for stats
	int  nwarning[mjNWARNING];		// how many times is each warning type generated

	// timers and impulse solver iteration statistics
	mjtNum timer[2][mjNTIMER];		// accumulated duration, number of calls
	mjtNum stat[2][mjNSTAT];		// impulse kinetic energy, rel. change

	// global properties
	mjtNum time;					// simulation time
	mjtNum energy[2];				// kinetic and potential energy

	//-------------------------------- end of info header

	// buffer and stack
	void*	  buffer;				// main buffer; all pointers point in it	(nbuffer bytes)
	mjtNum*	  stack;				// stack buffer								(nstack mjtNums)

	//-------------------------------- buffer content starts here

	// persistent user data
	mjtNum*	  userdata;				// user buffer								(nuserdata x 1)

	// 2nd-order state
	mjtNum*	  qpos;					// generalized position						(nq x 1)
	mjtNum*   qvel;					// generalized velocity						(nv x 1)
	mjtNum*   qvel_next;			// next-step generalized velocity			(nv x 1)

	// 3rd-order state
	mjtNum*	  act;					// actuator activation						(na x 1)
	mjtNum*	  act_next;				// next-step actuator activation			(na x 1)

	// inputs to forward dynamics
	mjtNum*	  ctrl;					// controls (2nd followed by 3rd order)		(nu x 1)
	mjtNum*   qfrc_applied;			// applied forces in generaled coordinates	(nv x 1)
	mjtNum*   xfrc_applied;			// applied forces in Cartesian coordinates	(nbody x 6)

	// forces computed by engine
	mjtNum*   qfrc_bias;			// interaction and gravitational forces		(nv x 1)
	mjtNum*   qfrc_passive;			// passive forces							(nv x 1)
	mjtNum*   qfrc_actuation;		// actuation forces							(nv x 1)
	mjtNum*   qfrc_impulse;			// impulse force (flc_J'*flc_f/dt)			(nv x 1)
	mjtNum*   qfrc_constraint;		// constraint force (inverse dynamics only) (nv x 1)

	// computed by 'mj_kinematics'
	mjtNum*   xpos;					// Cartesian position of body frame			(nbody x 3)
	mjtNum*   xquat;				// Cartesian orientation of body frame		(nbody x 4)
	mjtNum*   xmat;					// Cartesian orientation of body frame		(nbody x 9)
	mjtNum*   xipos;				// Cartesian position of body com			(nbody x 3)
	mjtNum*   ximat;				// Cartesian orientation of body inertia	(nbody x 9)
	mjtNum*   xanchor;				// Cartesian position of joint anchor		(njnt x 3)
	mjtNum*   xaxis;				// Cartesian joint axis						(njnt x 3)
	mjtNum*   geom_xpos;			// Cartesian geom position					(ngeom x 3)
	mjtNum*   geom_xmat;			// Cartesian geom orientation				(ngeom x 9)
	mjtNum*   site_xpos;			// Cartesian site position					(nsite x 3)
	mjtNum*   site_xmat;			// Cartesian site orientation				(nsite x 9)

	// computed by 'mj_com'
	mjtNum*   com_subtree;			// center of mass of each subtree			(nbody x 3)
	mjtNum*   cdof;					// com-based motion axis of each dof		(nv x 6)
	mjtNum*   cinert;				// com-based body inertia and mass			(nbody x 10)

	// computed by 'mj_tendon'
	int*	  ten_wrapadr;			// start address of tendon's path			(ntendon x 1)
	int*	  ten_wrapnum;			// number of wrap points in path			(ntendon x 1)
	mjtNum*	  ten_length;			// tendon lengths							(ntendon x 1)
	mjtNum*	  ten_moment;			// tendon moment arms						(ntendon x nv)
	int*	  wrap_obj;				// geom id; -1: site; -2: pulley			(nwrap*2 x 1)
	mjtNum*	  wrap_xpos;			// Cartesian 3D points in all path			(nwrap*2 x 3)

	// computed by 'mj_transmission'
	mjtNum*	  actuator_length;		// actuator lengths							(nu x 1)
	mjtNum*	  actuator_moment;		// actuator moment arms						(nu x nv)

	// computed by 'mj_solveActuation'
	mjtNum*	  actuator_force;		// actuator force							(nu x 1)

	// computed by 'mj_rne'
	mjtNum*   cvel;					// com-based velocity [3D rot; 3D tran]		(nbody x 6)

	// used by 'mj_rne' as temp, optionally finalized by 'mj_rnePost'
	mjtNum*   cacc;					// com-based acceleration					(nbody x 6)
	mjtNum*   cfrc_int;				// com-based interaction force with parent	(nbody x 6)
	mjtNum*   cfrc_ext;				// com-based external force on body			(nbody x 6)

	// computed by 'mj_crb'
	mjtNum*   qM;					// total generalized inertia matrix			(nM x 1)
	mjtNum*   qD;					// armature and implicit damping inertia	(nv x 1)

	// computed by 'mj_factorM'
	mjtNum*   qLD;					// L'*D*L factorization of M				(nM x 1)
	mjtNum*   qLDiagSqr;			// 1/sqrt(diag(D))							(nv x 1)

	// computed by 'mj_makeConstraint' 
	int*	  eq_id;				// equality constraint id (active only)		(nemax x 1)
	mjtNum*	  eq_err;				// equality constraint violation			(nemax x 1)
	mjtNum*	  eq_J;					// equality constraint Jacobian				(nemax x nv)

	// computed by 'mj_desiredVel'
	mjtNum*	  eq_vdes;				// desired next-step constraint velocity	(nemax x 1)

	// computed by 'mj_projectConstraint'
	mjtNum*	  eq_JMi;				// Je * inv(M)								(nemax x nv)
	mjtNum*	  eq_Achol;				// Cholesky(eq_J*inv(M)*eq_J' + R)			(nemax x nemax)
	mjtNum*	  eq_R;					// diag. regularizer for eq_A (soft only)	(nemax x 1)

	// computed by 'mj_addContact'
	mjContact* contact;				// list of all detected contacts			(nctotmax x 1)

	// computed by 'mj_makeImpulse'
	int*	  fri_id;				// frictional dof id						(nv x 1)
	int*	  lim_id;				// joint | njnt+tendon | njnt+nten+contact	(nlmax x 1)
	int*	  con_id;				// contact id (index in contact)			(ncmax x 1)
	int*	  lc_ind;				// limit and contact normal indices in flc	(nlmax+ncmax x 1)
	int*	  flc_signature;		// unique identifier for warmstart			(njmax x 1)
	mjtNum*   lc_dist;				// normal distance (neg: penetration)		(nlmax+ncmax x 1)
	mjtNum*   flc_J;				// impulse Jacobian							(njmax x nv)
	mjtNum*   flc_ek;				// epsilon, kappa for each impulse			(njmax x 2)

	// computed by 'mj_projectImpulse'
	mjtNum*   flc_A;				// J * P(M,Je) * J'							(njmax x njmax)
	mjtNum*   flc_R;				// diag. regularizer for flc_A				(njmax x 1)

	// computed by 'mj_desiredVel'
	mjtNum*   flc_vdes;				// desired next-step velocity				(njmax x 1)

	// computed by 'mj_solveImpulse'
	mjtNum*   flc_b;				// b = vdes - v0: solve Af = b s.t. Cone(f)	(njmax x 1)
	mjtNum*   flc_f;				// impluse 									(njmax x 1)
};

typedef struct _mjData mjData;



//---------------------------------- callback function types ----------------------------

// generic callback
typedef void (*mjfCallback) (const mjModel* m, const mjOption* o, mjData* d);

// user-defined constraint
typedef void (*mjfConstraint) (const mjModel* m, const mjOption* o, mjData* d,
							   int which, mjtNum* err, mjtNum* jac);

// collision detection
typedef int (*mjfCollision) (const mjModel* m, const mjOption* o, const mjData* d, 
							 mjContact* con, int g1, int g2, mjtNum mindist);

// user-defined actuator bias
typedef mjtNum (*mjfActBias) (mjtNum length, mjtNum velocity, 
							  int type, const mjtNum* prm);

// user-defined actuator gain
typedef mjtNum (*mjfActGain) (mjtNum length, mjtNum velocity, 
							  int type, const mjtNum* prm);

// user-defined actuator dynamics
typedef mjtNum (*mjfActDyn) (mjtNum length, mjtNum velocity, 
							 int type, const mjtNum* prm, 
							 mjtNum act, mjtNum ctrl, mjtNum dt, mjtNum* jac);

// timer
typedef int (*mjfTime)(void);		



//---------------------------------- macros ---------------------------------------------

// mark and free stack
#define mjMARKSTACK int _mark = d->pstack;
#define mjFREESTACK d->pstack = _mark;

// check disabled flag
#define mjDISABLED(x) (o->disableflags & (x))
