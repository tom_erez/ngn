//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//

#pragma once


//-------------------------------- mjOption fields --------------------------------------

// scalar fields of mjOption
#define MJOPTION_SCALARS		  \
	X( mjtNum,	timestep		) \
	X( mjtNum,	density			) \
	X( mjtNum,	viscosity		) \
	X( mjtNum,	expdist			) \
	X( int,		disableflags	) \
	X( int,		integrator		) \
	X( int,		collisionmode	) \
	X( int,		algorithm		) \
	X( int,		iterations		) \
	X( mjtByte,	eqsoft			) \
	X( mjtByte,	fastdiag		) \
	X( mjtByte,	remotecontact	) \
	X( mjtByte,	stats			)


// numeric fields of mjOption
#define MJOPTION_VECTORS	\
	X( gravity,			3 ) \
	X( wind,			3 ) \
	X( s_mindist,		2 )	\
	X( s_stiffness,		2 ) \
	X( s_damping,		2 ) \
	X( s_armature,		2 ) \
	X( s_friction,		2 ) \
	X( s_frictionloss,	2 ) \
	X( s_compliance,	2 ) \
	X( s_timeconst,		2 )	


//-------------------------------- mjModel fields ---------------------------------------

// int fields of mjModel
#define MJMODEL_INTS        \
	X( nq ) 				\
	X( nv ) 				\
	X( nu ) 				\
	X( na ) 				\
	X( nbody ) 				\
	X( njnt ) 				\
	X( ngeom ) 				\
	X( nsite ) 				\
	X( ncam ) 				\
	X( nmesh ) 				\
	X( nmeshvert ) 			\
	X( nmeshface ) 			\
	X( nmeshgraph ) 		\
	X( nhfield ) 			\
	X( nhfielddata ) 		\
	X( ncpair ) 			\
	X( neq ) 				\
	X( ntendon ) 			\
	X( nwrap ) 				\
	X( nnumeric ) 			\
	X( nnumericdata ) 		\
	X( ntext ) 				\
	X( ntextdata ) 			\
	X( nuser_body ) 		\
	X( nuser_jnt ) 			\
	X( nuser_geom ) 		\
	X( nuser_site ) 		\
	X( nuser_eq ) 			\
	X( nuser_tendon ) 		\
	X( nuser_actuator ) 	\
	X( nnames ) 			\
	X( nM ) 				\
	X( nemax ) 				\
	X( nlmax ) 				\
	X( ncmax ) 				\
	X( njmax ) 				\
	X( nctotmax ) 			\
	X( nstack ) 			\
	X( nuserdata ) 			\
	X( nbuffer )


// pointer fields of mjModel
#define MJMODEL_POINTERS                                          \
	X( mjtNum,	qpos0,				nq, 		1               ) \
	X( mjtNum,	qpos_spring,		nq, 		1               ) \
	X( int,	  	body_parentid,		nbody, 		1               ) \
	X( int,  	body_rootid,		nbody, 		1               ) \
	X( int,  	body_weldid,		nbody, 		1               ) \
	X( int,	  	body_jntnum,		nbody, 		1               ) \
	X( int,	  	body_jntadr,		nbody, 		1               ) \
	X( int,	  	body_dofnum,		nbody, 		1               ) \
	X( int,	  	body_dofadr,		nbody, 		1               ) \
	X( int,	  	body_geomnum,		nbody, 		1               ) \
	X( int,	  	body_geomadr,		nbody, 		1               ) \
	X( mjtNum,  body_pos,			nbody, 		3               ) \
	X( mjtNum,  body_quat,			nbody, 		4               ) \
	X( mjtNum,  body_ipos,			nbody, 		3               ) \
	X( mjtNum,  body_iquat,			nbody, 		4               ) \
	X( mjtNum,  body_mass,			nbody, 		1               ) \
	X( mjtNum,  body_inertia,		nbody, 		3               ) \
	X( mjtNum,  body_invweight0,	nbody, 		2               ) \
	X( mjtNum,  body_user,			nbody, 		nuser_body		) \
	X( int,	  	jnt_type,			njnt, 		1               ) \
	X( int,	  	jnt_qposadr,		njnt, 		1               ) \
	X( int,	  	jnt_dofadr,			njnt, 		1               ) \
	X( int,	  	jnt_bodyid,			njnt, 		1               ) \
	X( mjtByte, jnt_islimited,		njnt, 		1               ) \
	X( mjtNum,	jnt_pos,			njnt, 		3               ) \
	X( mjtNum,	jnt_axis,			njnt, 		3               ) \
	X( mjtNum,	jnt_stiffness,		njnt, 		1               ) \
	X( mjtNum,	jnt_range,			njnt, 		2               ) \
	X( mjtNum,	jnt_compliance,		njnt, 		1               ) \
	X( mjtNum,	jnt_timeconst,		njnt, 		1               ) \
	X( mjtNum,	jnt_mindist,		njnt, 		1               ) \
	X( mjtNum,	jnt_user,			njnt, 		nuser_jnt		) \
	X( int,	  	dof_bodyid,			nv, 		1               ) \
	X( int,	  	dof_jntid,			nv, 		1               ) \
	X( int,	  	dof_parentid,		nv, 		1               ) \
	X( int,	  	dof_Madr,			nv, 		1               ) \
	X( mjtByte, dof_isfrictional,	nv, 		1               ) \
	X( mjtNum,  dof_armature,		nv, 		1               ) \
	X( mjtNum,	dof_damping,		nv, 		1               ) \
	X( mjtNum,  dof_frictionloss,	nv, 		1               ) \
	X( mjtNum,  dof_maxvel,			nv, 		1               ) \
	X( mjtNum,  dof_invweight0,		nv, 		1               ) \
	X( int,	  	geom_type,			ngeom, 		1               ) \
	X( int,	  	geom_contype,		ngeom, 		1               ) \
	X( int,	  	geom_conaffinity,	ngeom, 		1               ) \
	X( int,	  	geom_condim,		ngeom, 		1               ) \
	X( int,	  	geom_bodyid,		ngeom, 		1               ) \
	X( int,	  	geom_dataid,		ngeom, 		1               ) \
	X( int,	  	geom_group,			ngeom, 		1               ) \
	X( mjtNum,  geom_size,			ngeom, 		3               ) \
	X( mjtNum,  geom_rbound,		ngeom, 		1               ) \
	X( mjtNum,  geom_pos,			ngeom, 		3               ) \
	X( mjtNum,  geom_quat,			ngeom, 		4               ) \
	X( mjtNum,  geom_friction,		ngeom, 		3               ) \
	X( mjtNum,  geom_compliance,	ngeom, 		1               ) \
	X( mjtNum,  geom_timeconst,		ngeom, 		1               ) \
	X( mjtNum,  geom_mindist,		ngeom, 		1               ) \
	X( mjtNum,  geom_user,			ngeom, 		nuser_geom		) \
	X( float,	geom_rgba,			ngeom, 		4               ) \
	X( int,	  	site_bodyid,		nsite, 		1               ) \
	X( int,	  	site_group,			nsite, 		1               ) \
	X( mjtNum,	site_pos,			nsite, 		3               ) \
	X( mjtNum,  site_quat,			nsite, 		4               ) \
	X( mjtNum,  site_user,			nsite, 		nuser_site		) \
	X( int,	  	mesh_faceadr,		nmesh, 		1               ) \
	X( int,	  	mesh_facenum,		nmesh, 		1               ) \
	X( int,	  	mesh_vertadr,		nmesh, 		1               ) \
	X( int,	  	mesh_vertnum,		nmesh, 		1               ) \
	X( int,	  	mesh_graphadr,		nmesh, 		1               ) \
	X( float,	mesh_vert,			nmeshvert, 	3               ) \
	X( int,	  	mesh_face,			nmeshface, 	3               ) \
	X( int,	  	mesh_graph,			nmeshgraph,	1               ) \
	X( int,	  	hfield_nrow,		nhfield, 	1               ) \
	X( int,	  	hfield_ncol,		nhfield, 	1               ) \
	X( int,	  	hfield_adr,			nhfield, 	1               ) \
	X( int,	  	hfield_geomid,		nhfield, 	1               ) \
	X( mjtByte, hfield_dir,			nhfield, 	1               ) \
	X( float,	hfield_data,		nhfielddata,1               ) \
	X( int,		pair_dim,			ncpair,		1				) \
	X( int,		pair_geom1,			ncpair,		1				) \
	X( int,		pair_geom2,			ncpair,		1				) \
	X( mjtNum,	pair_compliance,	ncpair,		1				) \
	X( mjtNum,	pair_timeconst,		ncpair,		1				) \
	X( mjtNum,	pair_mindist,		ncpair,		1				) \
	X( mjtNum,	pair_friction,		ncpair,		5				) \
	X( int,	  	eq_type,			neq, 		1               ) \
	X( int,	  	eq_obj1type,		neq, 		1               ) \
	X( int,	  	eq_obj2type,		neq, 		1               ) \
	X( int,	  	eq_obj1id,			neq, 		1               ) \
	X( int,	  	eq_obj2id,			neq, 		1               ) \
	X( int,	  	eq_size,			neq, 		1               ) \
	X( int,	  	eq_ndata,			neq, 		1               ) \
	X( mjtByte, eq_isactive,		neq, 		1               ) \
	X( mjtNum,	eq_compliance,		neq, 		1               ) \
	X( mjtNum,	eq_timeconst,		neq, 		1               ) \
	X( mjtNum,	eq_data,			neq, 		6               ) \
	X( mjtNum,	eq_user,			neq, 		nuser_eq		) \
	X( int,     tendon_adr,			ntendon, 	1               ) \
	X( int,	  	tendon_num,			ntendon, 	1               ) \
	X( mjtByte, tendon_islimited,	ntendon, 	1               ) \
	X( mjtNum,	tendon_compliance,	ntendon, 	1               ) \
	X( mjtNum,	tendon_timeconst,	ntendon, 	1               ) \
	X( mjtNum,	tendon_range,		ntendon, 	2               ) \
	X( mjtNum,	tendon_mindist,		ntendon, 	1               ) \
	X( mjtNum,	tendon_stiffness,	ntendon, 	1               ) \
	X( mjtNum,	tendon_damping,		ntendon, 	1               ) \
	X( mjtNum,	tendon_lengthspring,ntendon, 	1               ) \
	X( mjtNum,	tendon_invweight0,	ntendon, 	1               ) \
	X( mjtNum,	tendon_user,		ntendon, 	nuser_tendon 	) \
	X( int,	  	wrap_type,			nwrap, 		1               ) \
	X( int,	  	wrap_objid,			nwrap, 		1               ) \
	X( mjtNum,	wrap_prm,			nwrap, 		1               ) \
	X( int,	  	actuator_dyntype,	nu, 		1               ) \
	X( int,	  	actuator_trntype,	nu, 		1               ) \
	X( int,	  	actuator_gaintype,	nu, 		1               ) \
	X( int,	  	actuator_biastype,	nu, 		1               ) \
	X( int,	  	actuator_trnid,		nu, 		2               ) \
	X( mjtByte, actuator_isctrllimited, nu, 	1               ) \
	X( mjtByte, actuator_isforcelimited, nu,	1               ) \
	X( mjtNum,	actuator_dynprm,	nu, 		mjNDYN          ) \
	X( mjtNum,	actuator_trnprm,	nu, 		mjNTRN          ) \
	X( mjtNum,	actuator_gainprm,	nu, 		mjNGAIN         ) \
	X( mjtNum,	actuator_biasprm,	nu, 		mjNBIAS         ) \
	X( mjtNum,	actuator_ctrlrange,	nu, 		2               ) \
	X( mjtNum,	actuator_forcerange,nu, 		2               ) \
	X( mjtNum,	actuator_invweight0,nu, 		1               ) \
	X( mjtNum,	actuator_length0,	nu, 		1               ) \
	X( mjtNum,	actuator_lengthrange,nu, 		2               ) \
	X( mjtNum,	actuator_user,		nu, 		nuser_actuator	) \
	X( int,	  	numeric_adr,			nnumeric, 	1               ) \
	X( int,	  	numeric_size,		nnumeric, 	1               ) \
	X( mjtNum,  numeric_data,		nnumericdata,1               ) \
	X( int,	  	text_adr,			ntext, 		1               ) \
	X( char,	text_data,			ntextdata,	1               ) \
	X( int,	  	name_bodyadr,		nbody, 		1               ) \
	X( int,	  	name_jntadr,		njnt, 		1               ) \
	X( int,	  	name_geomadr,		ngeom, 		1               ) \
	X( int,	  	name_siteadr,		nsite, 		1               ) \
	X( int,	  	name_meshadr,		nmesh,   	1               ) \
	X( int,	  	name_hfieldadr,		nhfield, 	1               ) \
	X( int,	  	name_eqadr,			neq, 		1               ) \
	X( int,	  	name_tendonadr,		ntendon, 	1               ) \
	X( int,	  	name_actuatoradr,	nu, 		1               ) \
	X( int,	  	name_numericadr,		nnumeric, 	1               ) \
	X( int,	  	name_textadr,		ntext, 		1               ) \
	X( char,	names,				nnames, 	1               ) \
	X( mjtNum,	key_qpos,			nq,         mjNKEY          ) \
	X( mjtNum,	key_qvel,			nv,         mjNKEY          ) \
	X( mjtNum,	key_act,			na,         mjNKEY          ) \
	X( int,     cam_objtype,		ncam, 		1               ) \
	X( int,     cam_objid,			ncam, 		1               ) \
	X( int,	  	cam_resolution,		ncam, 		2               ) \
	X( mjtNum,  cam_fov,			ncam, 		2               ) \
	X( mjtNum,  cam_ipd,			ncam, 		1               )

// mjNKEY is switched to comply with format; does not affect size computation


// numeric fields of mjModel that can be perturbed (positive is safe)
#define MJMODEL_PERTURB                               \
	X( body_mass,			nbody, 		1           ) \
	X( jnt_stiffness,		njnt, 		1           ) \
	X( jnt_range,			njnt, 		2           ) \
	X( jnt_compliance,		njnt, 		1           ) \
	X( jnt_timeconst,		njnt, 		1           ) \
	X( jnt_mindist,			njnt, 		1           ) \
	X( geom_pos,			ngeom, 		3           ) \
	X( geom_quat,			ngeom, 		4           ) \
	X( geom_friction,		ngeom, 		3           ) \
	X( geom_compliance,		ngeom, 		1           ) \
	X( geom_timeconst,		ngeom, 		1           ) \
	X( geom_mindist,		ngeom, 		1           ) \
	X( site_pos,			nsite, 		3           ) \
	X( site_quat,			nsite, 		4           ) \
	X( pair_compliance,		ncpair,		1			) \
	X( pair_timeconst,		ncpair,		1			) \
	X( pair_mindist,		ncpair,		1			) \
	X( pair_friction,		ncpair,		5			) \
	X( eq_compliance,		neq, 		1           ) \
	X( eq_timeconst,		neq, 		1           ) \
	X( eq_data,				neq, 		6           ) \
	X( tendon_compliance,	ntendon, 	1           ) \
	X( tendon_timeconst,	ntendon, 	1           ) \
	X( tendon_range,		ntendon, 	2           ) \
	X( tendon_mindist,		ntendon, 	1           ) \
	X( tendon_stiffness,	ntendon, 	1           ) \
	X( tendon_damping,		ntendon, 	1           ) \
	X( actuator_dynprm,		nu, 		mjNDYN      ) \
	X( actuator_trnprm,		nu, 		mjNTRN      ) \
	X( actuator_gainprm,	nu, 		mjNGAIN     ) \
	X( actuator_biasprm,	nu, 		mjNBIAS     ) \
	X( actuator_ctrlrange,	nu, 		2           ) \
	X( actuator_forcerange,	nu, 		2           ) \
	X( key_qvel,			nv,         mjNKEY      ) \
	X( key_act,				na,         mjNKEY      ) \
	X( cam_fov,				ncam, 		2           ) \
	X( cam_ipd,				ncam, 		1           ) 


// unit normal fields of mjModel
#define MJMODEL_UNIT                                  \
	X( body_quat,			nbody, 		4           ) \
	X( body_iquat,			nbody, 		4           ) \
	X( jnt_axis,			njnt, 		3           ) \
	X( geom_quat,			ngeom, 		4           ) \
	X( site_quat,			nsite, 		4           ) 


//range fields of mjModel
#define MJMODEL_RANGE                    \
	X( jnt_range,			njnt 	   ) \
	X( tendon_range,		ntendon    ) \
	X( actuator_ctrlrange,	nu 		   ) \
	X( actuator_forcerange,	nu 		   ) 


//-------------------------------- mjData fields ----------------------------------------

// pointer fields of mjData
#define MJDATA_POINTERS                                      \
	X( mjtNum,	userdata,			nuserdata,	1          ) \
	X( mjtNum,	qpos,				nq, 		1          ) \
	X( mjtNum,  qvel,				nv, 		1          ) \
	X( mjtNum,  qvel_next,			nv, 		1          ) \
	X( mjtNum,	act,				na, 		1          ) \
	X( mjtNum,	act_next,			na, 		1          ) \
	X( mjtNum,	ctrl,				nu, 		1          ) \
	X( mjtNum,  qfrc_applied,		nv, 		1          ) \
	X( mjtNum,  xfrc_applied,		nbody, 		6          ) \
	X( mjtNum,  qfrc_bias,			nv, 		1          ) \
	X( mjtNum,  qfrc_passive,		nv, 		1          ) \
	X( mjtNum,  qfrc_actuation,		nv, 		1          ) \
	X( mjtNum,  qfrc_impulse,		nv, 		1          ) \
	X( mjtNum,  qfrc_constraint,	nv, 		1          ) \
	X( mjtNum,  xpos,				nbody, 		3          ) \
	X( mjtNum,  xquat,				nbody, 		4          ) \
	X( mjtNum,  xmat,				nbody, 		9          ) \
	X( mjtNum,  xipos,				nbody, 		3          ) \
	X( mjtNum,  ximat,				nbody, 		9          ) \
	X( mjtNum,  xanchor,			njnt,  		3          ) \
	X( mjtNum,  xaxis,				njnt,  		3          ) \
	X( mjtNum,  geom_xpos,			ngeom, 		3          ) \
	X( mjtNum,  geom_xmat,			ngeom, 		9          ) \
	X( mjtNum,  site_xpos,			nsite, 		3          ) \
	X( mjtNum,  site_xmat,			nsite, 		9          ) \
	X( mjtNum,  com_subtree,		nbody, 		3          ) \
	X( mjtNum,  cdof,				nv, 		6          ) \
	X( mjtNum,  cinert,				nbody, 		10         ) \
	X( int,	  	ten_wrapadr,		ntendon, 	1          ) \
	X( int,	  	ten_wrapnum,		ntendon, 	1          ) \
	X( mjtNum,	ten_length,			ntendon, 	1          ) \
	X( mjtNum,	ten_moment,			ntendon, 	nv         ) \
	X( int,	  	wrap_obj,			nwrap, 		2          ) \
	X( mjtNum,	wrap_xpos,			nwrap, 		6          ) \
	X( mjtNum,	actuator_length,	nu, 		1          ) \
	X( mjtNum,	actuator_moment,	nu, 		nv         ) \
	X( mjtNum,	actuator_force,		nu, 		1          ) \
	X( mjtNum,  cvel,				nbody, 		6          ) \
	X( mjtNum,  cacc,				nbody, 		6          ) \
	X( mjtNum,  cfrc_int,			nbody, 		6          ) \
	X( mjtNum,  cfrc_ext,			nbody, 		6          ) \
	X( mjtNum,  qM,					nM, 		1          ) \
	X( mjtNum,  qD,					nv, 		1          ) \
	X( mjtNum,  qLD,				nM, 		1          ) \
	X( mjtNum,  qLDiagSqr,			nv, 		1          ) \
	X( int,	  	eq_id,				nemax, 		1          ) \
	X( mjtNum,	eq_err,				nemax, 		1          ) \
	X( mjtNum,	eq_J,				nemax, 		nv         ) \
	X( mjtNum,	eq_vdes,			nemax, 		1          ) \
	X( mjtNum,	eq_JMi,				nemax, 		nv         ) \
	X( mjtNum,	eq_Achol,			nemax, 		nemax      ) \
	X( mjtNum,	eq_R,				nemax, 		1          ) \
	X( mjContact, contact,			nctotmax, 	1          ) \
	X( int,	  	fri_id,				nv, 		1          ) \
	X( int,	  	lim_id,				nlmax, 		1          ) \
	X( int,	  	con_id,				ncmax, 		1          ) \
	X( int,	  	lc_ind,				nlmax+m->ncmax, 1      ) \
	X( int,		flc_signature,		njmax,		1		   ) \
	X( mjtNum,  lc_dist,			nlmax+m->ncmax,	1      ) \
	X( mjtNum,  flc_J,				njmax, 		nv         ) \
	X( mjtNum,  flc_ek,				njmax, 		2          ) \
	X( mjtNum,  flc_A,				njmax, 		njmax      ) \
	X( mjtNum,  flc_R,				njmax, 		1          ) \
	X( mjtNum,  flc_vdes,			njmax, 		1          ) \
	X( mjtNum,  flc_b,				njmax, 		1          ) \
	X( mjtNum,  flc_f,				njmax, 		1   	   )
	

// scalar fields of mjData
#define MJDATA_SCALAR            \
	X( mjtNum,    time         ) \
	X( int,       nstack       ) \
	X( int,       nbuffer      ) \
	X( int,       pstack       ) \
	X( int,       maxstackuse  ) \
	X( int,       ne           ) \
	X( int,       nc           ) \
	X( int,       nfri         ) \
	X( int,       nlim         ) \
	X( int,       ncon         ) \
	X( int,       nflc         ) \
	X( int,       nstat        )


// vector fields of mjData
#define MJDATA_VECTOR                        \
	X( int,    nwarning,    mjNWARNING,  1 ) \
	X( mjtNum, energy,      2,           1 )
