//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


// Projected Gauss-Seidel solver: cone or pyramid
void mj_impulseGS(const mjModel* m, const mjOption* o, mjData* d, mjtByte flg_cone);


// solve QCQP in n dimensions:
//  min  0.5*x'*A*x - x'*b   s.t.  sum (xi/di)^2 <= r^2 
void solveQCQP(mjtNum* res, const mjtNum* A, const mjtNum* b, 
			   const mjtNum* d, mjtNum r, int n, const mjtNum* x0);

// solve QCQP in 2 dimensions
void solveQCQP2(mjtNum* res, const mjtNum* Ain, const mjtNum* bin, 
			    const mjtNum* d, mjtNum r, const mjtNum* x0);

// solve QCQP in 3 dimensions
void solveQCQP3(mjtNum* res, const mjtNum* Ain, const mjtNum* bin, 
			    const mjtNum* d, mjtNum r, const mjtNum* x0);
