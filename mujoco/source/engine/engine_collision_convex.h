//-----------------------------------//
//  This file is part of MuJoCo.     //
//  Copyright 2009-2014 Roboti LLC.  //
//-----------------------------------//


// ccd support function
void mjccd_support(const void *obj, const ccd_vec3_t *dir, ccd_vec3_t *vec);

// ccd first direction function
void mjccd_firstdir(const void *obj1, const void *obj2, ccd_vec3_t *dir);


// general convex collisions
int mjc_Convex(const mjModel* m, const mjOption* o, const mjData* d, mjContact* con,  
			   int g1, int g2, mjtNum mindist);
