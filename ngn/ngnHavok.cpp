/*
 *
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Product and Trade Secret source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2013 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 *
 */

#include "ngnHavok.h"
static void HK_CALL errorReport(const char* msg, void* userContext)
{
	using namespace std;
	cout << msg << endl;
}

static hkVector4 hkMakeVector3(mjtNum* mj)
{
	return 	hkVector4(	(hkFloat32)mj[0],
						(hkFloat32)mj[1],
						(hkFloat32)mj[2]);
}


ngnHavok::ngnHavok(mjModel* _mjm, mjOption* _mjo, ngnParams* params)
	:ngn(_mjm, _mjo, params)
{
	name = "Havok";
	PlatformInit();

	time = 0;
	// Need to have memory allocated for the solver. Allocate 1mb for it.
	hkMemoryRouter* memoryRouter = hkMemoryInitUtil::initDefault( hkMallocAllocator::m_defaultMallocAllocator, hkMemorySystem::FrameInfo(1024 * 1024) );
	hkBaseSystem::init( memoryRouter, errorReport );
	hkpWorldCinfo wcinfo;
	wcinfo.m_enableDeactivation = false;
	//wcinfo.m_solverDamp = 0.4;
	//wcinfo.m_solverTau = 0.0001;

	for( int i=0; i<mjm->nnumeric; i++ )
	{
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"havok: enable sleep") && mjm->numeric_data[mjm->numeric_adr[i]]==1 )
			wcinfo.m_enableDeactivation = true;
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"numiter") )
			wcinfo.m_solverIterations = mjm->numeric_data[mjm->numeric_adr[i]];
	}
	wcinfo.m_collisionTolerance = mjm->geom_mindist[0];
	world = new hkpWorld( wcinfo );
	hkpAgentRegisterUtil::registerAllAgents( world->getCollisionDispatcher() );

	hkpGroupFilter* groupFilter = new hkpGroupFilter();
	systemGroupID = groupFilter->getNewSystemGroup();
	layer = 1;
	groupFilter->enableCollisionsBetween(layer, layer);
	groupFilter->enableCollisionsBetween(layer, 2);
	world->setCollisionFilter(groupFilter);
	groupFilter->removeReference();

	//world->getCinfo(wcinfo);
	allowedPenetration=0;
	for( int i=0; i<mjm->nnumeric; i++ )
	{
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"Havok-penetration") )
			allowedPenetration = mjm->numeric_data[mjm->numeric_adr[i]];
	}

	world->setGravity(hkMakeVector3(mjo->gravity));

	b2g.resize(mjm->nbody);

	hkQuaternion I(0,0,0,1);
	I.setIdentity();
	hkpRigidBodyCinfo bodyCinfo;
	bodyCinfo.setTransform(hkTransform(I,
										hkVector4(0,0,-20)));
	hkVector4 halfExtents(20, 20, 20);
	hkpBoxShape* shape = new hkpBoxShape(halfExtents);
	shape->setRadius(0);
	bodyCinfo.m_shape = shape;
	bodyCinfo.m_motionType = hkpMotion::MOTION_FIXED;
	bodyCinfo.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(layer, systemGroupID, 1);
	bodyCinfo.m_allowedPenetrationDepth = allowedPenetration;
	bodyCinfo.m_friction = mjm->geom_friction[0];
	hkpRigidBody* ground = new hkpRigidBody(bodyCinfo);
	bodyCinfo.m_shape->removeReference();
	ground->setUserData(0);
	world->addEntity(ground);
	ground->removeReference();

	try
	{
		addChildren(0, 0);
	}
	catch( ngnError e )
	{
		cout << e.errorMsg.c_str() << endl;
	}
}

hkpConvexTransformShape* ngnHavok::createSingleShape(int geomInx)
{
	hkpConvexShape* shape;
	hkVector4 halfExtents = hkMakeVector3(mjm->geom_size+3*geomInx);
	switch( mjm->geom_type[geomInx] )
	{
	case mjGEOM_BOX:
		shape = new hkpBoxShape(halfExtents);
		break;
	case mjGEOM_SPHERE:
		shape = new hkpSphereShape((hkFloat32)mjm->geom_size[3*geomInx]);
		break;
	case mjGEOM_CAPSULE:
		hkVector4 from(	0,0,(hkFloat32)-mjm->geom_size[3*geomInx+1]);
		hkVector4 to(	0,0,(hkFloat32)mjm->geom_size[3*geomInx+1]);
		shape = new hkpCapsuleShape(from, to, (hkFloat32)mjm->geom_size[3*geomInx]);
	}

	hkpConvexTransformShape* ret = new hkpConvexTransformShape(shape, hkTransform(hkQuaternion(	(hkFloat32)mjm->geom_quat[geomInx*4+1],
																								(hkFloat32)mjm->geom_quat[geomInx*4+2],
																								(hkFloat32)mjm->geom_quat[geomInx*4+3],
																								(hkFloat32)mjm->geom_quat[geomInx*4]),
																	 hkMakeVector3(mjm->geom_pos+geomInx*3)));
	shape->removeReference();


	switch( mjm->geom_type[geomInx] )
	{
	case mjGEOM_BOX:
		ret->setRadius(0.001);
		break;
	case mjGEOM_SPHERE:
		break;
	case mjGEOM_CAPSULE:
		break;
	}

	//ret->setRadius(0.001);
	double aa = ret->getRadius();
	return ret;
}


hkpShape* ngnHavok::createShape(int bInx)
{
	vector<int> gs;
	for( int gInx=0; gInx<mjm->ngeom; gInx++ )
		if( mjm->geom_bodyid[gInx]==bInx )
			gs.push_back(gInx);

	if( gs.size()==0 )
	{
		b2g[bInx] = gs;
		return new hkpSphereShape(0.001);
	}
	else if( gs.size()==1 )
	{
		b2g[bInx] = gs;
		return createSingleShape(gs[0]);
	}
	else
	{
		// multiple shapes
		hkArray<hkpShape*> shapeArray;
		for( int i=0; i<gs.size(); i++ )
			shapeArray.pushBack(createSingleShape(gs[i]));
		hkpListShape* listShape = new hkpListShape( shapeArray.begin(), shapeArray.getSize() );
		for( int i=0; i<gs.size(); i++ )
			shapeArray[i]->removeReference();
		b2g[bInx] = gs;
		return listShape;

	}
}

void ngnHavok::addChildren(int parentInx, hkpRigidBody* parent)
{
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		hkpRigidBody* child;
		if( mjm->body_parentid[bInx]==parentInx )
		{
			hkpRigidBodyCinfo bodyCinfo;
			bodyCinfo.setTransform(hkTransform(hkQuaternion((hkFloat32)mjd->xquat[bInx*4+1],
															(hkFloat32)mjd->xquat[bInx*4+2],
															(hkFloat32)mjd->xquat[bInx*4+3],
															(hkFloat32)mjd->xquat[bInx*4]),
												hkMakeVector3(mjd->xpos+bInx*3)));
			bodyCinfo.m_shape = createShape(bInx);
			if( parent )
				bodyCinfo.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(layer, systemGroupID, bInx+1, parentInx+1);
			else
				bodyCinfo.m_collisionFilterInfo = hkpGroupFilter::calcFilterInfo(layer, systemGroupID, bInx+1);
			hkMassProperties massProperties;
			hkMatrix3 I; I.setIdentity();
			massProperties.m_centerOfMass.set(	(hkFloat32)mjm->body_ipos[bInx*3],
												(hkFloat32)mjm->body_ipos[bInx*3+1],
												(hkFloat32)mjm->body_ipos[bInx*3+2]);
			massProperties.m_inertiaTensor.setDiagonalMul(hkMakeVector3(mjm->body_inertia+bInx*3),
															I);
			massProperties.m_mass = (hkFloat32)mjm->body_mass[bInx];
			bodyCinfo.setMassProperties(massProperties);
			bodyCinfo.m_angularDamping = 0;
			bodyCinfo.m_linearDamping = 0;
			bodyCinfo.m_friction = mjm->geom_friction[0];
			bodyCinfo.m_allowedPenetrationDepth = allowedPenetration;

			mjtNum temp[6];
			ngu_transformSpatial(temp, mjd->cvel+6*bInx, mjd->xipos+3*bInx, mjd->com_subtree+3*mjm->body_rootid[bInx]);
			bodyCinfo.m_angularVelocity = hkMakeVector3(temp);
			bodyCinfo.m_linearVelocity = hkMakeVector3(temp+3);
			
			child = new hkpRigidBody(bodyCinfo);
			bodyCinfo.m_shape->removeReference();

			child->setUserData((hkUlong)bInx);
			world->addEntity(child);

		// create joint:
			int njnt=0, inx[6];
			// count joints, store indices
			for( int j=0; j<mjm->njnt; j++ )
				if( mjm->jnt_bodyid[j]==bInx )
				{
					inx[njnt]=j;
					njnt++;
				}

			if( njnt>1 )
				throw ngnError("we decided to support only 1 joint between parent and child");
			if( !njnt )
			{
				throw ngnError("add welds to odes!");
				// TODO: weld parent and child
			}
			else // add a hinge joint
			{
				if( mjm->jnt_type[inx[0]] == mjJNT_HINGE )
				{
					int j = inx[0];

					hkVector4 pivot = hkMakeVector3(mjd->xanchor+3*j);
					hkVector4 axis = hkMakeVector3(mjd->xaxis+3*j);
					hkpConstraintData* cd;
					if( parent )
					{
						if( mjm->jnt_islimited[j] )
						{
							cd = new hkpLimitedHingeConstraintData();
							((hkpLimitedHingeConstraintData*)cd)->setInWorldSpace(parent->getTransform(), child->getTransform(), pivot, axis);
							((hkpLimitedHingeConstraintData*)cd)->setMinAngularLimit((hkReal)-mjm->jnt_range[2*j+1]);
							((hkpLimitedHingeConstraintData*)cd)->setMaxAngularLimit((hkReal)-mjm->jnt_range[2*j]);
							((hkpLimitedHingeConstraintData*)cd)->setAngularLimitsTauFactor(1);
							if( mjm->dof_isfrictional[mjm->jnt_dofadr[j]] )
								((hkpLimitedHingeConstraintData*)cd)->setMaxFrictionTorque((hkReal)mjm->dof_frictionloss[mjm->jnt_dofadr[j]]);
						}
						else
						{
							cd = new hkpHingeConstraintData();
							((hkpHingeConstraintData*)cd)->setInWorldSpace(parent->getTransform(), child->getTransform(), pivot, axis);
						}
						world->createAndAddConstraintInstance(parent, child, cd)->removeReference();
					}
					else
					{
						if( mjm->jnt_islimited[j] )
						{
							cd = new hkpLimitedHingeConstraintData();
							((hkpLimitedHingeConstraintData*)cd)->setInWorldSpace(child->getTransform(), hkTransform::getIdentity(), pivot, axis);
							// with no parent, child is first, and so the limits are not reversed as above
							((hkpLimitedHingeConstraintData*)cd)->setMinAngularLimit((hkReal)mjm->jnt_range[2*j]);
							((hkpLimitedHingeConstraintData*)cd)->setMaxAngularLimit((hkReal)mjm->jnt_range[2*j+1]);
							((hkpLimitedHingeConstraintData*)cd)->setAngularLimitsTauFactor(1);
							if( mjm->dof_isfrictional[mjm->jnt_dofadr[j]] )
								((hkpLimitedHingeConstraintData*)cd)->setMaxFrictionTorque((hkReal)mjm->dof_frictionloss[mjm->jnt_dofadr[j]]);
						}
						else
						{
							cd = new hkpHingeConstraintData();
							((hkpHingeConstraintData*)cd)->setInWorldSpace(child->getTransform(), hkTransform::getIdentity(), pivot, axis);
						}
						world->createAndAddConstraintInstance(child, NULL, cd)->removeReference();
						// note that the order of entities for actuation (below) matches the parent-child case, not the world-child case
					}

					hkVector4f axisLocalParent;
					hkTransform T;
					if( parent)
						T.setInverse(parent->getTransform());
					else
						T = hkTransform::getIdentity();
					T.getRotation().multiplyVector(axis, axisLocalParent);
					hkVector4f axisLocalChild = hkMakeVector3(mjm->jnt_axis+3*j);
					hkpAction* ta;
					if( parent )
						ta = new torqueAction(parent, child, axisLocalParent, axisLocalChild, torques, mjm->jnt_dofadr[j]);
					else
						ta = new torqueAction(world->getFixedRigidBody(), child, axis, axisLocalChild, torques, mjm->jnt_dofadr[j]);
					world->addAction(ta);
					ta->removeReference();
					if( !(mjo->disableflags & (mjDSBL_PASSIVE)) )
					{
						hkpAction* sda;

						if( parent )
						{
							hkVector4 jnt_parent;
							jnt_parent.setTransformedInversePos(parent->getTransform(), hkMakeVector3(mjd->xanchor+3*j));
							sda = new springDamperAction(parent, child, axisLocalParent, axisLocalChild,
								jnt_parent, hkMakeVector3(mjm->jnt_pos+3*j),
								mjm->qpos_spring[mjm->jnt_qposadr[j]]-mjd->qpos[mjm->jnt_qposadr[j]],
								ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0),
								ngu_rescale(mjo->s_damping, mjm->dof_damping[mjm->jnt_dofadr[j]], 0));
						}
						else
						{
							sda = new springDamperAction(world->getFixedRigidBody(), child, axis, axisLocalChild,
								hkMakeVector3(mjd->xanchor+3*j), hkMakeVector3(mjm->jnt_pos+3*j),
								mjm->qpos_spring[mjm->jnt_qposadr[j]]-mjd->qpos[mjm->jnt_qposadr[j]],
								ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0),
								ngu_rescale(mjo->s_damping, mjm->dof_damping[mjm->jnt_dofadr[j]], 0));
						}
						world->addAction(sda);
						sda->removeReference();
												
					}
				}
			}


			addChildren(bInx, child);

			child->removeReference();
		}
	}
}

springDamperAction::springDamperAction(hkpRigidBody* _a, hkpRigidBody* _b, hkVector4 _axisA, hkVector4 _axisB, hkVector4 _jnt_posA, hkVector4 _jnt_posB, double qref, double _K, double _D)
:	hkpBinaryAction(_a, _b),
	axisA(_axisA),
	axisB(_axisB),
	jnt_posA(_jnt_posA),
	jnt_posB(_jnt_posB),
	K(_K),
	D(_D),
	ref(qref)
{
	if( K<=0 && D<=0 )
		return;


	mjtNum mat[9], qA[4], qB[4];


	hkTransform transA = ((hkpRigidBody*)m_entityA)->getTransform();

	for( int i=0; i<3; i++ )
		for( int j=0; j<3; j++ )
			mat[i*3+j]=transA.getRotation()(i,j);

	ngu_mat2Quat(qA, mat);


	hkTransform transB = ((hkpRigidBody*)m_entityB)->getTransform();

	for( int i=0; i<3; i++ )
		for( int j=0; j<3; j++ )
			mat[i*3+j]=transB.getRotation()(i,j);

	ngu_mat2Quat(qB, mat);

	ngu_negQuat(qA, qA);

	ngu_mulQuat(q_diff, qA, qB);

	ngu_negQuat(q_diff, q_diff);
}


torqueAction::torqueAction(hkpRigidBody* _a, hkpRigidBody* _b, hkVector4 _axisA, hkVector4 _axisB, mjtNum* _torqueVector, int _i )
	:springDamperAction(_a, _b, _axisA, _axisB, _axisA, _axisB,  0, 0, 0),
	torqueVector(_torqueVector),
	i(_i)
{
	if( !torqueVector )
		throw ngnError("torque pointer should be set on ngn construction");
	//i=_i;
}
void torqueAction::applyAction( const hkStepInfo& stepInfo )
{
	hkVector4 globalAxisA(0,0,0);
	((hkpRigidBody*)m_entityA)->getTransform().getRotation().multiplyVector(axisA, globalAxisA);
	hkVector4 globalAxisB(0,0,0);
	((hkpRigidBody*)m_entityB)->getTransform().getRotation().multiplyVector(axisB, globalAxisB);
	hkVector4 axisMean(globalAxisA);
	axisMean.add(globalAxisB);
	axisMean.mul(0.5);
	axisMean.normalize3();
	hkVector4 torque(axisMean);
	torque.mul((hkFloat32)torqueVector[i]);
	((hkpRigidBody*)m_entityB)->applyTorque(stepInfo.m_deltaTime, torque);
	torque.mul(-1);
	((hkpRigidBody*)m_entityA)->applyTorque(stepInfo.m_deltaTime, torque);
}

void springDamperAction::applyAction( const hkStepInfo& stepInfo )
{
	hkVector4 globalAxisA(0,0,0);
	((hkpRigidBody*)m_entityA)->getTransform().getRotation().multiplyVector(axisA, globalAxisA);
	hkVector4 globalAxisB(0,0,0);
	((hkpRigidBody*)m_entityB)->getTransform().getRotation().multiplyVector(axisB, globalAxisB);
	hkVector4 axisMean(globalAxisA);
	axisMean.add(globalAxisB);
	axisMean.mul(0.5);
	axisMean.normalize3();



	mjtNum mat[9], qA[4], qB[4], qtemp[4], qrel[4], axisM[3];
	for( int i=0; i<3; i++ )
		axisM[i]=axisMean(i);

	hkTransform transA = ((hkpRigidBody*)m_entityA)->getTransform();

	for( int i=0; i<3; i++ )
		for( int j=0; j<3; j++ )
			mat[i*3+j]=transA.getRotation()(i,j);

	ngu_mat2Quat(qA, mat);


	hkTransform transB = ((hkpRigidBody*)m_entityB)->getTransform();

	for( int i=0; i<3; i++ )
		for( int j=0; j<3; j++ )
			mat[i*3+j]=transB.getRotation()(i,j);

	ngu_mat2Quat(qB, mat);

	ngu_negQuat(qA, qA);

	ngu_mulQuat(qtemp, qA, qB);

	ngu_mulQuat(qrel, qtemp, q_diff);

    double cost2 = qrel[0];
    double sint2 = sqrt( qrel[1] * qrel[1] + qrel[2] * qrel[2] + qrel[3] * qrel[3] );
	
	double theta = ngu_dot3(axisM, qrel+1)>0 ?
		2*atan2(sint2, cost2) :
		2*atan2(sint2, -cost2);
	
	if( theta>3.14159265358979323846 )
		theta -= 2*3.14159265358979323846;

    double angle = theta;

	double springTorque = K*(angle-ref);

	hkVector4 avelA(((hkpRigidBody*)m_entityA)->getAngularVelocity());
	mjtNum velA = (mjtNum)(avelA.dot3(axisMean));
	hkVector4 avelB(((hkpRigidBody*)m_entityB)->getAngularVelocity());
	mjtNum velB = (mjtNum)(avelB.dot3(axisMean));
	double relvel = velB-velA;
	double damperTorque = D*relvel;
	hkVector4 torque(axisMean);
	torque.mul(springTorque+damperTorque);
	((hkpRigidBody*)m_entityA)->applyTorque(stepInfo.m_deltaTime, torque);
	torque.mul(-1);
	((hkpRigidBody*)m_entityB)->applyTorque(stepInfo.m_deltaTime, torque);
}

ngnHavok::~ngnHavok()
{
	world->removeReference();
	hkBaseSystem::quit();
	hkMemoryInitUtil::quit();

}
void ngnHavok::step()
{
	hkpStepResult r;
	r = world->stepDeltaTime((hkReal)timestep);
	if( r!=HK_STEP_RESULT_SUCCESS )
		r=HK_STEP_RESULT_SUCCESS;
	time += timestep;
}





double ngnHavok::getTime()
{
	return time;
}




void ngnHavok::fillState(ngnState* state)
{
	state->time = getTime();

	const hkpPhysicsSystem* p = world->getWorldAsOneSystem();
	int nb = p->getRigidBodies().getSize();
	for( int bh=0; bh<nb; bh++ )
	{
		int bInx = (int)p->getRigidBodies()[bh]->getUserData();
		if( bInx )
		{
			ngnPVState* bState = &state->bstates[bInx];
			hkpMotion* b = p->getRigidBodies()[bh]->getRigidMotion();
			hk2mj(bState->avel, b->getAngularVelocity());
			hk2mj(bState->xvel, b->getLinearVelocity());
			hkTransform t = p->getRigidBodies()[bh]->getTransform();
			hk2mj(bState->pos, t.getTranslation());
			hkRotationf mat = t.getRotation();
			hkQuaternion q(mat);
			hk2mj(mjd->xquat, q);
			ngu_quat2Mat(bState->mat, mjd->xquat);
			ngu_copy3(mjd->xpos, bState->pos);
			ngu_copy(mjd->xmat, bState->mat, 9);
			for( int g=0; g<b2g[bInx].size(); g++ )
			{
				int gInx = b2g[bInx][g];
				ngnPState* gState = &state->gstates[gInx];
				ngu_local2Global(mjd->xpos, mjd->xmat, mjd->xquat, mjm->geom_pos+3*gInx, mjm->geom_quat+4*gInx, gState->pos, gState->mat);
			}
		}

	}

	// finally, the floor:
	ngnPState* gstate = &state->gstates[0];
	gstate->mat[0]=gstate->mat[4]=gstate->mat[8]=1;

	p->removeReference();
}











// Keycode
#include <Common/Base/keycode.cxx>

// Productfeatures
// We're using only physics - we undef products even if the keycode is present so
// that we don't get the usual initialization for these products.
#undef HK_FEATURE_PRODUCT_AI
#undef HK_FEATURE_PRODUCT_ANIMATION
#undef HK_FEATURE_PRODUCT_CLOTH
#undef HK_FEATURE_PRODUCT_DESTRUCTION_2012
#undef HK_FEATURE_PRODUCT_DESTRUCTION
#undef HK_FEATURE_PRODUCT_BEHAVIOR
#undef HK_FEATURE_PRODUCT_MILSIM
#undef HK_FEATURE_PRODUCT_PHYSICS

// Also we're not using any serialization/versioning so we don't need any of these.
#define HK_EXCLUDE_FEATURE_SerializeDeprecatedPre700
#define HK_EXCLUDE_FEATURE_RegisterVersionPatches
//#define HK_EXCLUDE_FEATURE_RegisterReflectedClasses
#define HK_EXCLUDE_FEATURE_MemoryTracker

// This include generates an initialization function based on the products
// and the excluded features.
#include <Common/Base/Config/hkProductFeatures.cxx>

/*
 * Havok SDK - NO SOURCE PC DOWNLOAD, BUILD(#20130718)
 * 
 * Confidential Information of Havok.  (C) Copyright 1999-2013
 * Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
 * Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
 * rights, and intellectual property rights in the Havok software remain in
 * Havok and/or its suppliers.
 * 
 * Use of this software for evaluation purposes is subject to and indicates
 * acceptance of the End User licence Agreement for this product. A copy of
 * the license is included with this software and is also available at www.havok.com/tryhavok.
 * 
 */