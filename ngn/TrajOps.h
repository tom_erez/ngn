#include <fstream>
#include "ngn.h"

void writeTraj(ngnTraj* traj, const char* filename_c);

void calcMomentum(ngn* ng, ngnTraj* traj);

double calcTrajDist(ngnTraj* tA, ngnTraj* tB);

void calcPenetration(ngn* ng, ngnTraj* traj);

void calcJointError(ngn* ng, ngnTraj* traj);

void calcEnergy(ngn* ng, ngnTraj* traj);

