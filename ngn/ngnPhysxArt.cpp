#include "ngnPhysxArt.h"
#include <string>
using namespace physx;
using namespace std;


ngnPhysxArt::ngnPhysxArt(mjModel* _mjm, mjOption* _mjo, ngnParams* params)
	:ngnPhysxBase(_mjm, _mjo, params)
{
	name="PhysX_articulated";
	buffer = new PxArticulationLink*[mjm->nbody];

	vector<int> l2b; // from art's link to body, to be inserted as an item of a2b
	vector<int> bg2g;

	// children of the world get their own tree:
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		if( mjm->body_parentid[bInx]==0 )
		{
			l2b.clear();
			bg2g.clear();
			int j = mjm->body_jntadr[bInx];
			PxArticulation * art = gPhysics->createArticulation();
			PxU32 a1, a2;
			art->getSolverIterationCounts(a1,a2);
			art->setSolverIterationCounts(1,40);
			articulations.push_back(art);
			PxArticulationLink* child = art->createLink(0, getTransform(bInx));
			l2b.push_back(bInx);
			for( int gInx=0; gInx<mjm->ngeom; gInx++ )
				if( mjm->geom_bodyid[gInx]==bInx )
				{
					attachGeom(child, gInx);
					bg2g.push_back(gInx);
				}
			b2g[bInx]=bg2g;
			child->setCMassLocalPose(getTransform(0, mjm->body_ipos+3*bInx, 0, mjm->body_iquat+4*bInx));
			child->setMass((PxReal)mjm->body_mass[bInx]);
			child->setMassSpaceInertiaTensor(PxVec3(mjm->body_inertia[bInx*3],
													mjm->body_inertia[bInx*3+1],
													mjm->body_inertia[bInx*3+2]));
			mjtNum temp[6];
			ngu_transformSpatial(temp, mjd->cvel+6*bInx, mjd->xipos+3*bInx, mjd->com_subtree+3*mjm->body_rootid[bInx]);
			child->setAngularVelocity(PxVec3(temp[0], temp[1], temp[2]));
			child->setLinearVelocity(PxVec3(temp[3], temp[4], temp[5]));
			int nj=0, inx[6];
			for( int i=0; i<mjm->njnt; i++ )
				if( mjm->jnt_bodyid[i]==bInx )
				{
					inx[nj]=i;
					nj++;
				}

			if( nj>1 )
				throw ngnError("we decided to support only 1 joint between parent and child");
			if( !nj )
			{
				inx[0]=0;
				// TODO: weld parent and child
			}
			else if( mjm->jnt_type[j] == mjJNT_FREE )
			{
				// do nothing
			}
			else
			{
				int j = inx[0];
				if( mjm->jnt_type[j] != mjJNT_HINGE )
					throw ngnError("only hinge joints are supported");
				mjtNum tempmat[9], tempmatT[9];
				ngu_zero(tempmat, 9);
				ngu_copy3(tempmat,		mjd->xaxis+3*j);
				ngu_makeFrame(tempmat);

				ngu_transpose(tempmatT, tempmat, 3, 3);
				PxTransform globalJoint = PxTransform(	(PxReal)mjd->xanchor[j*3],
														(PxReal)mjd->xanchor[j*3+1],
														(PxReal)mjd->xanchor[j*3+2],
												mjMat2PxQuat(tempmatT));
				PxTransform global2child = getTransform(bInx);
				PxTransform child2joint = global2child.transformInv(globalJoint);
				PxTransform global2parent = PxTransform::createIdentity();
				PxTransform parent2joint = global2parent.transformInv(globalJoint);

				PxD6Joint* d6j;
				d6j=PxD6JointCreate(*gPhysics,
								child, child2joint,
								0, parent2joint);
					
				// disable all DOFs (twist will be handled later):
				d6j->setMotion(PxD6Axis::eX, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eY, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eZ, PxD6Motion::eLOCKED);
				//d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eSWING1, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eSWING2, PxD6Motion::eLOCKED);
				d6j->setProjectionLinearTolerance(jointLinearTolerance);
				d6j->setProjectionAngularTolerance(jointAngularTolerance);
				//d6j->setConstraintFlag(PxConstraintFlag::ePROJECTION, true);
					
				// limits:
				if( mjm->jnt_islimited[inx[0]] )
				{
					d6j->setTwistLimit(PxJointAngularLimitPair(	(PxReal)-mjm->jnt_range[j*2+1],
																(PxReal)-mjm->jnt_range[j*2],
																(PxReal)mjm->jnt_mindist[j]));
					d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eLIMITED);
						
				}
				else
					d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eFREE);
				
				if( !(mjo->disableflags & (mjDSBL_PASSIVE)) )
				{
					double stiffness = ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0);
					double damping = ngu_rescale(mjo->s_damping, mjm->dof_damping[mjm->jnt_dofadr[j]], 0);
					PxD6JointDrive drive(stiffness, damping ,1e6); // max force is hacked!
					d6j->setDrive(PxD6Drive::eTWIST, drive);
					double ref = mjm->qpos_spring[mjm->jnt_qposadr[j]] - mjd->qpos[mjm->jnt_qposadr[j]];
					d6j->setDrivePosition(PxTransform(PxQuat(sin(-ref/2), 0, 0, cos(-ref/2))));
				}
				
				pxJointPair jp;
				jp.parent = 0;
				jp.child = child;
				jp.axisLocalChild = pxMakeVector3(mjm->jnt_axis+3*j);
				jp.axisLocalChild.normalize();
				jp.axisLocalParent = pxMakeVector3(mjd->xaxis+3*j);
				jp.axisLocalParent.normalize();
				jp.dofAdr = mjm->jnt_dofadr[j];
				jp.jntAdr = j;
				jp.joint = d6j;
				//jp.ref = mjm->qpos_spring[mjm->jnt_qposadr[j]];
				jointPairs[j] = jp;
			}



			try
			{
				addChildren(bInx, child, l2b);
				gScene->addArticulation(*art);
				a2b.push_back(l2b);
			}
			catch( ngnError e )
			{
				cout << e.errorMsg.c_str() << endl;
			}
		}
	}
}

void ngnPhysxArt::addChildren(int parentInx, PxArticulationLink* parent, vector<int> &l2b)
{
	vector<int> bg2g;
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		bg2g.clear();
		int bodyGeoms = 0;
		if( mjm->body_parentid[bInx]==parentInx )
		{

		// add new body
			PxArticulationLink* child = articulations[articulations.size()-1]->createLink(parent, getTransform(bInx));
			l2b.push_back(bInx);
			for( int gInx=0; gInx<mjm->ngeom; gInx++ )
				if( mjm->geom_bodyid[gInx]==bInx )
				{
					attachGeom(child, gInx);
					bg2g.push_back(gInx);
				}
			b2g[bInx]=bg2g;
			child->setCMassLocalPose(getTransform(0, mjm->body_ipos+3*bInx, 0, mjm->body_iquat+4*bInx));
			child->setMass((PxReal)mjm->body_mass[bInx]);
			child->setMassSpaceInertiaTensor(PxVec3(mjm->body_inertia[bInx*3],
													mjm->body_inertia[bInx*3+1],
													mjm->body_inertia[bInx*3+2]));
			// body vel:
			mjtNum temp[6];
			ngu_transformSpatial(temp, mjd->cvel+6*bInx, mjd->xipos+3*bInx, mjd->com_subtree+3*mjm->body_rootid[bInx]);
			child->setAngularVelocity(PxVec3(temp[0], temp[1], temp[2]));
			child->setLinearVelocity(PxVec3(temp[3], temp[4], temp[5]));
			int nj=0, inx[6];
			for( int j=0; j<mjm->njnt; j++ )
				if( mjm->jnt_bodyid[j]==bInx )
				{
					inx[nj]=j;
					nj++;
				}

			if( nj>1 )
				throw ngnError("we decided to support only 1 joint between parent and child");
			if( !nj )
			{
				inx[0]=0;
				// TODO: weld parent and child
			}
			else if( mjm->jnt_type[inx[0]] == mjJNT_FREE )
			{
				// do nothing
			}
			else // add articulation joint
			{
				int j = inx[0];
				if( mjm->jnt_type[j] != mjJNT_HINGE )
					throw ngnError("only hinge joints are supported");
				mjtNum tempmat[9], tempmatT[9];
				ngu_zero(tempmat, 9);
				ngu_copy3(tempmat,		mjd->xaxis+3*j);
				//if( nj>1 )
				//	mju_copy3(tempmat+3,	mjd->xaxis+3*inx[1]);
				ngu_makeFrame(tempmat);
				/*
				if( nj>2 )
				{
					mjtNum tempAxis[3];
					mju_copy3(tempAxis, mjd->xaxis+3*inx[2]);
					mju_normalize3(tempAxis);
					mjtNum dot = mju_dot3(tempmat+6, tempAxis);
					if( mju_abs(dot)-1>1e-2 )
						throw ngnError("axes are not orthogonal!");
				}
				*/
				ngu_transpose(tempmatT, tempmat, 3, 3);
				PxTransform globalJoint = PxTransform(	(PxReal)mjd->xanchor[j*3],
														(PxReal)mjd->xanchor[j*3+1],
														(PxReal)mjd->xanchor[j*3+2],
												mjMat2PxQuat(tempmatT));
				PxTransform global2child = getTransform(bInx);
				PxTransform child2joint = global2child.transformInv(globalJoint);
				PxTransform global2parent = getTransform(parentInx);
				PxTransform parent2joint = global2parent.transformInv(globalJoint);

				if( parent )
				{
					PxArticulationJoint *joint = child->getInboundJoint();
					joint->setParentPose(parent2joint);
					joint->setChildPose(child2joint);

					joint->setSwingLimit(1e-10, 1e-10); //minimal ellipse - fudged!!
					joint->setSwingLimitEnabled(true);
					joint->setExternalCompliance(1);
					joint->setInternalCompliance(1);
					// limits:
					if( mjm->jnt_islimited[inx[0]] )
					{
						joint->setTwistLimit(	(PxReal)mjm->jnt_range[j*2],
												(PxReal)mjm->jnt_range[j*2+1]);
						joint->setTwistLimitEnabled(true);
					}
					else
						joint->setTwistLimitEnabled(false);

					/*
					// this is useless, because the articulated drive is only in acc mode!
					if( !(mjo->disableflags & (mjDSBL_PASSIVE)) )
					{
						double stiffness = ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0);
						double damping = ngu_rescale(mjo->s_damping, mjm->dof_damping[mjm->jnt_dofadr[j]], 0);
						joint->setStiffness(stiffness);
						joint->setDamping(damping);
						double ref = mjm->qpos_spring[mjm->jnt_qposadr[j]] - mjd->qpos[mjm->jnt_qposadr[j]];
						joint->setTargetOrientation(PxQuat(sin(ref/2), 0, 0, cos(ref/2)));
					}
					*/
				}
				else
					throw ngnError("i didn't expect getting here...");

				pxJointPair jp;
				jp.parent = parent;
				jp.child = child;
				jp.axisLocalChild = pxMakeVector3(mjm->jnt_axis+3*j);
				jp.axisLocalChild.normalize();
				jp.axisLocalParent = parent->getGlobalPose().rotateInv(pxMakeVector3(mjd->xaxis+3*j));
				jp.axisLocalParent.normalize();
				jp.jntAdr = j;
				jp.dofAdr = mjm->jnt_dofadr[j];
				jp.qposAdr = mjm->jnt_qposadr[j];
				double stiffness = ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0);
				double damping = ngu_rescale(mjo->s_damping, mjm->dof_damping[jp.dofAdr], 0);
				jp.K = stiffness;
				jp.D = damping;
				jp.ref = mjm->qpos_spring[jp.qposAdr];
				jp.joint = 0;
				jp.initFrameDiff = jp.parent->getGlobalPose().transformInv(jp.child->getGlobalPose()).getInverse();
				jointPairs[j] = jp;
			}

	//recurse:
			addChildren(bInx, child, l2b);
		}
	}
}


void ngnPhysxArt::fillState(ngnState* state)
{
	state->time = getTime();
	for( int a = 0; a<articulations.size(); a++ )
	{
		vector<int> l2b = a2b[a];
		articulations[a]->getLinks(buffer, l2b.size());
		for( int l=0; l<l2b.size(); l++ )
		{
			int b = l2b[l];
			ngnPVState* bState = &state->bstates[b];
			PxArticulationLink* body = ((PxArticulationLink*)buffer[l]);
			PxTransform xbody = body->getGlobalPose();
			transform2PState(xbody, bState);
			PxVec3 avel = body->getAngularVelocity();
			PxVec3 xvel = body->getLinearVelocity();
			for( int i=0; i<3; i++ )
			{
				bState->avel[i]=avel[i];
				bState->xvel[i]=xvel[i];
			}
			pxQuat2mjQuat(xbody.q, mjd->xquat);
			ngu_copy3(mjd->xpos, bState->pos);
			ngu_copy(mjd->xmat, bState->mat, 9);
			// xquat was written above
			for( int g=0; g<b2g[b].size(); g++ )
			{
				int gInx = b2g[b][g];
				ngnPState* gState = &state->gstates[gInx];
				ngu_local2Global(mjd->xpos, mjd->xmat, mjd->xquat, mjm->geom_pos+3*gInx, mjm->geom_quat+4*gInx, gState->pos, gState->mat);
			}
		}
	}
}
