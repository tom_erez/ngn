#pragma once

#include "ngn.h" 
#include "dart/math/Geometry.h"
#include "dart/dynamics/BodyNode.h"
#include "dart/dynamics/RevoluteJoint.h"
#include "dart/dynamics/Skeleton.h"
#include "dart/simulation/World.h"
#include "dart/dynamics/PlaneShape.h"
#include "dart/dynamics/BoxShape.h"
#include "dart/dynamics/CylinderShape.h"
#include "dart/dynamics/EllipsoidShape.h" // for sphere
#include "dart/dynamics/WeldJoint.h"
#include "dart/dynamics/FreeJoint.h"
#include "dart/collision/bullet/BulletCollisionDetector.h"
#include "dart/constraint/ConstraintSolver.h"
#include "engine/engine.h"

#pragma once
#ifdef _EXPORTING
   #define DECLSPEC    __declspec(dllexport)
#else
   #define DECLSPEC    __declspec(dllimport)
#endif

class ngnDART : public ngn
{
protected:
	double time;
	dart::simulation::World* world;
	vector<dart::dynamics::Skeleton*> skeletons;

	vector<int> s2q;
	vector<int> s2d;
	vector<vector<int>> t2j; // from tree and link to joint index
	vector<int> j2t; // from joint to tree
	vector<int> j2l; // from joint to tree

	void addChildren(dart::dynamics::Skeleton* skel, int parentInx, dart::dynamics::BodyNode* parent);
	void addShapes(int bInx, dart::dynamics::BodyNode* parent);
	vector<vector<int>> b2g;
public:
	DECLSPEC ngnDART(mjModel* _mjm, mjOption* _mjo, ngnParams* params);
	DECLSPEC ~ngnDART();
	DECLSPEC double getTime();
	DECLSPEC void step();
	DECLSPEC void fillState(ngnState * s);
};

