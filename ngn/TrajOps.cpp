#include "mujoco.h"
#include "TrajOps.h"
//#include <boost/filesystem.hpp>

using namespace std;

void writeTraj(ngnTraj* traj, const char* filename_c)
{
	ofstream fs;
	fs.open(filename_c, ios::out | ios::binary);

	//yuval: this should be fixed
	//boost::filesystem::path dir("path");
	//
	//if(!(boost::filesystem::exists(dir)))
	//{
	//	std::cout << "Doesn't Exist" << std::endl;

	//	if (boost::filesystem::create_directory(dir))
	//		std::cout << "....Successfully Created !" << endl;
	//}

	if( !fs.good() )
		printf("failed to open file!");

	int N = traj->states.size();
	int G = traj->ngeom;
	int B = traj->nbody;
	int M = 1+12*G+9*(B-1)+3+6+1+1;
	double size[2] = {M, N};
	fs.write((const char*)size, 2*sizeof(double));

	double* n = new double[30000];
	if (M > 30000)
	{
		printf("increase buffer size in writeTraj!");
	}
	else
	{
		for( int i=0; i<N; i++ )
		{
			int p=0;

			n[p++] = traj->states[i].time;
			for( int j=0; j<G; j++ )
				for(int k=0; k<3; k++ )
					n[p++] = traj->states[i].gstates[j].pos[k];
			for( int j=0; j<G; j++ )
				for(int k=0; k<9; k++ )
					n[p++] = traj->states[i].gstates[j].mat[k];
			for( int b=1; b<B; b++ )
				for(int k=0; k<3; k++ )
					n[p++] = traj->states[i].bstates[b].pos[k];
			for( int b=1; b<B; b++ )
			{
				for(int k=0; k<3; k++ )
					n[p++] = traj->states[i].bstates[b].avel[k];
				for(int k=0; k<3; k++ )
					n[p++] = traj->states[i].bstates[b].xvel[k];
			}
			n[p++] = traj->states[i].energy;
			n[p++] = traj->states[i].penetration;
			n[p++] = traj->states[i].jointError;

			for( int b=0; b<6; b++ )
				n[p++] = traj->states[i].momentum[b];
			n[p++] = traj->states[i].CPUtime;
			n[p++] = (double)traj->states[i].nc;

			fs.write((const char*)n, p*sizeof(double));
		}
		
	}
	fs.close();
	delete n;
}

void calcStateMomentum(ngn* ng, ngnState* state)
{
	mjModel* m = ng->mjm;

	double com[3]={0,0,0};
	double comvel[3]={0,0,0};
	ngu_zero(state->momentum, 6);
	mjtNum totalMass=0;
	// linear momentum, get com
	for( int b=1; b< m->nbody; b++ )
	{
		ngu_addToScl3(state->momentum+3, state->bstates[b].xvel, m->body_mass[b]);
		ngu_addToScl3(com, state->bstates[b].pos, m->body_mass[b]);
		ngu_addToScl3(comvel, state->bstates[b].xvel, m->body_mass[b]);
		totalMass += m->body_mass[b];
	}
	ngu_scl3(com, com, 1.0/totalMass);
	ngu_scl3(comvel, comvel, 1.0/totalMass);

	// if the root is not a free joint we cannot use the com as a reference point
	if( m->jnt_type[0] != mjJNT_FREE)
	{
		ngu_copy3(com, ng->mjd->xanchor);
		ngu_zero3(comvel);
	}

	// angular momentum
	for( int b=1; b< m->nbody; b++ )
	{
		mjtNum dp[3], dx[3], dv[3];
		ngu_sub3(dx, state->bstates[b].pos, com);
		ngu_sub3(dv, state->bstates[b].xvel, comvel);
		ngu_cross(dp, dx, dv);
		ngu_addToScl3(state->momentum, dp, m->body_mass[b]);

		// rotate to body frame
		mjtNum avel_local[3], mv[3];
		ngu_rotVecMatT(avel_local, state->bstates[b].avel, state->bstates[b].mat);

		// angular momentum in body frame
		for( int i=0; i<3; i++ )
			mv[i] = m->body_inertia[3*b+i]*avel_local[i];

		// rotate back
		mjtNum angmom[3];
		ngu_rotVecMat(angmom, mv, state->bstates[b].mat);
		ngu_addTo3(state->momentum, angmom);

	}
}

void calcMomentum(ngn* ng, ngnTraj* traj)
{
	for( int i=0; i<(int)traj->states.size(); i++ )
		calcStateMomentum(ng, &traj->states[i]);
}


void calcStateEnergy(ngn* ng, ngnState* state)
{
	mjModel* m = ng->mjm;

	//recreateKinematics(m, o, d, state);
	state->energy = 0;
	for( int b=1; b< m->nbody; b++ )
	{
		state->energy += ngu_dot3(state->bstates[b].xvel, state->bstates[b].xvel)*m->body_mass[b]/2;
		state->energy -= m->body_mass[b] * ngu_dot3(m->opt.gravity, state->bstates[b].pos);
		mjtNum avel_local[3];
		ngu_rotVecMatT(avel_local, state->bstates[b].avel, state->bstates[b].mat);
		for( int i=0; i<3; i++ )
			state->energy += 0.5*m->body_inertia[3*b+i]*avel_local[i]*avel_local[i];
	}
}

void calcEnergy(ngn* ng, ngnTraj* traj)
{
	for( int i=0; i<(int)traj->states.size(); i++ )
		calcStateEnergy(ng, &traj->states[i]);
}



void recreateKinematics(const mjModel* m, mjData* d, ngnState* state)
{
	int i;

	// ensure the ground is in place:
	ngu_zero3(d->xpos);
	ngu_zero(d->xquat, 4);
	d->xquat[0]=1;
	ngu_quat2Mat(d->xmat, d->xquat);

	for( i=1; i<m->nbody; i++ )
	{
		// fill in xpos, xmat:
		ngu_copy3(d->xpos+3*i, state->bstates[i].pos);
		ngu_copy(d->xmat+9*i, state->bstates[i].mat, 9);

		ngu_mat2Quat(d->xquat+4*i, d->xmat+9*i);

		// hack xanchor, xaxis:
		for( int j=0; j<m->body_jntnum[i]; j++ )
		{
			int jid = m->body_jntadr[i] + j;
			// compute axis in global frame
			ngu_rotVecQuat(d->xaxis+3*jid, m->jnt_axis+3*jid, d->xquat+4*i);
			// compute anchor in global frame
			ngu_rotVecQuat(d->xanchor+3*jid, m->jnt_pos+3*jid, d->xquat+4*i);
			ngu_addTo3(d->xanchor+3*jid, d->xpos+3*i);
		}
	}
	
	// compute Cartesian positions and orientations of body inertial frames
	for( i=1; i<m->nbody; i++ )
		ngu_local2Global(d->xpos+3*i, d->xmat+9*i, d->xquat+4*i,
					 m->body_ipos+3*i, m->body_iquat+4*i, d->xipos+3*i, d->ximat+9*i);

	// compute Cartesian positions and orientations of geoms
	for( i=0; i<m->ngeom; i++ )
		ngu_local2Global(d->xpos+3*m->geom_bodyid[i], d->xmat+9*m->geom_bodyid[i], d->xquat+4*m->geom_bodyid[i],
					 m->geom_pos+3*i, m->geom_quat+4*i, d->geom_xpos+3*i, d->geom_xmat+9*i);

	// compute Cartesian positions and orientations of sites
	for( i=0; i<m->nsite; i++ )
		ngu_local2Global(d->xpos+3*m->site_bodyid[i], d->xmat+9*m->site_bodyid[i], d->xquat+4*m->site_bodyid[i],
					 m->site_pos+3*i, m->site_quat+4*i, d->site_xpos+3*i, d->site_xmat+9*i);

}


void calcStatePenetration(ngn* ng, ngnState* state)
{
	mjModel* m = ng->mjm;
	mjData* d = ng->mjd;

	recreateKinematics(m, d, state);
	// proceed as normal:
	mj_makeConstraint(m, d);
	// finally:
	state->penetration = 0;
	state->nc = d->ncon;
	for( int i=0; i<d->ncon; i++ )
		state->penetration += ngu_min(d->contact[i].dist, 0);

}
/*
struct jointSite
{
	int inxParent;
	mjtNum posParent[3];
	int inxChild;
	mjtNum posChild[3];
};

void calcStateJointError(ngn* ng, ngnState* state, vector<jointSite*> jss)
{
	mjModel* m = ng->mjm;
	mjOption* o = ng->mjo;
	mjData* d = ng->mjd;

	recreateKinematics(m, o, d, state);
	mjtNum gposParent[3];
	mjtNum gposChild[3];
	mjtNum diff[3];
	state->jointError = 0;
	for( int i=0; i<jss.size(); i++ )
	{
		int iP = jss[i]->inxParent;
		ngu_rotVecMat(gposParent, jss[i]->posParent, d->xmat+9*iP);
		ngu_addTo3(gposParent, d->xpos+3*iP);  
		int iC = jss[i]->inxChild;
		ngu_rotVecMat(gposChild, jss[i]->posChild, d->xmat+9*iC);
		ngu_addTo3(gposChild, d->xpos+3*iC);  
		ngu_sub3(diff, gposParent, gposChild);
		state->jointError += ngu_norm3(diff);
	}
}


void calcJointError(ngn* ng, ngnTraj* traj)
{
	mjModel* m = ng->mjm;
	mjOption* o = ng->mjo;
	mjData* d = ng->mjd;
	// prepare a list of local poses of joints in both parent and child frames
	vector<jointSite*> jss;
	mj_resetData(m, o, d);
	mj_kinematics(m, o, d);
	for( int i=0; i<m->njnt; i++ )
		if( m->body_parentid[m->jnt_bodyid[i]]>0 )
		{
			jointSite* js = new jointSite;
			js->inxChild = m->jnt_bodyid[i];
			js->inxParent = m->body_parentid[m->jnt_bodyid[i]];
			ngu_copy3(js->posChild, m->jnt_pos+3*i);
			ngu_sub3(js->posParent, d->xanchor+3*i, d->xpos+3*js->inxParent);
			jss.push_back(js);
		}

	for( int i=0; i<(int)traj->states.size(); i++ )
		calcStateJointError(ng, &traj->states[i], jss);

	for( int i=0; i<jss.size(); i++ )
		delete jss[i];
}
*/
void calcPenetration(ngn* ng, ngnTraj* traj)
{
	for( int i=0; i<(int)traj->states.size(); i++ )
		calcStatePenetration(ng, &traj->states[i]);
}



double calcPVStateDist(ngnPVState* gA, ngnPVState* gB)
{
	mjtNum diff[3];
	ngu_sub3(diff, gA->pos, gB->pos);
	mjtNum agg = ngu_norm3(diff);
	
	ngu_sub3(diff, gA->avel, gB->avel);
	agg += ngu_norm3(diff);
	ngu_sub3(diff, gA->xvel, gB->xvel);
	agg += ngu_norm3(diff);
	
	return agg;
}

double calcStateDist(ngnState* sA, ngnState* sB)
{
	int N = sA->nbody;
	if( N!=sB->nbody )
		return -1e10;
	double ans=0;
	for( int i=0; i<N; i++ )
		ans += calcPVStateDist(&sA->bstates[i], &sB->bstates[i]);

	return ans;
}

double calcTrajDist(ngnTraj* tA, ngnTraj* tB)
{
	double ans = 0;
	int NA = tA->states.size();
	int NB = tB->states.size();
	ngnTraj *tS, *tL;// shorter and longer
	int NS, NL;
	if( NA<NB )
	{
		NS = NA;
		tS = tA;
		tL = tB;
		NL = NA;
	}
	else
	{
		NS = NB;
		tS = tB;
		tL = tA;
		NL = NA;
	}
	int i, iL = 0; // index in long traj
	for( i=0; i<NS; i++ )
	{
		double curTime = tS->states[i].time;
		// find relevant index in long traj
		while( iL<NL && abs(tL->states[iL].time - curTime) > 1e-8 )
			iL++;
		if( iL>=NL )
			break;
		// we now have a match.
		ans += calcStateDist(&tS->states[i], &tL->states[iL]);
	}
	return ans / i;
}