#pragma once

#pragma warning disable 82  111  181  280  367  442  693  869 2407 2415 2557 381 1879

#include <vector>
#include <map>
#include <stack>
#include <iostream>

#include "ngu.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <mmsystem.h>



using namespace std;

struct ngnPState
{
	double pos[3];
	double mat[9];
	ngnPState() {pos[0]=0;pos[1]=0;pos[2]=0;mat[0]=0;mat[1]=0;mat[2]=0;mat[3]=0;mat[4]=0;mat[5]=0;mat[6]=0;mat[7]=0;mat[8]=0;}
};

struct ngnPVState : public ngnPState
{
	double avel[3];
	double xvel[3];
	ngnPVState() : ngnPState() {
		avel[0]=avel[1]=avel[2]=0;
		xvel[0]=xvel[1]=xvel[2]=0;
	}
};

struct ngnState 
{
public:
	int ngeom;
	ngnState(int _ngeom, int _nbody);
	ngnState(const ngnState& other);
	~ngnState();
	int nbody;
	double time;
	double energy;
	double penetration;
	double jointError;
	double CPUtime;
	int nc;

	double momentum[6];
	ngnPState* gstates;
	ngnPVState* bstates;
};

struct ngnTraj
{
	friend class ngn;
protected:
	ngnTraj(int _nbody, int _ngeom, int N);
public:
	~ngnTraj();
	vector<ngnState> states;
	int nbody;
	int ngeom;
};

#define MAX_ACT_PARAMS 300
struct ngnParams
{
	int keyframe;
	double duration;
	int range[2];
	int nSegments;
	int nAct;
	double actuator[MAX_ACT_PARAMS];
	int rangeType;
	string outdir;
	ngnParams()
	{
		outdir = "C:/temp/output/";
		duration	= 1.0;
		keyframe    =-1;
		range[0]	= 0;
		range[1]	= 0;
		nSegments	= 1;
		nAct		= 0;
		rangeType   = 0;
		ngu_zero(actuator, MAX_ACT_PARAMS);
	}
};

typedef void (*ngnTorqueCB) (const ngnParams *params, const mjModel* mjm, double time, mjtNum* torques);


struct jointPair
{
	int dofAdr;
	int qposAdr;
	int jntAdr;
	double K;
	double ref;
	double D;
	// subclasses should define parent, child and axis
};

class ngn
{
protected:
	mjtNum* torques;
	vector<ngnTraj*> trajs;
public:
	mjModel *mjm;
	mjData *mjd;
	mjData *mjd0;
	ngnParams *params;
	double timestep;
	string name;
	BASE_DECLSPEC ngn(mjModel* _mjm, ngnParams* params);
	// TODO: This is potentially an UNDEFINED BEHAVIOR: the base destructor should be protected or virtual.  however, linking errors result in both cases.
	BASE_DECLSPEC ~ngn();
	//BASE_DECLSPEC void setState(ngnState* state){}
	//BASE_DECLSPEC void setStateFromMjData(){}
	virtual double getTime()=0;
	virtual void step()=0;
	virtual void fillState(ngnState * s)=0;
	BASE_DECLSPEC void updateTorques();
	BASE_DECLSPEC ngnTraj* integrate(ngnState* initState, double period);
	BASE_DECLSPEC void clearTrajVector();
};


class ngnError
{
public:
	ngnError(string msg = 0){errorMsg=msg;};

	string errorMsg;				// error message
};


#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "Ws2_32.lib")

static bool _tmInitialized = false;
static int _tmBase;
static LARGE_INTEGER _tmFreqHR;
static LARGE_INTEGER _tmBaseHR;


// initialize 1 msec timer, record base time
static void mjBeginTime(void)
{
	if( _tmInitialized )
		return;

	// initialize multimedia timer
	timeBeginPeriod(1);
	_tmBase = timeGetTime();

	// initialize high performance timer
	QueryPerformanceFrequency(&_tmFreqHR);
	QueryPerformanceCounter(&_tmBaseHR);

	_tmInitialized = true;
};



// close timer
static void mjEndTime(void)
{
	if( _tmInitialized )
	{
		timeEndPeriod(1);
		_tmInitialized = false;
	}
};



// get time in milliseconds scince initialization
static int mjGetTime(void)
{
	if( !_tmInitialized )
		mjBeginTime();

	return (timeGetTime() - _tmBase);
};

// get time in microseconds scince initialization
static int mjGetTimeHR(void)
{
	if( !_tmInitialized )
		mjBeginTime();

	LARGE_INTEGER tm;
	QueryPerformanceCounter(&tm);
	return (int) (((tm.QuadPart - _tmBaseHR.QuadPart)*1000000)/_tmFreqHR.QuadPart);
};