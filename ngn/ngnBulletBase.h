#include "btBulletDynamicsCommon.h"
#include "ngn.h"
//#include "engine/engine.h"

#define CLASS_DECLSPEC

#pragma once

static void copyQuat(const btQuaternion *btquat, mjtNum* mjquat)
{
	mjquat[0] = btquat->getW();
	mjquat[1] = btquat->getX();
	mjquat[2] = btquat->getY();
	mjquat[3] = btquat->getZ();
}

static void btQuat2rot(const btQuaternion* btquat, mjtNum* rot)
{
	mjtNum mjquat[4];
	mjquat[0] = btquat->getW();
	mjquat[1] = btquat->getX();
	mjquat[2] = btquat->getY();
	mjquat[3] = btquat->getZ();
	ngu_quat2Mat(rot, mjquat);
}

static btQuaternion mjQuat2btQuat(mjtNum* mjQuat)
{
	return btQuaternion(mjQuat[1],
						mjQuat[2],
						mjQuat[3],
						mjQuat[0]);
}

static btQuaternion mjMat2btQuat(mjtNum* mjMat)
{
	mjtNum mjQuat[4];
	ngu_mat2Quat(mjQuat, mjMat);
	return mjQuat2btQuat(mjQuat);
}

static btVector3 makebtVector3(mjtNum* pos)
{
	btVector3 bpos(	pos[0],
					pos[1],
					pos[2]);
	return bpos;
}
static btTransform getTransform(mjtNum* pos, mjtNum* mat)
{
	btVector3 bpos = makebtVector3(pos);
	btMatrix3x3 rot(mat[0], mat[1], mat[2],
					mat[3], mat[4], mat[5],
					mat[6], mat[7], mat[8]);
	btTransform ret(rot, bpos);
	return ret;
}

class ngnBulletBase : public ngn
{
protected:
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btBroadphaseInterface* overlappingPairCache;
	vector<vector<int>> b2g;
	float m_globalCfm;
	float m_erp;
	float m_numIterations;

	double time;
	btCollisionShape* bodyGeoms(int bInx);
	float cfm;
	float erp;
	int numiter;
public:
	CLASS_DECLSPEC ngnBulletBase(mjModel* _mjm, ngnParams* params);
	CLASS_DECLSPEC ~ngnBulletBase();
	CLASS_DECLSPEC double getTime();
	CLASS_DECLSPEC double computeEnergy();
};

