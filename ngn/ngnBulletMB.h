#include "ngnBulletBase.h"
#include "BulletDynamics/Featherstone/btMultiBody.h"
#include "BulletDynamics/Featherstone/btMultiBodyConstraintSolver.h"
#include "BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.h"
#include "BulletDynamics/Featherstone/btMultiBodyLinkCollider.h"
#include "BulletDynamics/Featherstone/btMultiBodyLink.h"
#include "BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.h"
#include "BulletDynamics/Featherstone/btMultiBodyJointMotor.h"
#include "BulletDynamics/Featherstone/btMultiBodyPoint2Point.h"

#pragma once
#define CLASS_DECLSPEC

class ngnBulletMB : public ngnBulletBase
{
protected:
	btMultiBodyConstraintSolver* solver;
	btMultiBodyDynamicsWorld* dynamicsWorld;
	vector<btMultiBody*> trees;
	vector<vector<int>> t2j; // from tree and link to joint index
	vector<int> j2t; // from joint to tree
	vector<int> j2l; // from joint to tree
	int ngnBulletMB::addChildren(btMultiBody* tree, vector<int> &l2b, int linkIndex, int parentInx);
public:
	CLASS_DECLSPEC ngnBulletMB(mjModel* _mjm, ngnParams* params);
	CLASS_DECLSPEC void step();
	CLASS_DECLSPEC void fillState(ngnState * s);
	CLASS_DECLSPEC ~ngnBulletMB();
};

