#pragma once
#include <Common/Base/hkBase.h>
#include <Common/Base/Memory/System/Util/hkMemoryInitUtil.h>
#include <Common/Base/Memory/Allocator/Malloc/hkMallocAllocator.h>
#include <Common/Base/Fwd/hkcstdio.h>

// Physics
#include <Physics2012/Dynamics/World/hkpWorld.h>
#include <Physics2012/Dynamics/World/hkpPhysicsSystem.h>
#include <Physics2012/Collide/Dispatch/hkpAgentRegisterUtil.h>
#include <Physics2012/Dynamics/Entity/hkpRigidBody.h>
#include <Physics2012/Dynamics/Action/hkpBinaryAction.h>
#include <Physics/Constraint/Data/LimitedHinge/hkpLimitedHingeConstraintData.h>
#include <Physics/Constraint/Data/Hinge/hkpHingeConstraintData.h>
#include <Physics/Constraint/Data/hkpConstraintDataUtils.h>
#include <Physics/Constraint/Motor/Callback/hkpCallbackConstraintMotor.h>
#include <Physics2012/Collide/Shape/Convex/Triangle/hkpTriangleShape.h>
#include <Physics2012/Collide/Shape/Convex/Box/hkpBoxShape.h>
#include <Physics2012/Collide/Shape/Convex/Sphere/hkpSphereShape.h>
#include <Physics2012/Collide/Shape/Convex/Capsule/hkpCapsuleShape.h>
#include <Physics2012/Collide/Shape/Convex/ConvexTransform/hkpConvexTransformShape.h>
#include <Physics2012/Collide/Shape/Compound/Collection/List/hkpListShape.h>
#include <Physics2012/Collide/Filter/group/hkpGroupFilter.h>
#include <Physics2012/Utilities/Dynamics/Inertia/hkpInertiaTensorComputer.h>

// Platform specific initialization
#include <Common/Base/System/Init/PlatformInit.cxx>

#include "ngn.h"
#include "engine/engine.h"

#pragma once

class springDamperAction : public hkpBinaryAction
{
protected:
	hkVector4 axisA;
	hkVector4 axisB;
	hkVector4 jnt_posA;
	hkVector4 jnt_posB;
	hkTransform transDiff;
	mjtNum q_diff[4];
	double ref;
	double K;
	double D;
public:
	void applyAction( const hkStepInfo& stepInfo );
	springDamperAction(hkpRigidBody* _a, hkpRigidBody* _b,
		hkVector4 _axisA, hkVector4 _axisB,
		hkVector4 _jnt_posA, hkVector4 _jnt_posB,
		double qref, double _K, double _D);
	hkpAction* clone(const hkArray<hkpEntity*>&, const hkArray<hkpPhantom*>&) const { HK_ERROR(0xF894A7BF, "Clonning not implemented"); return HK_NULL; }
};
class torqueAction : public springDamperAction
{
private:
	int i;
	mjtNum* torqueVector;
public:
	void applyAction( const hkStepInfo& stepInfo );
	torqueAction(hkpRigidBody* _a, hkpRigidBody* _b, hkVector4 _axisA, hkVector4 _axisB, mjtNum* _torqueVector, int _i );
};

class ngnHavok : public ngn
{
protected:
	double time;
	float allowedPenetration;
	hkpWorld* world;
	int systemGroupID;
	int layer;
	void addChildren(int parentInx, hkpRigidBody* parent);
	hkpConvexTransformShape* createSingleShape(int geomInx);
	hkpShape* createShape(int bInx);
	vector<vector<int>> b2g;
public:
	__declspec(dllexport) ngnHavok(mjModel* _mjm, mjOption* _mjo, ngnParams *params);
	__declspec(dllexport) double getTime();
	__declspec(dllexport) void step();
	__declspec(dllexport) void fillState(ngnState * s);
	__declspec(dllexport) ~ngnHavok();

};

/*
static hkRotation mj2hk_m(const mjtNum* m)
{
	hkRotation res();
	hkRotation res();
	
	for( int i=0; i<3; i++ )
		for( int j=0; j<3; j++ )
			res(i,j)=m[i*3+j];
}
*/
static void hk2mj(mjtNum* res, const hkVector4 v, int n=3)
{
	for( int i=0; i<n; i++ )
		res[i]=(mjtNum)v(i);
}
static void hk2mj(mjtNum* res, const hkQuaternion v)
{
	res[0]=(mjtNum)v(3);
	for( int i=0; i<3; i++ )
		res[1+i]=(mjtNum)v(i);
}
