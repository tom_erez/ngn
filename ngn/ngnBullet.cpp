#include "ngnBullet.h"

ngnBullet::ngnBullet(mjModel* _mjm, ngnParams* params)
	:ngnBulletBase(_mjm, params)
{
	name = "Bullet";
	bodies.clear();
	bodies.resize(mjm->nbody);

	///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	// btDantzigSolver* mlcp = new btDantzigSolver();
	// btSolveProjectedGaussSeidel* mlcp = new btSolveProjectedGaussSeidel;
	// solver = new btMLCPSolver(mlcp);
/*
	if (useMCLPSolver)
	{
		m_dynamicsWorld ->getSolverInfo().m_minimumSolverBatchSize = 1;//for direct solver it is better to have a small A matrix
	} else
	{
		m_dynamicsWorld ->getSolverInfo().m_minimumSolverBatchSize = 128;//for direct solver, it is better to solve multiple objects together, small batches have high overhead
	}



		if (m_dynamicsWorld->getConstraintSolver()->getSolverType()==BT_MLCP_SOLVER)
		{
			btMLCPSolver* sol = (btMLCPSolver*) m_dynamicsWorld->getConstraintSolver();
			int numFallbacks = sol->getNumFallbacks();
			if (numFallbacks)
			{
				static int totalFailures = 0;
				totalFailures+=numFallbacks;
				printf("MLCP solver failed %d times, falling back to btSequentialImpulseSolver (SI)\n",totalFailures);
			}
			sol->setNumFallbacks(0);
		}
*/

	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,overlappingPairCache,solver,collisionConfiguration);

	dynamicsWorld->setGravity(btVector3(mjm->opt.gravity[0],
										mjm->opt.gravity[1],
										mjm->opt.gravity[2]));

	btContactSolverInfo &solverInfo = dynamicsWorld->getSolverInfo();
	solverInfo.m_globalCfm = m_globalCfm;
	solverInfo.m_erp = m_erp;
	solverInfo.m_numIterations = m_numIterations;
	solverInfo.m_maxGyroscopicForce = 100000;

	// WTF?!
	//solverInfo.m_minimumSolverBatchSize = 0;

	btStaticPlaneShape* groundGeom = new btStaticPlaneShape( btVector3(0,0,1),0);

	btDefaultMotionState* myMotionState = new btDefaultMotionState(btTransform(btQuaternion(0,0,0,1),btVector3(0,0,0)));
	btRigidBody::btRigidBodyConstructionInfo rbInfo(0, myMotionState, groundGeom, btVector3(0,0,0));
	rbInfo.m_friction = mjm->geom_friction[0];
	btRigidBody* body = new btRigidBody(rbInfo);
	dynamicsWorld->addRigidBody(body);
	bodies[0]=body;

	jointPairs.resize(mjm->njnt);

	try
	{
		addChildren(0, 0);
	}
	catch( ngnError e )
	{
		cout << e.errorMsg.c_str() << endl;
	}
	time = 0;
}

void ngnBullet::addChildren(int parentInx, btRigidBody* parent)
{
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		if( mjm->body_parentid[bInx]==parentInx )
		{
			btTransform bodyTransform;
			bodyTransform.setIdentity();
			bodyTransform.setOrigin(makebtVector3(mjd->xpos+bInx*3));
			bodyTransform.setRotation(mjMat2btQuat(mjd->xmat+bInx*9));

			btCollisionShape* shape = bodyGeoms(bInx);
			btDefaultMotionState* ms = new btDefaultMotionState(bodyTransform);

			//shape->calculateLocalInertia(mjm->body_mass[bInx],localInertia);


			btRigidBody::btRigidBodyConstructionInfo rbInfo(mjm->body_mass[bInx], ms, shape, makebtVector3(mjm->body_inertia+bInx*3));
			
			rbInfo.m_additionalAngularDampingFactor =
				rbInfo.m_additionalDamping = 
				rbInfo.m_additionalDampingFactor =
				rbInfo.m_angularDamping = 
				rbInfo.m_linearDamping = 
				rbInfo.m_restitution = 0;

			rbInfo.m_rollingFriction = mjm->geom_friction[2];
			rbInfo.m_friction = mjm->geom_friction[0];
			btRigidBody* child = new btRigidBody(rbInfo);
			dynamicsWorld->addRigidBody(child);
			bodies[bInx] = child;
			mjtNum temp[6];
			ngu_transformSpatial(temp, mjd->cvel+6*bInx, mjd->xipos+3*bInx, mjd->com_subtree+3*mjm->body_rootid[bInx]);
			child->setAngularVelocity(btVector3(temp[0], temp[1], temp[2]));
			child->setLinearVelocity(btVector3(temp[3], temp[4], temp[5]));
			child->setFlags(BT_ENABLE_GYROPSCOPIC_FORCE);

			btHingeConstraint* jId;
			int njnt=0, inx[6];
			// count joints, store indices
			for( int j=0; j<mjm->njnt; j++ )
				if( mjm->jnt_bodyid[j]==bInx )
				{
					inx[njnt]=j;
					njnt++;
				}

			if( njnt>1 )
				throw ngnError("we decided to support only 1 joint between parent and child");
			if( !njnt )
			{
				inx[0]=0;
				// TODO: weld parent and child
			}
			else // add a hinge joint
			{
				int j = inx[0];
				if( mjm->jnt_type[j] == mjJNT_HINGE )
				{
					if( !parent ) // connect to world
					{
						jId = new btHingeConstraint(*child, makebtVector3(mjm->jnt_pos+3*j), makebtVector3(mjm->jnt_axis+3*j).normalize());
						// limits:
						if( mjm->jnt_limited[j] )
						{
							btScalar a = jId->getHingeAngle();
							jId->setLimit(	a+mjm->jnt_range[2*j]-mjd->qpos[mjm->jnt_qposadr[j]],
											a+mjm->jnt_range[2*j+1]-mjd->qpos[mjm->jnt_qposadr[j]]);
						}
					}
					else
					{
						mjtNum frame[9], tempmat[9];
						mjtNum x2z[9];
						ngu_zero(frame, 9);
						ngu_addToScl3(frame, mjd->xaxis+3*j, 1);
						ngu_makeFrame(frame);
						// looking at btHingeConstraint.cpp, it seems like the rotation axis is Z (where?)
						ngu_zero(x2z, 9);
						x2z[2] = -1; x2z[4] = 1; x2z[6] = 1;
						ngu_mulMatMat(tempmat, x2z, frame, 3, 3, 3);
						ngu_transpose(frame, tempmat, 3, 3);
						btMatrix3x3 rot(frame[0], frame[1], frame[2],
										frame[3], frame[4], frame[5],
										frame[6], frame[7], frame[8]);
						btTransform globalJoint(rot, makebtVector3(mjd->xanchor+3*j));
						btTransform global2child = child->getCenterOfMassTransform();
						btTransform child2joint = global2child.inverseTimes(globalJoint);
						btTransform global2parent = parent->getCenterOfMassTransform();
						btTransform parent2joint = global2parent.inverseTimes(globalJoint);
						jId = new btHingeConstraint(*parent, *child, parent2joint, child2joint);
						/*
						if( !(mjo->disableflags & (mjDSBL_PASSIVE)) )
						{
							btGeneric6DofSpringConstraint* spring = new btGeneric6DofSpringConstraint(*parent, *child, parent2joint, child2joint, false);
							for( int i=0; i<6; i++ )
							{
								spring->enableSpring(i, false);
							}
							spring->enableSpring(5, true);
							spring->setStiffness(5,ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0)); 
							spring->setDamping(5, 1);//1.0/(1+ngu_rescale(mjo->s_damping, mjm->dof_damping[mjm->jnt_dofadr[j]], 0))); // this is a bullshit hack - bullet's implementation asks "what fraction of the current velocity do you want retained", so 1 is "no damping" and 0 is "full damping"
							spring->setEquilibriumPoint(5, jId->getHingeAngle() - mjm->qpos_spring[mjm->jnt_qposadr[j]] + mjd->qpos[mjm->jnt_qposadr[j]]);
							dynamicsWorld->addConstraint(spring, true);
						}
						*/
						// limits:
						if( mjm->jnt_limited[j] )
						{
							btScalar a = jId->getHingeAngle();
							jId->setLimit(	-a-mjm->jnt_range[2*j+1]+mjd->qpos[mjm->jnt_qposadr[j]],
											-a-mjm->jnt_range[2*j]+mjd->qpos[mjm->jnt_qposadr[j]]);
						}
					}
					dynamicsWorld->addConstraint(jId,true);
					btJointPair jp;
					jp.parent = parent;
					jp.child = child;
					jp.axisLocalChild = makebtVector3(mjm->jnt_axis+3*j).normalize();
					if( parent )
						jp.axisLocalParent = quatRotate(parent->getCenterOfMassTransform().inverse().getRotation(), makebtVector3(mjd->xaxis+3*j)).normalize();
					else
						jp.axisLocalParent = makebtVector3(mjd->xaxis+3*j).normalize();
					jp.jntAdr = j;
					jp.dofAdr = mjm->jnt_dofadr[j];
					jp.qposAdr = mjm->jnt_qposadr[j];
					jp.K = mjm->jnt_stiffness[j];
					jp.D = mjm->dof_damping[jp.dofAdr];
					if( parent )
						jp.ref = jId->getHingeAngle() - mjm->qpos_spring[jp.qposAdr] + mjd->qpos[jp.qposAdr];
					else
						jp.ref = jId->getHingeAngle() + mjm->qpos_spring[jp.qposAdr] - mjd->qpos[jp.qposAdr];
					jp.jId = jId;
					jointPairs[j] = jp;
				}
				else
				{
					if( parent )
						throw ngnError("only hinges are supported");
				}
			}
			// (yuval) try-catch here, to simply skip 
			addChildren(bInx, child);
		}
	}
}
ngnBullet::~ngnBullet()
{
	for (int i=dynamicsWorld->getNumConstraints()-1; i>=0 ;i--)
	{
		btTypedConstraint* obj = dynamicsWorld->getConstraint(i);
		dynamicsWorld->removeConstraint(obj);
		delete obj;
	}

	for (int i=dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		btCollisionShape* shape = body->getCollisionShape();
		if( shape->isCompound() )
		{
			for( int s=0; s<((btCompoundShape*)shape)->getNumChildShapes(); s++ )
				delete ((btCompoundShape*)shape)->getChildShape(s);
		}
		delete shape;
		dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	delete dynamicsWorld;
	delete solver;

	bodies.clear();

}


void ngnBullet::step()
{
	for( int i=0; i<mjm->nv; i++ )
	{
		int j = mjm->dof_jntid[i];
		if( mjm->jnt_type[j]==mjJNT_HINGE )
		{
			btJointPair jp = jointPairs[j];
			btVector3 axisGlobalParent, axisGlobalChild;
			axisGlobalChild = quatRotate(jp.child->getCenterOfMassTransform().getRotation(), jp.axisLocalChild);
			if( jp.parent )
				axisGlobalParent = quatRotate(jp.parent->getCenterOfMassTransform().getRotation(), jp.axisLocalParent);
			else
				axisGlobalParent = jp.axisLocalParent;
			btVector3 axisMean = 0.5*axisGlobalParent.normalize() + 0.5*axisGlobalChild.normalize();
			axisMean.normalize();
			
			if( !(mjm->opt.disableflags & (mjDSBL_PASSIVE)) )
			{
				btScalar velP = 0;
				if( jp.parent )
				{
					btVector3 avelParent(jp.parent->getAngularVelocity());
					velP = avelParent.dot(axisMean);
				}
				btVector3 avelChild(jp.child->getAngularVelocity());
				btScalar velC = avelChild.dot(axisMean);
				btScalar relvel = velP-velC;
				btScalar damperTorque = jp.D*relvel;

				btScalar offset = jp.jId->getHingeAngle() - jp.ref;
				btScalar springTorque = jp.K*offset;

				if( jp.parent )
					torques[i] += damperTorque + springTorque;
				else
					torques[i] += damperTorque - springTorque;
			}
			
			jp.child->applyTorque(axisMean*torques[i]);// /timestep);
			if( jp.parent )
				jp.parent->applyTorque(axisMean*-torques[i]);// /timestep);
		}
	}

	dynamicsWorld->stepSimulation(timestep, 1, timestep);
	time += timestep;
}


void ngnBullet::fillState(ngnState* state)
{
	state->time = getTime();

	for( int b=1; b<bodies.size(); b++ )
	{
		ngnPVState* bState = &state->bstates[b];
		btTransform tt = bodies[b]->getCenterOfMassTransform();
		btVector3 pPos = tt.getOrigin();
		if( (int)(state->time*1000) % 100 < 0.001 )
			cout << pPos[0] << ' '<< pPos[1] <<' '<< pPos[2] << endl;

		btQuaternion quat = tt.getRotation();
		btQuat2rot(&quat, bState->mat);
		for( int i=0; i<3; i++ )
			bState->pos[i] = (mjtNum)pPos[i];
		btVector3 avel = bodies[b]->getAngularVelocity();
		for( int i=0; i<3; i++ )
			bState->avel[i] = (mjtNum)avel[i];
		btVector3 xvel = bodies[b]->getLinearVelocity();
		for( int i=0; i<3; i++ )
			bState->xvel[i] = (mjtNum)xvel[i];

		copyQuat(&quat, mjd->xquat);
		ngu_copy3(mjd->xpos, bState->pos);
		ngu_copy(mjd->xmat, bState->mat, 9);
		// xquat was written above
		for( int g=0; g<b2g[b].size(); g++ )
		{
			int gInx = b2g[b][g];
			ngnPState* gState = &state->gstates[gInx];
				ngu_local2Global(mjd->xpos, mjd->xmat, mjd->xquat, mjm->geom_pos+3*gInx, mjm->geom_quat+4*gInx, gState->pos, gState->mat);
		}

	}
}
