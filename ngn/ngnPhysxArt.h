#include "ngnPhysXBase.h"

using namespace physx;


class ngnPhysxArt : public ngnPhysxBase
{
	PxArticulationLink** buffer;
	vector<PxArticulation*>	articulations;
	vector<vector<int>> a2b; // from articulation and link to body index
public:
	__declspec(dllexport) ngnPhysxArt(mjModel* _mjm, mjOption* _mjo, ngnParams *params);
	void addChildren(int parentInx, PxArticulationLink* parent, vector<int> &l2b);
	__declspec(dllexport) void fillState(ngnState * s);
};