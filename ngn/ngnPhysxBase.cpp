#include "ngnPhysxBase.h"
#include <string>
using namespace physx;
using namespace std;

class myhandler : public PxAssertHandler
{
public:
	~myhandler() {}
	void operator()(const char* exp, const char* file, int line, bool& ignore)
	{
		ignore=true;
	}
};


PxDefaultAllocator		gAllocator;
PxDefaultErrorCallback	gErrorCallback;
myhandler h;

ngnPhysxBase::ngnPhysxBase(mjModel* _mjm, mjOption* _mjo, ngnParams *params)
	:ngn(_mjm, _mjo, params)
{
	PxSetAssertHandler(h);
	// defaults:
	jointAngularTolerance=1e-2;
	jointLinearTolerance=1e-2;
	for( int i=0; i<mjm->nnumeric; i++ )
	{
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"joint angular tolerance") )
			jointAngularTolerance = mjm->numeric_data[mjm->numeric_adr[i]];
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"joint linear tolerance") )
			jointLinearTolerance = mjm->numeric_data[mjm->numeric_adr[i]];
	}

	gFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gAllocator, gErrorCallback);
	PxProfileZoneManager* profileZoneManager = &PxProfileZoneManager::createProfileZoneManager(gFoundation);
	gPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *gFoundation, PxTolerancesScale(),true,profileZoneManager);
	PxSceneDesc sceneDesc(gPhysics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(	(PxReal)mjo->gravity[0],
								(PxReal)mjo->gravity[1],
								(PxReal)mjo->gravity[2]);
	gDispatcher = PxDefaultCpuDispatcherCreate(2);
	sceneDesc.cpuDispatcher	= gDispatcher;
	sceneDesc.filterShader	= PxDefaultSimulationFilterShader;
	gScene = gPhysics->createScene(sceneDesc);

	PxMaterial*				gMaterial	= NULL;
	gMaterial = gPhysics->createMaterial(0.5f, 0.5f, 0.6f);
	PxRigidStatic* groundPlane = PxCreatePlane(*gPhysics, PxPlane(0,0,1,0), *gMaterial);
	gMaterial->release();
	gScene->addActor(*groundPlane);

	b2g.resize(mjm->nbody);
	jointPairs.resize(mjm->njnt);
}

ngnPhysxBase::~ngnPhysxBase()
{
	gScene->release();
	gDispatcher->release();
	PxProfileZoneManager* profileZoneManager = gPhysics->getProfileZoneManager();
	gPhysics->release();	
	profileZoneManager->release();
	gFoundation->release();
}

void ngnPhysxBase::step()
{
	for( int i=0; i<mjm->nv; i++ )
	{
		int j = mjm->dof_jntid[i];
		pxJointPair jp = jointPairs[j];
		//double offset = jp.joint->getTwist() - jp.ref;
		if( mjm->jnt_type[j]==mjJNT_HINGE )
		{
			PxVec3 axisGlobalParent, axisGlobalChild;
			axisGlobalChild = jp.child->getGlobalPose().rotate(jp.axisLocalChild);
			if( jp.parent )
				axisGlobalParent = jp.parent->getGlobalPose().rotate(jp.axisLocalParent);
			else
				axisGlobalParent = jp.axisLocalParent;
			axisGlobalParent.normalize();
			axisGlobalChild.normalize();
			PxVec3 axisMean = 0.5*axisGlobalParent + 0.5*axisGlobalChild;
			axisMean.normalize();
			if( !(mjo->disableflags & (mjDSBL_PASSIVE)) && !jp.joint ) // we only need to add spring torques manually to articulated - cartesian has built-in functionality
			{
				double velP = 0;
				if( jp.parent )
				{
					PxVec3 avelParent(jp.parent->getAngularVelocity());
					velP = avelParent.dot(axisMean);
				}
				PxVec3 avelChild(jp.child->getAngularVelocity());
				double velC = avelChild.dot(axisMean);
				double relvel = velP-velC;
				double damperTorque = jp.D*relvel;
				double offset;
				PxTransform relT;
				if( jp.parent )
					relT = jp.parent->getGlobalPose().transformInv(jp.child->getGlobalPose())*jp.initFrameDiff;
				else
					relT = jp.child->getGlobalPose()*jp.initFrameDiff;
				PxVec3 aa(relT.q.x,relT.q.y,relT.q.z);
				aa.normalize();
				double dir = aa.dot(axisMean);
				double angle = relT.q.getAngle()*(dir>0?1:-1);
				offset = angle - jp.ref;
				double springTorque = jp.K*-offset;

				torques[i] += damperTorque + springTorque;
			}
			jp.child->addTorque(axisMean*torques[i]);///timestep);
			if( jp.parent )
				jp.parent->addTorque(axisMean*-torques[i]);///timestep);
		}
	}
	
	try {
		gScene->simulate(timestep);
		gScene->fetchResults(true);
	}
	catch( void* e )
	{
		throw ngnError("physx crashed");
	}
}


PxTransform ngnPhysxBase::getTransform(int inx, const mjtNum* xpos, const mjtNum* xmat, const mjtNum* quat)
{
	if(xmat && quat )
		throw ngnError("BUG: both xmat and quat were provided to getTransform");
	int geomInx=0, bodyInx=0;
	if( inx<0 )
		geomInx = -inx;
	else if( inx>=0 )
		bodyInx = inx;
	if( !xpos )
	{
		if( bodyInx>=0 )
			xpos = mjd->xpos+3*bodyInx;
		else
			xpos = mjd->geom_xpos+3*geomInx;
	}
	if( !xmat )
	{
		if( bodyInx>=0 )
			xmat = mjd->xmat+9*bodyInx;
		else
			xmat = mjd->geom_xmat+9*geomInx;
	}

	PxQuat q = (quat ? mjQuat2PxQuat(quat) : mjMat2PxQuat(xmat));
	PxTransform ret =  PxTransform(	(PxReal)xpos[0],
									(PxReal)xpos[1],
									(PxReal)xpos[2],
									q);

	if( geomInx && mjm->geom_type[geomInx]==mjGEOM_CAPSULE )
	{
		PxTransform roty = PxTransform(	0, 0, 0, PxQuat((PxReal)-mjPI/2, PxVec3(0,1,0)));
		return ret.transform(roty);
	}
	else
		return ret;
}

void ngnPhysxBase::attachGeom(PxRigidBody* child, int geomInx)
{
	PxMaterial*	gMaterial	=  gPhysics->createMaterial((PxReal)mjm->geom_friction[geomInx*3], (PxReal)mjm->geom_friction[geomInx*3], 0.0f);
	PxShape* shape;
	switch( mjm->geom_type[geomInx] )
	{
	case mjGEOM_BOX:
		shape = child->createShape(PxBoxGeometry(	(PxReal)mjm->geom_size[3*geomInx],
													(PxReal)mjm->geom_size[3*geomInx+1],
													(PxReal)mjm->geom_size[3*geomInx+2]),
									*gMaterial);
		break;
	case mjGEOM_SPHERE:
		shape = child->createShape(PxSphereGeometry((PxReal)mjm->geom_size[3*geomInx]),
									*gMaterial);
		break;
	case mjGEOM_CAPSULE:
		shape = child->createShape(PxCapsuleGeometry(	(PxReal)mjm->geom_size[3*geomInx],
									(PxReal)mjm->geom_size[3*geomInx+1]),
									*gMaterial);
		break;
	default:
		throw ngnError("only box, sphere and capsule supported for now");
	}
	shape->setLocalPose(getTransform(-geomInx, mjm->geom_pos+3*geomInx, 0, mjm->geom_quat+4*geomInx));

	gMaterial->release();
}

void ngnPhysxBase::transform2PState(PxTransform t, ngnPState* gstate)
{
	gstate->pos[0]=t.p[0];
	gstate->pos[1]=t.p[1];
	gstate->pos[2]=t.p[2];

	mjtNum quat[4];
	quat[0] = t.q.w;
	quat[1] = t.q.x;
	quat[2] = t.q.y;
	quat[3] = t.q.z;
	ngu_normalize(quat, 4);
	ngu_quat2Mat(gstate->mat, quat);
}

double ngnPhysxBase::getTime()
{
	return (gScene->getTimestamp()-1)*timestep;
}

