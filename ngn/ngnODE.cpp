#include "ngnODE.h"

ngnODE::ngnODE(mjModel* _mjm, ngnParams* params)
	:ngn(_mjm, params)
{
	name = "ODE";
	joints.clear();
	b2g.resize(mjm->nbody);
	joints.resize(mjm->njnt);
	jointPairs.resize(mjm->njnt);
	bodies.clear();
	bodies.resize(mjm->nbody);
	dInitODE2(0);
	dAllocateODEDataForThread(dAllocateMaskAll);
	wId = dWorldCreate();
	dWorldSetContactSurfaceLayer(wId, 0); // should be the default
	dWorldSetDamping (wId, 0, 0); // should be the default
	//dWorldSetQuickStepW - not touching for now
	dWorldSetStepIslandsProcessingMaxThreadCount(wId, 1);

	numiter = 0; // 60?
	// grab parameters from custom fields:
	for( int i=0; i<mjm->nnumeric; i++ )
	{
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"CFM") )
			dWorldSetCFM(wId,mjm->opt.timestep*mjm->numeric_data[mjm->numeric_adr[i]]);
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"ERP") )
		{
			dReal mjerp = mjm->numeric_data[mjm->numeric_adr[i]];
			//mjerp = pow(mjerp, sqrt(1e-3/mjo->timestep));
			dWorldSetERP(wId,mjerp);
		}
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"numiter") )
			numiter = mjm->numeric_data[mjm->numeric_adr[i]];
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"layer") )
			dWorldSetContactSurfaceLayer(wId, mjm->numeric_data[mjm->numeric_adr[i]]);
	}
	if( numiter )
		dWorldSetQuickStepNumIterations (wId, numiter);



	dWorldSetGravity(wId,	(dReal)mjm->opt.gravity[0],
							(dReal)mjm->opt.gravity[1],
							(dReal)mjm->opt.gravity[2]);
	sId = dSimpleSpaceCreate(0);
	//sId = dHashSpaceCreate(0);
	ground = dCreatePlane(sId, 0, 0, 1, 0);
	try
	{
		addChildren(0, 0);
	}
	catch( ngnError e )
	{
		cout << e.errorMsg.c_str() << endl;
	}
	time = 0;
	contactGroup = dJointGroupCreate(0);
}

void ngnODE::attachGeom(dBodyID child, int bInx, int geomInx)
{
	dGeomID geom;
	switch( mjm->geom_type[geomInx] )
	{
	case mjGEOM_BOX:
		geom = dCreateBox(sId,	(dReal)mjm->geom_size[3*geomInx]*2,
								(dReal)mjm->geom_size[3*geomInx+1]*2,
								(dReal)mjm->geom_size[3*geomInx+2]*2);
		break;
	case mjGEOM_SPHERE:
		geom = dCreateSphere(sId, (dReal)mjm->geom_size[3*geomInx]);
		break;
	case mjGEOM_CAPSULE:
		geom = dCreateCapsule(sId,	(dReal)mjm->geom_size[3*geomInx], //radius
									(dReal)mjm->geom_size[3*geomInx+1]*2); //length, not half-length!!
		break;
	default:
		throw ngnError("only box, sphere and capsule supported for now");
	}
	dGeomSetBody(geom, child);
	dGeomSetOffsetPosition(geom,	(dReal)mjm->geom_pos[3*geomInx],
									(dReal)mjm->geom_pos[3*geomInx+1],
									(dReal)mjm->geom_pos[3*geomInx+2]);
	dQuaternion Q;
	for( int i=0; i<4; i++ )
		Q[i]=(dReal)mjm->geom_quat[4*geomInx+i];

	dGeomSetOffsetQuaternion(geom, Q);
}

void ngnODE::addChildren(int parentInx, dBodyID parentID)
{
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		dBodyID childID;
		if( mjm->body_parentid[bInx]==parentInx )
		{

		// add new body
			childID = dBodyCreate(wId);
			bodies[bInx]=childID;
			dBodySetPosition(childID,	(dReal)mjd->xpos[bInx*3],
										(dReal)mjd->xpos[bInx*3+1],
										(dReal)mjd->xpos[bInx*3+2]);
			dMatrix3 R;
			dRSetIdentity(R);
			for( int i=0; i<3; i++ )
				for( int j=0; j<3; j++ )
					R[i*4+j] = (dReal)mjd->xmat[bInx*9+i*3+j];
			dBodySetRotation(childID, R);

			mjtNum temp[6];
			ngu_transformSpatial(temp, mjd->cvel+6*bInx, mjd->xipos+3*bInx, mjd->com_subtree+3*mjm->body_rootid[bInx]);
			dBodySetAngularVel(childID, temp[0], temp[1], temp[2]);
			dBodySetLinearVel(childID,  temp[3], temp[4], temp[5]);

			dBodySetDamping(childID, 0, 0);

			vector<int> gs;
			for( int gInx=0; gInx<mjm->ngeom; gInx++ )
				if( mjm->geom_bodyid[gInx]==bInx )
				{
					gs.push_back(gInx);
					attachGeom(childID, bInx, gInx);
				}
			b2g[bInx] = gs;
			
			dMass* mass = new dMass;//dMassSetParameters;
			mass->setZero();
			mass->mass = (dReal)mjm->body_mass[bInx];
			//dMassTranslate(mass,	,
			//						(dReal)mjm->body_ipos[bInx*3+1],
			//						(dReal)mjm->body_ipos[bInx*3+2]);
			for( int i=0; i<3; i++ )
			{
				mass->I[i*4+i] = (dReal)mjm->body_inertia[bInx*3+i];
			}
			dRSetIdentity(R);
			dQuaternion Q;
			for( int i=0; i<4; i++ )
				Q[i]=(dReal)mjm->body_iquat[4*bInx+i];
			dRfromQ(R, Q);
			dMassRotate(mass, R);
			dBodySetMass(childID, mass);
			delete mass;
		// create joint:
			dJointID jId;
			int njnt=0, inx[6];
			// count joints, store indices
			for( int j=0; j<mjm->njnt; j++ )
				if( mjm->jnt_bodyid[j]==bInx )
				{
					inx[njnt]=j;
					njnt++;
				}

			if( njnt>1 )
				throw ngnError("we decided to support only 1 joint between parent and child");
			if( !njnt )
			{
				throw ngnError("add welds to odes!");
				// TODO: weld parent and child
			}
			else // add a hinge joint
			{
				int j = inx[0];
				if( mjm->jnt_type[j] == mjJNT_HINGE )
				{
					jId = dJointCreateHinge(wId, 0);
					dJointAttach(jId, childID, parentID);
					dJointSetHingeAnchor(jId,	(dReal)mjd->xanchor[3*j],
												(dReal)mjd->xanchor[3*j+1],
												(dReal)mjd->xanchor[3*j+2]);
					dJointSetHingeAxis(jId,	(dReal)mjd->xaxis[3*j],
											(dReal)mjd->xaxis[3*j+1],
											(dReal)mjd->xaxis[3*j+2]);

					dJointSetHingeParam(jId, dParamBounce, 0);
					dJointSetHingeParam(jId, dParamFudgeFactor, 1);
					dJointSetHingeParam(jId, dParamERP, dWorldGetERP(wId));
					dJointSetHingeParam(jId, dParamCFM, dWorldGetCFM(wId));
					if( mjm->jnt_limited[j] )
					{
						dJointSetHingeParam(jId, dParamLoStop, mjm->jnt_range[2*j]);
						dJointSetHingeParam(jId, dParamHiStop, mjm->jnt_range[2*j+1]);
						dJointSetHingeParam(jId, dParamStopCFM, dWorldGetCFM(wId));
						dJointSetHingeParam(jId, dParamStopERP, dWorldGetERP(wId));
					}
					else
					{
						dJointSetHingeParam(jId, dParamLoStop, -dInfinity);
						dJointSetHingeParam(jId, dParamHiStop, dInfinity);
					}
					/*
					double h = mjo->timestep;
					// rescale!
					double kp = mjm->jnt_stiffness[j];
					double kd = mjm->dof_damping[mjm->jnt_dofadr[j]];
					if( !(mjo->disableflags & (mjDSBL_PASSIVE)) && h*kp + kd > 0 )
					{
						dJointSetHingeParam(jId, dParamLoStop, dJointGetHingeAngle(jId));
						dJointSetHingeParam(jId, dParamHiStop, dJointGetHingeAngle(jId));
						dJointSetHingeParam(jId, dParamStopERP, h*kp / (h*kp + kd));
						dJointSetHingeParam(jId, dParamStopCFM, 1 / (h*kp + kd));
					}
					else
					{
					}
					*/
					jointPair jp;
					jp.jntAdr = j;
					jp.dofAdr = mjm->jnt_dofadr[j];
					jp.qposAdr = mjm->jnt_qposadr[j];
					double stiffness = mjm->jnt_stiffness[j];
					double damping = mjm->dof_damping[mjm->jnt_dofadr[j]];
					jp.K = stiffness;
					jp.D = damping;
					//if( parent )
					//	jp.ref = dJointGetHingeAngle(jId) - mjm->qpos_spring[jp.qposAdr] + mjd->qpos[jp.qposAdr];
					//else
						jp.ref = dJointGetHingeAngle(jId) + mjm->qpos_spring[jp.qposAdr] - mjd->qpos[jp.qposAdr];
					//jp.jId = jId;
					jointPairs[j] = jp;
					joints[j] = jId;
				}
				else
				{
					if( parentID )
						throw ngnError("only hinges are supported");
				}

			}
			addChildren(bInx, childID);
		}
	}
}
ngnODE::~ngnODE()
{
	dJointGroupDestroy(contactGroup);
	dSpaceDestroy(sId);
	dWorldDestroy(wId);
	dCloseODE();
	joints.clear();
	bodies.clear();

}

#define MAX_CONTACTS 4
void nearCallback(void *v, dGeomID o1, dGeomID o2)
{
  int i;
  ngnODE* me = (ngnODE*)v;

  // exit without doing anything if the two bodies are connected by a joint
  dBodyID b1 = dGeomGetBody(o1);
  dBodyID b2 = dGeomGetBody(o2);
  if (b1 && b2 && dAreConnectedExcluding (b1,b2,dJointTypeContact)) return;

  dContact contact[MAX_CONTACTS];
  for (i=0; i<MAX_CONTACTS; i++) {
	contact[i].surface.mode = dContactApprox1;//dContactBounce | dContactSoftCFM;
	contact[i].surface.mu = me->mjm->geom_friction[0];
	contact[i].surface.mu2 = me->mjm->geom_friction[0];
	//contact[i].surface.bounce = 0;
	//contact[i].surface.bounce_vel = 0;
	//contact[i].surface.soft_cfm = 0.01;
  }
  if (int numc = dCollide (o1,o2,MAX_CONTACTS,&contact[0].geom,
			   sizeof(dContact))) {
	for (i=0; i<numc; i++) {
		dJointID c = dJointCreateContact (me->wId, me->contactGroup, contact+i);
		dJointAttach (c,b1,b2);
	}
  }
}



void ngnODE::step()
{
	dSpaceCollide(sId, this, &nearCallback);

	for( int i=0; i<mjm->nv; i++ )
	{
		int j = mjm->dof_jntid[i];
		if( mjm->jnt_type[j]==mjJNT_HINGE )
		{
			if( !(mjm->opt.disableflags & (mjDSBL_PASSIVE)) )
			{
				jointPair jp = jointPairs[j];
				double relvel = dJointGetHingeAngleRate(joints[j]);
				double damperTorque = jp.D*-relvel;

				double offset = dJointGetHingeAngle(joints[j]) - jp.ref;
				double springTorque = jp.K*-offset;

				torques[i] += damperTorque + springTorque;
			}
			dJointAddHingeTorque(joints[j], torques[i]);
		}
	}

	if( numiter )
		dWorldQuickStep(wId, timestep);
	else
		dWorldStep(wId, timestep);

	dJointGroupEmpty(contactGroup);

	time += timestep;
}

double ngnODE::getTime()
{
	return time;
}

void ngnODE::fillState(ngnState* state)
{
	state->time = getTime();
	for( int b=1; b<bodies.size(); b++ )
	{
		ngnPVState* bState = &state->bstates[b];
		dBodyID bId = bodies[b];
		const dReal* pPos = dBodyGetPosition(bId);
		const dReal* pxvel = dBodyGetLinearVel(bId);
		const dReal* pavel = dBodyGetAngularVel(bId);
		const dReal* pQ = dBodyGetQuaternion(bId);
		mjtNum q[4];
		for( int i=0; i<4; i++ )
			q[i] = (mjtNum)pQ[i];
		ngu_normalize(q, 4);
		ngu_quat2Mat(bState->mat, q);
		for( int i=0; i<3; i++ )
		{
			bState->pos[i] = (mjtNum)pPos[i];
			bState->avel[i] = (mjtNum)pavel[i];
			bState->xvel[i] = (mjtNum)pxvel[i];
		}
		
		ngu_copy(mjd->xquat, q, 4);
		ngu_copy3(mjd->xpos, bState->pos);
		ngu_copy(mjd->xmat, bState->mat, 9);
		for( int g=0; g<b2g[b].size(); g++ )
		{
			int gInx = b2g[b][g];
			ngnPState* gState = &state->gstates[gInx];
				ngu_local2Global(mjd->xpos, mjd->xmat, mjd->xquat, mjm->geom_pos+3*gInx, mjm->geom_quat+4*gInx, gState->pos, gState->mat);
		}
	}
}
