#include "ngnPhysXBase.h"

using namespace physx;

class ngnPhysxRigid : public ngnPhysxBase
{
private:
	void addChildren(int parentInx, PxRigidDynamic* parent);
public:
	__declspec(dllexport) ngnPhysxRigid(mjModel* _mjm, mjOption* _mjo, ngnParams* params);
	__declspec(dllexport) void fillState(ngnState * s);
};