#include "stdlib.h"
#include "mujoco.h"

#include "TrajOps.h"

#include "ngnODE.h"
#include "ngnMujoco.h"
#include "ngnBullet.h"
#include "ngnBulletMB.h"

#include <algorithm>
#include <string>
using namespace std;



template<class T>
void simulate(mjModel* model, ngnParams params, mjtNum* X0)
{
	int total_time = mjGetTimeHR(); 
	double sim_time = 0, dtms;
	//string dtstr;
	double basedt = model->opt.timestep, dt = model->opt.timestep;
	int baseNumiter, iterIndex=-1;
	int nq = model->nq, nv = model->nv;

	for( int s=0; s<params.nSegments; s++ )
		for( int i=0; i<=params.range[1]-params.range[0]; i++ )
		{
			if (params.rangeType == 0) // modify timestep
			{
				dt = basedt*pow(2.0, i+params.range[0]);
				model->opt.timestep = dt;
			}
			else		// modify iterations
			{
						// grab parameters from custom fields:
				for( int j=0; j<model->nnumeric; j++ )
				{
					if( !strcmp(model->names+model->name_numericadr[j],"numiter") )
					{
						iterIndex = model->numeric_adr[j];
						baseNumiter = model->numeric_data[iterIndex];
						model->numeric_data[iterIndex] *= pow(2.0, i+params.range[0]);
						cout << " iterations = " << model->numeric_data[iterIndex] <<  endl;
					}
				}

			}

			// set initial state
			if(X0)
			{
				ngu_copy(model->key_qpos, X0 + s*(nq+nv), nq);
				ngu_copy(model->key_qvel, X0 + s*(nq+nv) + nq, nv);
			}

			T* ng = new T(model, &params);
			dtms = 1000*dt;

			string dtstr = (dtms >= 1.0 ? to_string((int)dtms) : "1/" + to_string((int)(1/dtms)));
			cout << model->names << " " << ng->name << " dt = " << dtstr << "ms" << endl;

			ngnTraj* traj = ng->integrate(0,params.duration); //timing(duration);

			if( iterIndex >= 0 )
				model->numeric_data[iterIndex] = baseNumiter;

			for( int j=1; j<(int)traj->states.size(); j++ )
				sim_time += traj->states[j].CPUtime;

			calcMomentum(ng, traj);
			calcEnergy(ng, traj);
			calcPenetration(ng, traj);

			char num[3]={'0','0','0'};
			sprintf(num,"%02d",i+1);
			string str = params.outdir + ng->name + "-" + num;

			
			if( params.nSegments > 1 )
			{
				char num[3]={'0','0','0'};
				sprintf(num,"%02d",s+1);
				str = str + "-" + num;
			}
			str += ".dat";
			writeTraj(traj, str.c_str());
			delete ng;
		}


	total_time = mjGetTimeHR() - total_time;

	cout << "step time: " << sim_time;
	cout << ", other: " << 1e-6*(double)(total_time)-sim_time;
	cout << endl;
}


int main(int nin, char** cin)
{

	//mjData* data = mj_makeData(model, &option);
	ngnParams params;
	params.keyframe=0;

	string modelfile="test.xml";
	if( nin>1 )
	{
		modelfile = string(cin[1]);
	}

	string engineName;
	engineName.clear();
	if( nin>2 )
	{
		engineName = string(cin[2]);
		transform(engineName.begin(), engineName.end(), engineName.begin(), ::tolower);
	}

	//if( nin>3 )
	//{
	//	string modifier = string(cin[3]);
	//	if( modifier[0]=='-')
	//	{
	//		if( modifier == "-single" )
	//		{
	//			// single trajectory
	//		}
	//	}
	//}

	if( nin>3 )
	{
		cout << "too many input agruments" << endl;
		getchar();
		return 1;
	}

	// get output directory: less ugly way to do this?
	int lastslash = modelfile.rfind("/");
	lastslash = lastslash == modelfile.npos ? 0 : lastslash;
	int lastbslash = modelfile.rfind("\\");
	lastbslash = lastbslash == modelfile.npos ? 0 : lastbslash;
	int slash = max(lastslash, lastbslash);
	int dot = modelfile.rfind(".");
	string fname = modelfile.substr(slash+1,dot-slash-1);
	params.outdir = params.outdir + fname + "/" ;


	mjModel* model=0;
	// load mj xml:
	char errmsg[300];

	// not .mjb extension: convert
	model = mj_loadXML(modelfile.c_str(), errmsg);

	if( !model )
	{
		cout << "could not load specified file:\n" << errmsg << endl;
		getchar();
		return 1;
	}

	mjtNum* X0 = 0; 

	try
	{
		// grab parameters from custom fields:
		for( int i=0; i<model->nnumeric; i++ )
		{
			if( !strcmp(model->names+model->name_numericadr[i],"duration") )
				params.duration = model->numeric_data[model->numeric_adr[i]];
			if( !strcmp(model->names+model->name_numericadr[i],"iterRange") )
				params.rangeType = 1;
			if( !strcmp(model->names+model->name_numericadr[i],"dtrange") )
			{
				if( model->numeric_size[i]!=2 )
					throw ngnError("exactly two values are needed to simulate a range of timesteps - integers that are the power-of-two in seconds (e.g., '-2 4' means simulating all dt's from 0.25s to 16s)");
				params.range[0] = (int)model->numeric_data[model->numeric_adr[i]];
				params.range[1] = (int)model->numeric_data[model->numeric_adr[i]+1];
			}
			if( !strcmpi(model->names+model->name_numericadr[i],"CPG") )
			{
				int nAct = model->numeric_size[i];
				if( nAct<3 || nAct%3 !=0 )
					throw ngnError("CPG triplets: [amplitude frequency phase]");
				params.nAct = nAct/3;
				ngu_copy(params.actuator, model->numeric_data+model->numeric_adr[i], nAct);
			}
			if( !strcmpi(model->names+model->name_numericadr[i],"CFMrange") )
			{

			}
		}

		// initial states for multiple segments
		for( int i=0; i<model->nnumeric; i++ )
		{
			if( !strcmp(model->names+model->name_numericadr[i],"consistency") )
			{
				int nx = model->nq + model->nv;
				params.nSegments = model->numeric_data[model->numeric_adr[i]];
				X0 = (mjtNum*)malloc(nx*params.nSegments*sizeof(mjtNum));
				mjtNum dt = model->opt.timestep*pow(2.0,params.range[0]);
				int N = (int)(params.duration/dt);
				int segDist = N/(params.nSegments+1);
				// use stiff contact parameters ?
				mjOption stiffOpt = model->opt;
				/*
				TODO(TASSA)
				stiffOpt.s_timeconst[0] = 1e-3;
				stiffOpt.s_timeconst[1] = 0;
				stiffOpt.s_compliance[0] = 1e-5;
				stiffOpt.s_timeconst[1] = 0;
				stiffOpt.timestep = dt;
				*/
				ngnMujoco* mj = new ngnMujoco(model, &params);
				cout << "initial trajectory..." << endl;
				for( int seg=0; seg<params.nSegments; seg++ )
				{
					mj->integrate(0, segDist*dt);
					mj->fillQState(X0 + seg*nx);
					mj->clearTrajVector();
				}
				delete mj;

				params.duration = 2*model->opt.timestep*pow(2.0,params.range[1]); // segment duration
				params.keyframe = 0;		// use the first keyframe to set initial state
			}
		}
	}
	catch( ngnError e )
	{
		cout << e.errorMsg.c_str() << endl;
		return 1;
	}


	#define NGN_ENGINES \
	X(ngnMujoco) \
	X(ngnMujocoRK4) \
	X(ngnODE) \
	X(ngnBullet) \
	X(ngnBulletMB) 

	if( engineName.length()>0 )
	{

		if( engineName=="mujoco_euler" )
			simulate<ngnMujoco>(model, params, X0);
		else if( engineName=="mujoco_rk" )
			simulate<ngnMujocoRK4>(model, params, X0);
		else if( engineName=="ode" )
			simulate<ngnODE>(model, params, X0);
		else if( engineName=="bullet" )
			simulate<ngnBullet>(model, params, X0);
		else if( engineName=="bulletmb" )
			simulate<ngnBulletMB>(model, params, X0);
		else
		{
			cout << "engine name not recognized!" << endl;
			return 1;
		}
	}
	else
	{
		#define X(name) simulate<name>(model, params, X0);
			NGN_ENGINES
		#undef X
	}	

	if(X0)
		free(X0);

//	getchar();
//	Sleep(1000);
	return 0;
}

