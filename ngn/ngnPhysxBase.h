#include "PxPhysicsAPI.h"
#include "ngn.h"
#pragma once
using namespace physx;

struct pxJointPair : public jointPair
{
	PxRigidBody* parent;
	PxRigidBody* child;
	PxVec3 axisLocalChild;
	PxVec3 axisLocalParent;
	PxD6Joint* joint;
	PxTransform initFrameDiff;
};


class ngnPhysxBase : public ngn
{
protected:
	PxFoundation*			gFoundation;
	PxPhysics*				gPhysics;

	PxDefaultCpuDispatcher*	gDispatcher;
	PxScene*				gScene;
	vector<pxJointPair> jointPairs;
	PxTransform getTransform(int gInx, const mjtNum* xpos=0, const mjtNum* xmat=0, const mjtNum* quat=0);
	void attachGeom(PxRigidBody* child, int gInx);
	mjtNum jointLinearTolerance;
	mjtNum jointAngularTolerance;
	vector<vector<int>> b2g;
public:
	ngnPhysxBase(mjModel* _mjm, mjOption* _mjo, ngnParams* params);
	__declspec(dllexport) double getTime();
	__declspec(dllexport) void step();
	void transform2PState(PxTransform t, ngnPState* pstate);
	__declspec(dllexport) ~ngnPhysxBase();
};


static PxQuat mjMat2PxQuat(const mjtNum* xmat)
{
	PxVec3 xmatCol1 = PxVec3(	(PxReal)xmat[0],
								(PxReal)xmat[3],
								(PxReal)xmat[6]);
	PxVec3 xmatCol2 = PxVec3(	(PxReal)xmat[1],
								(PxReal)xmat[4],
								(PxReal)xmat[7]);
	PxVec3 xmatCol3 = PxVec3(	(PxReal)xmat[2],
								(PxReal)xmat[5],
								(PxReal)xmat[8]);
	PxMat33 pxmat = PxMat33(xmatCol1, xmatCol2, xmatCol3);
	return PxQuat(pxmat);
}

static PxQuat mjQuat2PxQuat(const mjtNum* quat)
{
	return PxQuat(	(PxReal)quat[1], 
					(PxReal)quat[2],
					(PxReal)quat[3],
					(PxReal)quat[0]);
}
static void pxQuat2mjQuat(const PxQuat quat, mjtNum* mjquat)
{
	mjquat[0]=quat.w;
	mjquat[1]=quat.x;
	mjquat[2]=quat.y;
	mjquat[3]=quat.z;
}
static PxVec3 pxMakeVector3(const mjtNum* vec)
{
	return PxVec3(	(PxReal)vec[0], 
					(PxReal)vec[1],
					(PxReal)vec[2]);
}