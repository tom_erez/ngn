#pragma once


#define mjUSEDOUBLE
#include "mujoco.h"

#define BASE_DECLSPEC

//------------------------------ 3D vector and matrix-vector operations --------------

// set vector to zero
BASE_DECLSPEC void ngu_zero3(mjtNum* res);

// copy vector
BASE_DECLSPEC void ngu_copy3(mjtNum* res, const mjtNum* data);

// scale vector
BASE_DECLSPEC void ngu_scl3(mjtNum* res, const mjtNum* vec, mjtNum scl);

// add vectors
BASE_DECLSPEC void ngu_add3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2);

// subtract vectors
BASE_DECLSPEC void ngu_sub3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2);

// add to vector
BASE_DECLSPEC void ngu_addTo3(mjtNum* res, const mjtNum* vec);

// add scaled to vector
BASE_DECLSPEC void ngu_addToScl3(mjtNum* res, const mjtNum* vec, mjtNum scl);

// normalize vector, return length before normalization
BASE_DECLSPEC mjtNum ngu_normalize3(mjtNum* res);

// compute vector length (without normalizing)
BASE_DECLSPEC mjtNum ngu_norm3(const mjtNum* res);

// vector dot-product
BASE_DECLSPEC mjtNum ngu_dot3(const mjtNum* vec1, const mjtNum* vec2);

// Cartesian distance between 3D vectors
BASE_DECLSPEC mjtNum ngu_dist3(const mjtNum* pos1, const mjtNum* pos2);

// multiply vector by 3D rotation matrix
BASE_DECLSPEC void ngu_rotVecMat(mjtNum* res, const mjtNum* vec, const mjtNum* mat);

// multiply vector by transposed 3D rotation matrix
BASE_DECLSPEC void ngu_rotVecMatT(mjtNum* res, const mjtNum* vec, const mjtNum* mat);


//------------------------------ general vector operations ---------------------------

// set vector to zero
BASE_DECLSPEC void ngu_zero(mjtNum* res, int n);

// set matrix to diagonal
BASE_DECLSPEC void ngu_diag(mjtNum* mat, mjtNum value, int n);

// copy vector
BASE_DECLSPEC void ngu_copy(mjtNum* res, const mjtNum* data, int n);

// scale vector
BASE_DECLSPEC void ngu_scl(mjtNum* res, const mjtNum* vec, mjtNum scl, int n);

// add vectors
BASE_DECLSPEC void ngu_add(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, int n);

// subtract vectors
BASE_DECLSPEC void ngu_sub(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, int n);

// add to vector
BASE_DECLSPEC void ngu_addTo(mjtNum* res, const mjtNum* vec, int n);

// add scaled to vector
BASE_DECLSPEC void ngu_addToScl(mjtNum* res, const mjtNum* vec, mjtNum scl, int n);

// normalize vector, return length before normalization
BASE_DECLSPEC mjtNum ngu_normalize(mjtNum* res, int n);

// compute vector length (without normalizing)
BASE_DECLSPEC mjtNum ngu_norm(const mjtNum* res, int n);

// vector dot-product
BASE_DECLSPEC mjtNum ngu_dot(const mjtNum* vec1, const mjtNum* vec2, const int n);


//------------------------------ matrix-matrix operations ----------------------------

// transpose matrix
BASE_DECLSPEC void ngu_transpose(mjtNum* res, const mjtNum* mat, int r, int c);


// multiply matrices
BASE_DECLSPEC void ngu_mulMatMat(mjtNum* res, const mjtNum* mat1, const mjtNum* mat2,
				   int r1, int c1, int c2);

//------------------------------ quaternion operations -----------------------------

// rotate vector by quaternion
BASE_DECLSPEC void ngu_rotVecQuat(mjtNum* res, const mjtNum* vec, const mjtNum* quat);

// convert quaternion to 3D rotation matrix
BASE_DECLSPEC void ngu_quat2Mat(mjtNum* res, const mjtNum* quat);

// convert 3D rotation matrix to quaterion
BASE_DECLSPEC void ngu_mat2Quat(mjtNum* quat, const mjtNum* mat);

BASE_DECLSPEC void ngu_negQuat(mjtNum* res, const mjtNum* quat);

BASE_DECLSPEC void ngu_quat2Vel(mjtNum* res, const mjtNum* quat, mjtNum dt);

BASE_DECLSPEC void ngu_axisAngle2Quat(mjtNum* res, const mjtNum* axis, mjtNum angle);

BASE_DECLSPEC void ngu_mulQuat(mjtNum* res, const mjtNum* qa, const mjtNum* qb);

//------------------------------ spatial algebra --------------------------------

BASE_DECLSPEC void ngu_local2Global(const mjtNum* xpos, const mjtNum* xmat, const mjtNum* xquat,
					 const mjtNum* lpos, const mjtNum* lquat, mjtNum* gpos, mjtNum* gmat);

// vector cross-product, 3D
BASE_DECLSPEC void ngu_cross(mjtNum* res, const mjtNum* a, const mjtNum* b);

// transform 6D motion or force vector between frames
//  rot is 3-by-3 matrix; flg_force determines vector type (motion or force)
BASE_DECLSPEC void ngu_transformSpatial(mjtNum* res, const mjtNum* vec,
						  const mjtNum* newpos, const mjtNum* oldpos);

// make 3D frame given X axis (and possibly Y axis)
BASE_DECLSPEC void ngu_makeFrame(mjtNum* frame);

//------------------------------ miscellaneous --------------------------------

// min function, single evaluation of a and b
BASE_DECLSPEC mjtNum ngu_min(mjtNum a, mjtNum b);

// max function, single evaluation of a and b
BASE_DECLSPEC mjtNum ngu_max(mjtNum a, mjtNum b);

// sign function
BASE_DECLSPEC mjtNum ngu_sign(mjtNum x);

// round to nearest integer
BASE_DECLSPEC int ngu_round(mjtNum x);

// convert type id to type name
const BASE_DECLSPEC char* ngu_type2Str(int type);

// return 1 if nan or abs(x)>mjMAXVAL, 0 otherwise
BASE_DECLSPEC int ngu_isBad(mjtNum x);

// return 1 if all elements are 0
BASE_DECLSPEC int ngu_isZero(mjtNum* vec, int n);

// rescaling:  y = MAX(minval, s0 + s1*x)
BASE_DECLSPEC mjtNum ngu_rescale(const mjtNum* s, mjtNum x, mjtNum minval);

