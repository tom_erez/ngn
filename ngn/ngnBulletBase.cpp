#include "ngnBulletBase.h"


btCollisionShape* ngnBulletBase::bodyGeoms(int bInx)
{
	btTransform gTrans;
	btCollisionShape* geom;
	vector<int> gs;
	for( int gInx=0; gInx<mjm->ngeom; gInx++ )
		if( mjm->geom_bodyid[gInx]==bInx )
			gs.push_back(gInx);
	b2g[bInx] = gs;
	if( gs.empty() )
		return new btEmptyShape();

	btCompoundShape* compound = new btCompoundShape();

	for( int i=0; i<gs.size(); i++ )
	{
		int geomInx = gs[i];
		switch( mjm->geom_type[geomInx] )
		{
		case mjGEOM_BOX:
			geom = new btBoxShape(makebtVector3(mjm->geom_size+3*geomInx));
			break;
		case mjGEOM_SPHERE:
			geom = new btSphereShape(mjm->geom_size[3*geomInx]);
			break;
		case mjGEOM_CAPSULE:
			geom = new btCapsuleShapeZ(	mjm->geom_size[3*geomInx],
										2*mjm->geom_size[3*geomInx+1]);
			break;
		default:
			throw ngnError("only box, sphere and capsule supported for now");
		}
		geom->setUserIndex(geomInx);
		// build geom transform from body:
		gTrans.setOrigin(makebtVector3(mjm->geom_pos+3*geomInx));
		gTrans.setRotation(mjQuat2btQuat(mjm->geom_quat+4*geomInx));
		compound->addChildShape(gTrans, geom);
	}
	compound->setUserIndex(bInx);
	return compound;
}

ngnBulletBase::ngnBulletBase(mjModel* _mjm, ngnParams* params)
	:ngn(_mjm, params)
{
	time = 0;
	b2g.resize(mjm->nbody);

	collisionConfiguration = new btDefaultCollisionConfiguration();

	///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	dispatcher = new	btCollisionDispatcher(collisionConfiguration);

	///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	overlappingPairCache = new btDbvtBroadphase();
	// grab parameters from custom fields:
	for( int i=0; i<mjm->nnumeric; i++ )
	{
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"CFM") )
			m_globalCfm = mjm->numeric_data[mjm->numeric_adr[i]];
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"ERP") )
			m_erp = mjm->numeric_data[mjm->numeric_adr[i]];
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"numiter") )
			m_numIterations = mjm->numeric_data[mjm->numeric_adr[i]];
	}
}

ngnBulletBase::~ngnBulletBase()
{
	delete overlappingPairCache;
	delete dispatcher;
	delete collisionConfiguration;
}


double ngnBulletBase::getTime()
{
	return time;
}
