//#include "engine/engine.h"
//#pragma once
#include "mujoco.h"
#include "ngn.h"

ngn::ngn(mjModel* _mjm, ngnParams *_params)
	:mjm(_mjm),
	params(_params)
{
	timestep = mjm->opt.timestep;
	mjd = mj_makeData(mjm);

	if( params->keyframe>=0 )
	{
		int nq = mjm->nq;
		int nv = mjm->nv;
		ngu_copy(mjd->qpos, mjm->key_qpos+params->keyframe*nq, nq);
		ngu_copy(mjd->qvel, mjm->key_qvel+params->keyframe*nv, nv);
	}

	mj_forward(mjm, mjd);

	// maybe zero out the stack size to save space?
	mjd0 = mj_makeData(mjm);
	mj_forward(mjm, mjd0);

	torques = (mjtNum*)malloc(mjm->nv*sizeof(mjtNum));
	ngu_zero(torques, mjm->nv);
}

ngn::~ngn()
{
	clearTrajVector();
	mj_deleteData(mjd);
	mj_deleteData(mjd0);
	free(torques);
}
void ngn::clearTrajVector()
{
	for( int i=0; i<trajs.size(); i++ )
		delete trajs[i];
	trajs.clear();
}


void ngn::updateTorques()
{
	int act = 0;
	double amplitude, frequency, phase;
	if( torques )
	{
		for( int i=0; i<mjm->nv; i++ )
			torques[i]=0;

		if ( params->nAct )
			for( int i=0; i<mjm->nv; i++ )
			{
				amplitude=params->actuator[3*act];
				frequency=params->actuator[3*act+1];
				phase=params->actuator[3*act+2];
				torques[i]=amplitude*cos(6.2832*getTime()*frequency+phase*i);
				act = ++act % params->nAct;
			}
	}
}

ngnTraj* ngn::integrate(ngnState *initState, double duration)
{
//	if( initState )
//		setState(initState);
	int N = (int)(duration/mjm->opt.timestep)+1;
	bool divergence = false;
	ngnTraj* traj = new ngnTraj(mjm->nbody, mjm->ngeom, N);
	trajs.push_back(traj);
	fillState(&(traj->states[0]));
	traj->states[0].CPUtime = 0;
	

	for( int i=1; i<N; i++ )
	{
		int musec = mjGetTimeHR();
		try
		{
			updateTorques();
			step();
		}
		catch( void* e )
		{
			throw ngnError("oy vey");
		}
		musec = mjGetTimeHR() - musec;
		fillState(&(traj->states[i]));
		traj->states[i].CPUtime = musec*1e-6;

		for( int b=0; b<mjm->nbody; b++ )
			if( ngu_norm3(traj->states[i].bstates[b].xvel) > 1e5 )
				divergence = true;

		if( divergence )
		{
			cout << "velocity has exceeded 1e5, " << name.c_str() << " aborting." << endl;
			break;
		}
	}
	return traj;
}

ngnTraj::ngnTraj(int _nbody, int _ngeom, int N) :
	states(N, ngnState(_nbody,_ngeom)),
	nbody(_nbody),
	ngeom(_ngeom)
{
}

ngnTraj::~ngnTraj()
{
}


ngnState::ngnState(int _nbody, int _ngeom) :
	nbody(_nbody),
	ngeom(_ngeom)
{
	bstates = new ngnPVState[nbody];
	gstates = new ngnPState[ngeom];
}

ngnState::ngnState(const ngnState& other)
{
	nbody=other.nbody;
	ngeom=other.ngeom;
	penetration = sqrt(-1.0);
	bstates = new ngnPVState[other.nbody];
	gstates = new ngnPState[other.ngeom];
}

ngnState::~ngnState()
{
	delete[] gstates;
	delete[] bstates;
}
