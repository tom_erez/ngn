#include "ngnPhysxRigid.h"
#include <string>
using namespace physx;
using namespace std;


ngnPhysxRigid::ngnPhysxRigid(mjModel* _mjm, mjOption* _mjo, ngnParams* params)
	:ngnPhysxBase(_mjm, _mjo, params)
{
	name = "PhysX";

	try
	{
		addChildren(0, 0);
	}
	catch( ngnError e )
	{
		cout << e.errorMsg.c_str() << endl;
	}
}



void ngnPhysxRigid::addChildren(int parentInx, PxRigidDynamic* parent)
{
	vector<int> bg2g;
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		bg2g.clear();
		if( mjm->body_parentid[bInx]==parentInx )
		{

		// add new body
			PxRigidDynamic* child = gPhysics->createRigidDynamic(getTransform(bInx));
			for( int gInx=0; gInx<mjm->ngeom; gInx++ )
				if( mjm->geom_bodyid[gInx]==bInx )
				{
					attachGeom(child, gInx);
					bg2g.push_back(gInx);
				}
			b2g[bInx]=bg2g;
			child->setCMassLocalPose(getTransform(0, mjm->body_ipos+3*bInx, 0, mjm->body_iquat+4*bInx));
			child->setMass((PxReal)mjm->body_mass[bInx]);
			child->setMassSpaceInertiaTensor(PxVec3(mjm->body_inertia[bInx*3],
													mjm->body_inertia[bInx*3+1],
													mjm->body_inertia[bInx*3+2]));
			
			child->setAngularDamping(0);
			child->setLinearDamping(0);
			
			gScene->addActor(*child);
			// body vel:
			mjtNum temp[6];
			ngu_transformSpatial(temp, mjd->cvel+6*bInx, mjd->xipos+3*bInx, mjd->com_subtree+3*mjm->body_rootid[bInx]);
			child->setAngularVelocity(PxVec3(temp[0], temp[1], temp[2]));
			child->setLinearVelocity(PxVec3(temp[3], temp[4], temp[5]));
			child->userData=(void*)bInx;

			int ndof=0, inx[6];
			for( int j=0; j<mjm->njnt; j++ )
				if( mjm->jnt_bodyid[j]==bInx )
				{
					inx[ndof]=j;
					ndof++;
				}

			if( ndof>1 )
				throw ngnError("we decided to support only 1 joint between parent and child");
			if( !ndof )
			{
				inx[0]=0;
				throw ngnError("add welds to physx!");
				// TODO: weld parent and child
			}
			else if( mjm->jnt_type[inx[0]] == mjJNT_FREE )
			{
				// do nothing
			}
			else // add 6DOF joint
			{
				int j = inx[0];
				if( mjm->jnt_type[inx[0]] != mjJNT_HINGE )
					throw ngnError("only hinge joints are supported");
				mjtNum tempmat[9], tempmatT[9];
				ngu_zero(tempmat, 9);
				ngu_copy3(tempmat,		mjd->xaxis+3*j);
				ngu_makeFrame(tempmat);
				ngu_transpose(tempmatT, tempmat, 3, 3);
				PxTransform globalJoint = PxTransform(	(PxReal)mjd->xanchor[j*3],
														(PxReal)mjd->xanchor[j*3+1],
														(PxReal)mjd->xanchor[j*3+2],
												mjMat2PxQuat(tempmatT));
				PxTransform global2child = getTransform(bInx);
				PxTransform child2joint = global2child.transformInv(globalJoint);
				PxTransform global2parent = getTransform(parentInx);
				PxTransform parent2joint = global2parent.transformInv(globalJoint);
				// PxRevoluteJoint* aa;
				// use D6 joint, because it has a drive with stiffness
				PxD6Joint* d6j;
				d6j=PxD6JointCreate(*gPhysics,
								child, child2joint,
								parent, parent2joint);
					
				// disable all DOFs (twist will be handled later):
				d6j->setMotion(PxD6Axis::eX, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eY, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eZ, PxD6Motion::eLOCKED);
				//d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eSWING1, PxD6Motion::eLOCKED);
				d6j->setMotion(PxD6Axis::eSWING2, PxD6Motion::eLOCKED);
				d6j->setProjectionLinearTolerance(jointLinearTolerance);
				d6j->setProjectionAngularTolerance(jointAngularTolerance);
				//d6j->setConstraintFlag(PxConstraintFlag::ePROJECTION, true);
					
				// limits:
				if( mjm->jnt_islimited[inx[0]] )
				{
					d6j->setTwistLimit(PxJointAngularLimitPair(	(PxReal)-mjm->jnt_range[j*2+1],
																(PxReal)-mjm->jnt_range[j*2],
																(PxReal)mjm->jnt_mindist[j]));
					d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eLIMITED);
						
				}
				else
					d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eFREE);
					
				if( !(mjo->disableflags & (mjDSBL_PASSIVE)) )
				{
					double stiffness = ngu_rescale(mjo->s_stiffness, mjm->jnt_stiffness[j], 0);
					double damping = ngu_rescale(mjo->s_damping, mjm->dof_damping[mjm->jnt_dofadr[j]], 0);
					PxD6JointDrive drive(stiffness, damping ,1e6, false); // max force is hacked; last input indicates this is a force-based, rather than acceleration-based spring
					d6j->setDrive(PxD6Drive::eTWIST, drive);
					double ref = mjm->qpos_spring[mjm->jnt_qposadr[j]] - mjd->qpos[mjm->jnt_qposadr[j]];
					d6j->setDrivePosition(PxTransform(PxQuat(sin(-ref/2), 0, 0, cos(-ref/2))));
				}
				pxJointPair jp;
				jp.joint = d6j;
				jp.parent = parent;
				jp.child = child;
				jp.axisLocalChild = pxMakeVector3(mjm->jnt_axis+3*j);
				jp.axisLocalChild.normalize();
				if( parent )
					jp.axisLocalParent = parent->getGlobalPose().rotateInv(pxMakeVector3(mjd->xaxis+3*j));
				else
					jp.axisLocalParent = pxMakeVector3(mjd->xaxis+3*j);
				jp.axisLocalParent.normalize();
				jp.dofAdr = mjm->jnt_dofadr[j];
				jp.jntAdr = -1;
				// not setting ref because springs in cartesian are handled with built-in functionality
				jointPairs[j] = jp;
			}

	//recurse:
			addChildren(bInx, child);
		}
	}
}



void ngnPhysxRigid::fillState(ngnState* state)
{
	state->time = getTime();
	PxActor** buffer = new PxActor*[mjm->nbody-1];
	int nb = gScene->getActors(PxActorTypeFlag::eRIGID_DYNAMIC, buffer, mjm->nbody-1);
	for( int b=1; b<mjm->nbody; b++ )
	{
		PxRigidDynamic* body = ((PxRigidDynamic*)buffer[b-1]);
		int bInx = (int)body->userData;
		ngnPVState* bState = &state->bstates[bInx];
		PxVec3 inertia = body->getMassSpaceInertiaTensor();
		PxTransform xbody = body->getGlobalPose();
		// save bstate:
		transform2PState(xbody, bState);
		PxVec3 avel = body->getAngularVelocity();
		PxVec3 xvel = body->getLinearVelocity();
		for( int i=0; i<3; i++ )
		{
			bState->avel[i]=avel[i];
			bState->xvel[i]=xvel[i];
		}
		pxQuat2mjQuat(xbody.q, mjd->xquat);
		ngu_copy3(mjd->xpos, bState->pos);
		ngu_copy(mjd->xmat, bState->mat, 9);
		// xquat was written above
		for( int g=0; g<b2g[bInx].size(); g++ )
		{
			int gInx = b2g[bInx][g];
			ngnPState* gState = &state->gstates[gInx];
			ngu_local2Global(mjd->xpos, mjd->xmat, mjd->xquat, mjm->geom_pos+3*gInx, mjm->geom_quat+4*gInx, gState->pos, gState->mat);
		}

	}
}
