#include "ngnBulletMB.h"
#include "mujoco.h"
ngnBulletMB::ngnBulletMB(mjModel* _mjm, ngnParams* _params)
	:ngnBulletBase(_mjm, _params)
{
	name = "BulletMB";

	solver = new btMultiBodyConstraintSolver;
	
	dynamicsWorld = new btMultiBodyDynamicsWorld(dispatcher,overlappingPairCache,solver,collisionConfiguration);

	dynamicsWorld->setGravity(makebtVector3(mjm->opt.gravity));

	btContactSolverInfo &solverInfo = dynamicsWorld->getSolverInfo();
	solverInfo.m_globalCfm = m_globalCfm;
	solverInfo.m_erp = m_erp;
	solverInfo.m_numIterations = m_numIterations;
	solverInfo.m_maxGyroscopicForce = 100000;

	btStaticPlaneShape* groundGeom = new btStaticPlaneShape( btVector3(0,0,1),0);

	btDefaultMotionState* myMotionState = new btDefaultMotionState(btTransform(btQuaternion(0,0,0,1),btVector3(0,0,0)));
	btRigidBody::btRigidBodyConstructionInfo rbInfo(0, myMotionState, groundGeom, btVector3(0,0,0));
	rbInfo.m_friction = mjm->geom_friction[0];
	btRigidBody* body = new btRigidBody(rbInfo);
	dynamicsWorld->addRigidBody(body);
	j2t.resize(mjm->njnt);
	j2l.resize(mjm->njnt);
	btVector3 axis_in_child_frame;
	vector<int> l2j; // from tree's link to joint, to be inserted as an item of t2j

	// children of the world get their own tree:
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		if( mjm->body_parentid[bInx]==0 )
		{
			int numLinks = -1; // not including the base
			for(int b=1; b<mjm->nbody; b++ )
				if( mjm->body_rootid[b] == bInx || mjm->body_rootid[b] == 0 ) // the second case refers to hinge with world
					numLinks++;

			l2j.clear();
			btMultiBody * tree;
			int j = mjm->body_jntadr[bInx];
			int linkIndex;
			if( mjm->jnt_type[j]== mjJNT_FREE )
			{
				l2j.resize(numLinks+1); // including the base
				tree = new btMultiBody(numLinks, mjm->body_mass[bInx], makebtVector3(mjm->body_inertia+bInx*3), false, false);
				tree->setCanSleep(false);  // should be an input parameter
				tree->setLinearDamping(0);
				tree->setAngularDamping(0);
				tree->setUseGyroTerm(true);
				dynamicsWorld->addMultiBody(tree);

				tree->setBasePos(makebtVector3(mjd->qpos+mjm->jnt_qposadr[j]));
				mjtNum negQuat[4];
				ngu_negQuat(negQuat, mjd->qpos+mjm->jnt_qposadr[j]+3);
				btQuaternion btNegQuat = mjQuat2btQuat(negQuat);
				tree->setWorldToBaseRot(btNegQuat);
				tree->setBaseVel(makebtVector3(mjd->qvel+mjm->jnt_dofadr[j]));
				btVector3 angvel = makebtVector3(mjd->qvel+mjm->jnt_dofadr[j]+3);
				btVector3 rangvel = quatRotate(mjQuat2btQuat(mjd->qpos+mjm->jnt_qposadr[j]+3), angvel);
				tree->setBaseOmega(rangvel);
				btMultiBodyLinkCollider* c = new btMultiBodyLinkCollider(tree, -1);
				c->setCollisionShape(bodyGeoms(bInx));
				//btTransform vv(mjQuat2btQuat(negquat), makebtVector3(mjd->qpos+mjm->jnt_qposadr[j]));
				//c->setWorldTransform(vv);//
				c->setWorldTransform(getTransform(mjd->xpos+bInx*3,mjd->xmat+bInx*9));
				c->setFriction(mjm->geom_friction[0]);
				c->setRollingFriction(mjm->geom_friction[2]);
				c->setRestitution(0);
				dynamicsWorld->addCollisionObject(c, btBroadphaseProxy::DefaultFilter, btBroadphaseProxy::AllFilter);
				tree->setBaseCollider(c);
				
				linkIndex=-1;
				l2j[linkIndex+1]=j;
			}
			else if( mjm->jnt_type[j]== mjJNT_HINGE )
			{
				numLinks++; // adding a dummy base
				l2j.resize(numLinks+1);
				tree = new btMultiBody(numLinks, 1, btVector3(1,1,1), true, false, false); // adding a dummy body as fixed base
				dynamicsWorld->addMultiBody(tree);

				tree->setBasePos(0*makebtVector3(mjd0->xpos));
				tree->setWorldToBaseRot(btQuaternion::getIdentity());

				linkIndex=0;
				btMultiBodyLinkCollider* c = new btMultiBodyLinkCollider(tree, linkIndex);
				c->setCollisionShape(bodyGeoms(bInx));
				c->setWorldTransform(getTransform(mjd0->xpos+bInx*3,mjd0->xmat+bInx*9));
				c->setFriction(mjm->geom_friction[0]);
				c->setRollingFriction(mjm->geom_friction[2]);
				((btMultiBodyDynamicsWorld*)dynamicsWorld)->addCollisionObject(c, btBroadphaseProxy::DefaultFilter, btBroadphaseProxy::AllFilter);
				tree->getLink(linkIndex).m_collider = c;
				
				mjtNum tempQuat[4];
				ngu_mat2Quat(tempQuat, mjd0->xmat+9*bInx);
				ngu_negQuat(tempQuat, tempQuat);
				tree->setupRevolute(linkIndex, mjm->body_mass[bInx], makebtVector3(mjm->body_inertia+bInx*3), -1,
									mjQuat2btQuat(tempQuat),
									makebtVector3(mjm->jnt_axis+3*j), //makebtVector3(mjd0->xaxis+3*j),
									makebtVector3(mjd0->xanchor+3*j),
									-makebtVector3(mjm->jnt_pos+3*j),
									true);
				tree->setJointPos(linkIndex, mjd->qpos[mjm->jnt_qposadr[j]]);
				tree->setJointVel(linkIndex, mjd->qvel[mjm->jnt_dofadr[j]]);
				if (mjm->jnt_limited[j])
				{
					btMultiBodyConstraint* con = new btMultiBodyJointLimitConstraint(tree,0,mjm->jnt_range[2*j],mjm->jnt_range[2*j+1]);
					((btMultiBodyDynamicsWorld*)dynamicsWorld)->addMultiBodyConstraint(con);
				}
				l2j[0]=-1;
				l2j[linkIndex+1]=j;
			}
			j2t[j]=t2j.size();
			j2l[j]=linkIndex;
			tree->setCanSleep(false);  // should be an input parameter + already specified in constructor
			tree->setLinearDamping(0);
			tree->setAngularDamping(0);
			tree->setUseGyroTerm(true);
			tree->setHasSelfCollision(true);
			trees.push_back(tree);
			try
			{
				int totalNumLinks = addChildren(tree, l2j, linkIndex, bInx);
				if( totalNumLinks != numLinks-1 )
					throw new ngnError("num links mismatch");
			}
			catch( ngnError e )
			{
				cout << e.errorMsg.c_str() << endl;
			}
			t2j.push_back(l2j);
		}
	}
}

int ngnBulletMB::addChildren(btMultiBody* tree, vector<int> &l2j, int parentLinkIndex, int parentInx)
{
	int linkIndex=parentLinkIndex;
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		if( mjm->body_parentid[bInx]==parentInx )
		{
			linkIndex++;
			btMultiBodyLinkCollider* c = new btMultiBodyLinkCollider(tree, linkIndex);
			c->setCollisionShape(bodyGeoms(bInx));
			c->setWorldTransform(getTransform(mjd0->xpos+bInx*3,mjd0->xmat+bInx*9));
			dynamicsWorld->addCollisionObject(c, btBroadphaseProxy::DefaultFilter, btBroadphaseProxy::AllFilter);
			c->setFriction(mjm->geom_friction[0]);
			c->setRollingFriction(mjm->geom_friction[2]);
			tree->getLink(linkIndex).m_collider = c;
				//c->setAnisotropicFriction
				//c->setContactProcessingThreshold
				//c->setRestitution
			mjtNum tempQuat[4];
			btQuaternion child2parent = mjQuat2btQuat(mjm->body_quat+4*bInx);
			ngu_negQuat(tempQuat, mjm->body_quat+4*bInx);
			btQuaternion parent2child = mjQuat2btQuat(tempQuat);
			int j = mjm->body_jntadr[bInx];
			tree->setupRevolute(linkIndex, mjm->body_mass[bInx], makebtVector3(mjm->body_inertia+bInx*3), parentLinkIndex,
								parent2child, //const btQuaternion &zero_rot_parent_to_this,  // rotate points in parent frame to this frame, when q = 0
								makebtVector3(mjm->jnt_axis+3*j), //const btVector3 &joint_axis,    // in my frame
								quatRotate(child2parent, makebtVector3(mjm->jnt_pos+3*j))+makebtVector3(mjm->body_pos+3*bInx), //const btVector3 &parent_axis_position,    // vector from parent COM to joint axis, in PARENT frame
								-makebtVector3(mjm->jnt_pos+3*j), //const btVector3 &my_axis_position,       // vector from joint axis to my COM, in MY frame
								true);
			tree->setJointPos(linkIndex, mjd->qpos[mjm->jnt_qposadr[j]]);
			tree->setJointVel(linkIndex, mjd->qvel[mjm->jnt_dofadr[j]]);
			if (mjm->jnt_limited[j])
			{
				btMultiBodyConstraint* con = new btMultiBodyJointLimitConstraint(tree, linkIndex, mjm->jnt_range[2*j],mjm->jnt_range[2*j+1]);
				((btMultiBodyDynamicsWorld*)dynamicsWorld)->addMultiBodyConstraint(con);
			}
			l2j[linkIndex+1]=j;
			j2t[j]=t2j.size();
			j2l[j]=linkIndex;
			linkIndex = addChildren(tree, l2j, linkIndex, bInx);
		}
	}
	return linkIndex;
}
ngnBulletMB::~ngnBulletMB()
{
	for (int i=dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
			btCollisionShape* shape = body->getCollisionShape();
			if( shape )
				{
				if( shape->isCompound() )
				{
					for( int s=0; s<((btCompoundShape*)shape)->getNumChildShapes(); s++ )
						delete ((btCompoundShape*)shape)->getChildShape(s);
				}
				delete shape;
			}
		}
		else
		{
			btMultiBodyLinkCollider* lc = btMultiBodyLinkCollider::upcast(obj);
			if( lc )
				1;
		}
		dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	delete dynamicsWorld;
	delete solver;

}


void ngnBulletMB::step()
{


	for( int i=0; i<mjm->nv; i++ )
	{
		int j = mjm->dof_jntid[i];
		btMultiBody* tree = trees[j2t[j]];
		if( mjm->jnt_type[j]==mjJNT_HINGE )
		{
			if( !(mjm->opt.disableflags & (mjDSBL_PASSIVE)) )
			{
				double angle = tree->getJointPos(j2l[j]);
				double offset = angle - mjm->qpos_spring[j];
				double stiffness = mjm->jnt_stiffness[j];
				double springTorque = -offset*stiffness;

				double vel = tree->getJointVel(j2l[j]);
				double damping = mjm->dof_damping[mjm->jnt_dofadr[j]];
				double dampTorque = -vel*damping;
				tree->addJointTorque(j2l[j], springTorque + dampTorque);
			}
			tree->addJointTorque(j2l[j], torques[i]);
		}
	}
	dynamicsWorld->stepSimulation(timestep, 1, timestep);
	time += timestep;
}



void ngnBulletMB::fillState(ngnState* state)
{
	state->time = getTime();

	for( int t=0; t<trees.size(); t++ )
	{
		// first, the base:
		int j = t2j[t][0];
		int q, d;
		mjtNum pos[3];
		if( j>=0 ) // this is not a fake base for a world hinge
		{
			q = mjm->jnt_qposadr[j];
			d = mjm->jnt_dofadr[j];

			btVector3 pPos = trees[t]->getBasePos();
			btVector3 xvel = trees[t]->getBaseVel();
			btVector3 avel = trees[t]->getBaseOmega();
			btQuaternion quat = trees[t]->getWorldToBaseRot();
			copyQuat(&quat, mjd->qpos+q+3);
			btVector3 ravel = quatRotate(mjQuat2btQuat(mjd->qpos+q+3), avel);
			ngu_negQuat( mjd->qpos+q+3, mjd->qpos+q+3);
			for( int i=0; i<3; i++ )
				mjd->qpos[q+i] = (mjtNum)pPos[i];
			for( int i=0; i<3; i++ )
				mjd->qvel[d+i] = (mjtNum)xvel[i];
			for( int i=0; i<3; i++ )
				mjd->qvel[d+i+3] = (mjtNum)ravel[i];
		}

		for( int k=1; k<t2j[t].size(); k++ )
		{
			j = t2j[t][k];
			d = mjm->body_dofadr[mjm->jnt_bodyid[j]];
			q = mjm->jnt_qposadr[j];
			mjd->qpos[q] = trees[t]->getJointPos(k-1); // excluding the base
			mjd->qvel[d] = trees[t]->getJointVel(k-1); // excluding the base
		}
	}
	
	if( (int)(time*1000) % 100 < 0.001 )
	 	cout << mjd->qpos[0] << ' '<< mjd->qpos[1] <<' '<< mjd->qpos[2] << endl;
	mj_forward(mjm, mjd);
	for( int g=0; g<mjm->ngeom; g++ )
	{
		ngnPState* gState = &state->gstates[g];
		ngu_copy3(gState->pos, mjd->geom_xpos+g*3);
		ngu_copy(gState->mat, mjd->geom_xmat+g*9, 9);
	}
	for( int b=1; b<mjm->nbody; b++ )
	{
		ngnPVState* bState = &state->bstates[b];
		ngu_copy3(bState->pos, mjd->xpos+b*3);
		ngu_copy(bState->mat, mjd->xmat+b*9, 9);
		mjtNum temp[6];
		ngu_transformSpatial(temp, mjd->cvel+6*b, mjd->xipos+3*b, mjd->com_subtree+3*mjm->body_rootid[b]);
		ngu_copy3(bState->avel, temp);
		ngu_copy3(bState->xvel, temp+3);
	}
}
