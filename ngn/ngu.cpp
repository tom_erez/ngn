/*******************************************
This file is part of the MuJoCo software.
(C) 2013 Emo Todorov. All rights reserved.
*******************************************/
#include "ngn.h"

//------------------------------ 3D vector and matrix-vector operations --------------

// set vector to zero
void ngu_zero3(mjtNum* res)
{
	res[0] = res[1] = res[2] = 0.0;
}



// copy vector
void ngu_copy3(mjtNum* res, const mjtNum* data)
{
	res[0] = data[0];
	res[1] = data[1];
	res[2] = data[2];
}



// scale vector
void ngu_scl3(mjtNum* res, const mjtNum* vec, mjtNum scl)
{
	res[0] = vec[0] * scl;
	res[1] = vec[1] * scl;
	res[2] = vec[2] * scl;
}



// add vectors
void ngu_add3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2)
{
	res[0] = vec1[0] + vec2[0];
	res[1] = vec1[1] + vec2[1];
	res[2] = vec1[2] + vec2[2];
}



// subtract vectors
void ngu_sub3(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2)
{
	res[0] = vec1[0] - vec2[0];
	res[1] = vec1[1] - vec2[1];
	res[2] = vec1[2] - vec2[2];
}



// add to vector
void ngu_addTo3(mjtNum* res, const mjtNum* vec)
{
	res[0] += vec[0];
	res[1] += vec[1];
	res[2] += vec[2];
}



// add scaled to vector
void ngu_addToScl3(mjtNum* res, const mjtNum* vec, mjtNum scl)
{
	res[0] += vec[0] * scl;
	res[1] += vec[1] * scl;
	res[2] += vec[2] * scl;
}



// normalize vector, return length before normalization
mjtNum ngu_normalize3(mjtNum* vec)
{
	mjtNum norm = sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
	mjtNum normInv;

	if( norm < mjMINVAL )
	{
		vec[0] = 1.0;
		vec[1] = vec[2] = 0.0;
	}
	else
	{
		normInv = 1.0/norm;
		vec[0] *= normInv;
		vec[1] *= normInv;
		vec[2] *= normInv;
	}

	return norm;
}



// compute vector length (without normalizing)
mjtNum ngu_norm3(const mjtNum* vec)
{
	return sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
}



// vector dot-product
mjtNum ngu_dot3(const mjtNum* vec1, const mjtNum* vec2)
{
	return vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2];
}



// Cartesian distance between 3D vectors
mjtNum ngu_dist3(const mjtNum* pos1, const mjtNum* pos2)
{
	mjtNum dif[3] = {pos1[0]-pos2[0], pos1[1]-pos2[1], pos1[2]-pos2[2]};
	return sqrt(dif[0]*dif[0] + dif[1]*dif[1] + dif[2]*dif[2]);
}



// multiply vector by 3D rotation matrix
void ngu_rotVecMat(mjtNum* res, const mjtNum* vec, const mjtNum* mat)
{
	res[0] = mat[0]*vec[0] + mat[1]*vec[1] + mat[2]*vec[2];
	res[1] = mat[3]*vec[0] + mat[4]*vec[1] + mat[5]*vec[2];
	res[2] = mat[6]*vec[0] + mat[7]*vec[1] + mat[8]*vec[2];
}



// multiply vector by transposed 3D rotation matrix
void ngu_rotVecMatT(mjtNum* res, const mjtNum* vec, const mjtNum* mat)
{
	res[0] = mat[0]*vec[0] + mat[3]*vec[1] + mat[6]*vec[2];
	res[1] = mat[1]*vec[0] + mat[4]*vec[1] + mat[7]*vec[2];
	res[2] = mat[2]*vec[0] + mat[5]*vec[1] + mat[8]*vec[2];
}



//------------------------------ vector operations ---------------------------------

// set vector to zero
void ngu_zero(mjtNum* res, int n)
{
	memset(res, 0, n*sizeof(mjtNum));
}



// set matrix to diagonal
void ngu_diag(mjtNum* mat, mjtNum value, int n)
{
	int i;
	ngu_zero(mat, n*n);
	for( i=0; i<n; i++ )
		mat[i*(n+1)] = value;
}



// copy vector
void ngu_copy(mjtNum* res, const mjtNum* vec, int n)
{
	memcpy(res, vec, n*sizeof(mjtNum));
}



// scale vector
void ngu_scl(mjtNum* res, const mjtNum* vec, mjtNum scl, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] = vec[i] * scl;
}



// add vectors
void ngu_add(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] = vec1[i] + vec2[i];
}



// subtract vectors
void ngu_sub(mjtNum* res, const mjtNum* vec1, const mjtNum* vec2, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] = vec1[i] - vec2[i];
}



// add to vector
void ngu_addTo(mjtNum* res, const mjtNum* vec, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] += vec[i];
}



// add scaled to vector... FMA ???
void ngu_addToScl(mjtNum* res, const mjtNum* vec, mjtNum scl, int n)
{
	int i = 0;

	for( i=0; i<n; i++ )
		res[i] += vec[i]*scl;
}



// normalize vector, return length before normalization
mjtNum ngu_normalize(mjtNum* res, int n)
{
	int i;
	mjtNum norm = (mjtNum)sqrt(ngu_dot(res, res, n));
	mjtNum normInv;

	if( norm < mjMINVAL )
	{
		res[0] = 1;
		for( i=1; i<n; i++ )
			res[i] = 0;
	}
	else
	{
		normInv = 1.0/norm;
		for( i=0; i<n; i++ )
			res[i] *= normInv;
	}

	return norm;
}



// compute vector length (without normalizing)
mjtNum ngu_norm(const mjtNum* res, int n)
{
	return sqrt(ngu_dot(res, res, n));
}



// vector dot-product... FMA ???
mjtNum ngu_dot(const mjtNum* vec1, const mjtNum* vec2, const int n)
{
	int i = 0;
	mjtNum res = 0;

	for( i=0; i<n; i++ )
		res += vec1[i] * vec2[i];

	return res;
}


//------------------------------ matrix-matrix operations ----------------------------

// transpose matrix
void ngu_transpose(mjtNum* res, const mjtNum* mat, int r, int c)
{
	int i, j;

	for( i=0; i<r; i++ )
		for( j=0; j<c; j++ )
			res[j*r+i] = mat[i*c+j];
}


// multiply matrices, exploit sparsity of mat1
void ngu_mulMatMat(mjtNum* res, const mjtNum* mat1, const mjtNum* mat2,
				   int r1, int c1, int c2)
{
   int i, k;
   mjtNum tmp;

   ngu_zero(res, r1*c2);

   for( i=0; i<r1; i++ )
	  for( k=0; k<c1; k++ )
		 if( (tmp = mat1[i*c1+k]) )
			ngu_addToScl(res+i*c2, mat2+k*c2, tmp, c2);
}


//------------------------------ quaternion operations -----------------------------

// rotate vector by quaternion
void ngu_rotVecQuat(mjtNum* res, const mjtNum* vec, const mjtNum* quat)
{
	mjtNum mat[9];
	ngu_quat2Mat(mat, quat);
	ngu_rotVecMat(res, vec, mat);
}



// muiltiply quaternions
void ngu_mulQuat(mjtNum* res, const mjtNum* qa, const mjtNum* qb)
{
	res[0] = qa[0]*qb[0] - qa[1]*qb[1] - qa[2]*qb[2] - qa[3]*qb[3];
	res[1] = qa[0]*qb[1] + qa[1]*qb[0] + qa[2]*qb[3] - qa[3]*qb[2];
	res[2] = qa[0]*qb[2] - qa[1]*qb[3] + qa[2]*qb[0] + qa[3]*qb[1];
	res[3] = qa[0]*qb[3] + qa[1]*qb[2] - qa[2]*qb[1] + qa[3]*qb[0];
}



// negate quaternion
void ngu_negQuat(mjtNum* res, const mjtNum* quat)
{
	res[0] = quat[0];
	res[1] = -quat[1];
	res[2] = -quat[2];
	res[3] = -quat[3];
}


// convert quaternion to 3D rotation matrix
void ngu_quat2Mat(mjtNum* res, const mjtNum* quat)
{
	mjtNum q00 = quat[0]*quat[0];
	mjtNum q11 = quat[1]*quat[1];
	mjtNum q22 = quat[2]*quat[2];
	mjtNum q33 = quat[3]*quat[3];
	res[0] = q00 + q11 - q22 - q33;
	res[4] = q00 - q11 + q22 - q33;
	res[8] = q00 - q11 - q22 + q33;
	res[1] = 2*(quat[1]*quat[2] - quat[0]*quat[3]);
	res[2] = 2*(quat[1]*quat[3] + quat[0]*quat[2]);
	res[3] = 2*(quat[1]*quat[2] + quat[0]*quat[3]);
	res[5] = 2*(quat[2]*quat[3] - quat[0]*quat[1]);
	res[6] = 2*(quat[1]*quat[3] - quat[0]*quat[2]);
	res[7] = 2*(quat[2]*quat[3] + quat[0]*quat[1]);
}



// convert 3D rotation matrix to quaterion
void ngu_mat2Quat(mjtNum* quat, const mjtNum* mat)
{
	mjtNum trace = 1.0 + mat[0] + mat[4] + mat[8];

	if( trace <= 0 )		// SHOULD NOT OCCUR
		trace = mjMINVAL;

	// compute quaternion
	quat[0] = sqrt(trace) / 2.0;
	quat[1] = (mat[7] - mat[5]) / quat[0] / 4.0;
	quat[2] = (mat[2] - mat[6]) / quat[0] / 4.0;
	quat[3] = (mat[3] - mat[1]) / quat[0] / 4.0;
	ngu_normalize(quat, 4);
}
// convert quaternion (corresponding to orientation difference) to 3D velocity
void ngu_quat2Vel(mjtNum* res, const mjtNum* quat, mjtNum dt)
{
	mjtNum axis[3] = {quat[1], quat[2], quat[3]};
	mjtNum sin_a_2 = ngu_normalize3(axis);
	mjtNum speed = 2 * mju_atan2(sin_a_2, quat[0]);

	// when axis-angle is larger than pi, rotation is in the opposite direction
	if( speed>mjPI )
		speed -= 2*mjPI;
	speed /= dt;

	ngu_scl3(res, axis, speed);
}
// convert axisAngle to quaternion
void ngu_axisAngle2Quat(mjtNum* res, const mjtNum* axis, mjtNum angle)
{
	mjtNum s = mju_sin(angle*0.5);
	res[0] = mju_cos(angle*0.5);
	res[1] = axis[0]*s;
	res[2] = axis[1]*s;
	res[3] = axis[2]*s;
}


//------------------------------ spatial algebra --------------------------------

// vector cross-product, 3D
void ngu_cross(mjtNum* res, const mjtNum* a, const mjtNum* b)
{
	res[0] = a[1]*b[2] - a[2]*b[1];
	res[1] = a[2]*b[0] - a[0]*b[2];
	res[2] = a[0]*b[1] - a[1]*b[0];
}


// transform 6D motion or force vector between frames
//  rot is 3-by-3 matrix; flg_force determines vector type (motion or force)
void ngu_transformSpatial(mjtNum* res, const mjtNum* vec,
						  const mjtNum* newpos, const mjtNum* oldpos)
{
	mjtNum cros[3], dif[3], tran[6];

	// apply translation
	ngu_copy(tran, vec, 6);
	ngu_sub3(dif, newpos, oldpos);
	ngu_cross(cros, dif, vec);
	ngu_sub3(tran+3, vec+3, cros);
	ngu_copy(res, tran, 6);
}


void ngu_local2Global(const mjtNum* xpos, const mjtNum* xmat, const mjtNum* xquat,
					 const mjtNum* lpos, const mjtNum* lquat, mjtNum* gpos, mjtNum* gmat)
{
	mjtNum tmp[4];

	ngu_rotVecMat(gpos, lpos, xmat);
	ngu_addTo3(gpos, xpos);  
	ngu_mulQuat(tmp, xquat, lquat);
	ngu_quat2Mat(gmat, tmp);
}


//------------------------------ miscellaneous --------------------------------

// make 3D frame given X axis (normal) and possibly Y axis (tangent 1)
void ngu_makeFrame(mjtNum* frame)
{
	mjtNum tmp[3], yaxis[3] = {0,0,0};

	// if yaxis undefined, set yaxis to (0,1,0) if possible, otherwize (0,0,1)
	if( ngu_norm3(frame+3) < 0.5 )
	{
		ngu_zero3(frame+3);

		if( frame[1]<0.5 && frame[1]>-0.5 )
			frame[4] = 1;
		else
			frame[5] = 1;
	}

	// make yaxis orthogonal to xaxis
	ngu_scl3(tmp, frame, ngu_dot3(frame, frame+3));
	ngu_sub3(frame+3, frame+3, tmp);
	ngu_normalize3(frame+3);

	// zaxis = cross(xaxis, yaxis)
	ngu_cross(frame+6, frame, frame+3);
}


// min function, avoid re-evaluation
mjtNum ngu_min(mjtNum a, mjtNum b)
{
	if( a <= b )
		return a;
	else 
		return b;
}



// max function, avoid re-evaluation
mjtNum ngu_max(mjtNum a, mjtNum b)
{
	if( a >= b )
		return a;
	else 
		return b;
}



// sign function
mjtNum ngu_sign(mjtNum x)
{
	if( x<0 )
		return -1;
	else if( x>0 )
		return 1;
	else return 0;
}



// round to nearest integer
int ngu_round(mjtNum x)
{
	mjtNum lower = floor(x);
	mjtNum upper = ceil(x);

	if( x-lower < upper-x )
		return (int)lower;
	else
		return (int)upper;
}



// return 1 if nan or abs(x)>mjMAXVAL, 0 otherwise
int ngu_isBad(mjtNum x)
{
	return( x!=x || x>mjMAXVAL || x<-mjMAXVAL );
}



// return 1 if all elements are 0
int ngu_isZero(mjtNum* vec, int n)
{
	int i;

	for( i=0; i<n; i++ )
		if( vec[i]!=0 )
			return 0;

	return 1;
}



// rescaling:  y = MAX(minval, s0 + s1*x)
mjtNum ngu_rescale(const mjtNum* s, mjtNum x, mjtNum minval)
{
	return ngu_max(minval, s[0] + s[1] * x);
}
