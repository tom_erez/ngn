#include <ode/ode.h>
#include "ngn.h"

#pragma once
#define DECLSPEC
class ngnODE : public ngn
{
protected:
	dSpaceID sId;
	dGeomID ground;
	double time;
	vector<dBodyID> bodies;
	vector<dJointID> joints;
	vector<jointPair> jointPairs;
	vector<vector<int>> b2g;
	void attachGeom(dBodyID child, int bodyInx, int geomInx);
	double computeBodyEnergy(dBodyID bId);
	void addChildren(int parentInx, dBodyID parentID);
	float cfm;
	float erp;
	int numiter;
public:
	dWorldID wId;
	dJointGroupID contactGroup;
	//ngnODE(mjModel* _mjm, mjOption* _mjo):ngn(_mjm, _mjo){ngnODE(_mjm, _mjo, 0);};
	DECLSPEC ngnODE(mjModel* _mjm, ngnParams* params);
	DECLSPEC double getTime();
	DECLSPEC void step();
	DECLSPEC void fillState(ngnState * s);
	DECLSPEC ~ngnODE();
};

