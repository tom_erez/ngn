#include "ngn.h"
#include "mujoco.h"

#pragma once

#define DECLSPEC

class ngnMujoco : public ngn
{
protected:
public:
	DECLSPEC ngnMujoco(mjModel* _mjm, ngnParams* params);
	DECLSPEC double getTime();
	DECLSPEC void step();
	DECLSPEC void fillState(ngnState * s);
	DECLSPEC void fillQState(mjtNum* x);
	DECLSPEC ~ngnMujoco();
	//DECLSPEC double computeBodyEnergy(int bInx);
	//DECLSPEC double computeEnergy();
};

class ngnMujocoRK4 : public ngnMujoco
{
public:
	DECLSPEC ngnMujocoRK4(mjModel* _mjm, ngnParams* params);
};