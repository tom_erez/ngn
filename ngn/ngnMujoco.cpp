#include "ngnMujoco.h"

ngnMujoco::ngnMujoco(mjModel* _mjm, ngnParams* params)
	:ngn(_mjm, params)
{
	name="MuJoCo_Euler";
	mjm->opt.integrator = mjINT_EULER;

	/*
	for( int i=0; i<mjm->nnumeric; i++ )
		if( !strcmp(mjm->names+mjm->name_numericadr[i],"numiter") )
			mjo->iterations = mjm->numeric_data[mjm->numeric_adr[i]];
	*/
	mj_forward(mjm, mjd);
}

ngnMujocoRK4::ngnMujocoRK4(mjModel* _mjm, ngnParams* params)
	:ngnMujoco(_mjm, params)
{
	name="MuJoCo_RK";
	mjm->opt.integrator = mjINT_RK4;
}

ngnMujoco::~ngnMujoco()
{
}

void ngnMujoco::step()
{
	mjm->opt.timestep = timestep;
	ngu_zero(mjd->qfrc_applied, mjm->nv);
	for( int i=0; i<mjm->nv; i++ )
	{
		int j = mjm->dof_jntid[i];
		if( mjm->jnt_type[j]==mjJNT_HINGE )
			mjd->qfrc_applied[i]=torques[i];
	}
	mj_step(mjm, mjd);
}

double ngnMujoco::getTime()
{
	return mjd->time;
}

void ngnMujoco::fillQState(mjtNum* x)
{
	ngu_copy(x, mjd->qpos, mjm->nq);
	ngu_copy(x + mjm->nq, mjd->qvel, mjm->nv);
}
void ngnMujoco::fillState(ngnState* state)
{
	state->time = getTime();
	mj_step1(mjm, mjd);
	for( int g=0; g<mjm->ngeom; g++ )
	{
		ngnPState* gState = &state->gstates[g];
		ngu_copy3(gState->pos, mjd->geom_xpos+g*3);
		ngu_copy(gState->mat, mjd->geom_xmat+g*9, 9);
	}
	for( int b=1; b<mjm->nbody; b++ )
	{
		ngnPVState* bState = &state->bstates[b];
		ngu_copy3(bState->pos, mjd->xpos+b*3);
		ngu_copy(bState->mat, mjd->xmat+b*9, 9);
		mjtNum temp[6];
		ngu_transformSpatial(temp, mjd->cvel+6*b, mjd->xipos+3*b, mjd->com_subtree+3*mjm->body_rootid[b]);
		ngu_copy3(bState->avel, temp);
		ngu_copy3(bState->xvel, temp+3);
	}
}
