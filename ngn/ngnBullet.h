#include "ngnBulletBase.h"

#pragma once

#define CLASS_DECLSPEC

struct btJointPair : public jointPair
{
	btRigidBody* parent;
	btRigidBody* child;
	btVector3 axisLocalParent;
	btVector3 axisLocalChild;
	btHingeConstraint* jId;
};

class ngnBullet : public ngnBulletBase
{
protected:
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* dynamicsWorld;
	vector<btHingeConstraint*> joints;
	vector<btJointPair> jointPairs;

	vector<btRigidBody*> bodies;
	void addChildren(int parentInx, btRigidBody* parent);
public:
	CLASS_DECLSPEC ngnBullet(mjModel* _mjm, ngnParams* params);
	CLASS_DECLSPEC void step();
	CLASS_DECLSPEC void fillState(ngnState * s);
	CLASS_DECLSPEC ~ngnBullet();
};

