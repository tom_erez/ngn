

#include "ngnDART.h"


using namespace dart;
using namespace math;
using namespace dynamics;
using namespace simulation;
using namespace Eigen;


static Isometry3d getTransform(mjtNum* pos, mjtNum* mat)
{
	Isometry3d t;
	t.setIdentity();
	t.translation() = Vector3d(pos[0], pos[1], pos[2]);
	Matrix3d m;
	for( int i=0; i<9; i++ )
		m(i) = mat[i];
	t.linear() = m;
	return t;
}

static Vector3d makeVector(mjtNum* mj)
{
	return Vector3d(mj[0], mj[1], mj[2]);
}
//#include "dart/utils/Paths.h"
//#include "dart/utils/SkelParser.h"

ngnDART::ngnDART(mjModel* _mjm, mjOption* _mjo, ngnParams* params)
	:ngn(_mjm, _mjo, params)
{
	name = "DART";
	time = 0;
	world = new World;
	world->setTimeStep(mjo->timestep);
	world->getConstraintSolver()->setCollisionDetector(
							new collision::BulletCollisionDetector());

	world->setGravity(makeVector(mjo->gravity));

	b2g.resize(mjm->nbody);

	j2t.resize(mjm->njnt);
	j2l.resize(mjm->njnt);
	vector<int> l2j; // from tree's link to joint, to be inserted as an item of t2j

	/*
	world = dart::utils::SkelParser::readWorld(
        DART_DATA_PATH"/skel/bullet_collision.skel");

  	skeletons.push_back(world->getSkeleton(1));
	s2q.push_back(0);
	s2d.push_back(0);
	*/
	Skeleton* skel;
	BodyNode* body;
	Shape* shape;
	Joint* joint;

	// ground
	joint = new WeldJoint();
	joint->setTransformFromParentBodyNode(Isometry3d::Identity());
	joint->setTransformFromChildBodyNode(Isometry3d::Identity());

	body = new BodyNode("ground");
	body->setParentJoint(joint);
	shape = new PlaneShape(Vector3d(0,0,1),Vector3d(0,0,0));
	mjtNum pos[] = {0,0,0};
	mjtNum mat[] = {1,0,0,
					0,1,0,
					0,0,1};
	shape->setLocalTransform(getTransform(pos,mat));
	body->addCollisionShape(shape);

	skel = new Skeleton("ground");
	skel->addBodyNode(body);
	skel->setMobile(false);
	world->addSkeleton(skel);

	// children of the world get their own skeleton:
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		if( mjm->body_parentid[bInx]==0 )
		{
			body = new BodyNode();
			addShapes(bInx, body);
			body->setMomentOfInertia(mjm->body_inertia[3*bInx],
									mjm->body_inertia[3*bInx+1],
									mjm->body_inertia[3*bInx+2]);
			body->setMass(mjm->body_mass[bInx]);
			int j = mjm->body_jntadr[bInx];
			int q = mjm->jnt_qposadr[j];
			int d = mjm->jnt_dofadr[j];
			if( mjm->jnt_type[j]== mjJNT_FREE )
			{
				joint = new FreeJoint();
				mjtNum matT[9];
				ngu_transpose(matT, mjd->xmat+9*bInx, 3, 3);
				Isometry3d t = getTransform(mjd->xpos+3*bInx, matT);
				joint->setTransformFromChildBodyNode(Isometry3d::Identity());
				joint->setTransformFromParentBodyNode(t);

				Vector6d vels;
				for( int i=0;i<3;i++ )
					vels(i)=mjd->qvel[d+i+3];
				for( int i=0;i<3;i++ )
					vels(i+3)=mjd->qvel[d+i];
				joint->setVelocities(vels);
			}
			else if( mjm->jnt_type[j]== mjJNT_HINGE )
			{
				joint->setTransformFromChildBodyNode(Isometry3d::Identity());
				joint->setTransformFromParentBodyNode(getTransform(mjd->xanchor+3*j, mjd->xmat)); // this is a hack: i assume mujoco's world's mat is identity
				joint = new RevoluteJoint(makeVector(mjd->xaxis+3*j));

				joint->setPositionLimited(mjm->jnt_islimited[j]);
				joint->setPositionLowerLimit(0, mjm->jnt_range[2*j]);
				joint->setPositionUpperLimit(0, mjm->jnt_range[2*j+1]);
				joint->setVelocity(0, mjd->qvel[d]);
			}

			body->setParentJoint(joint);
			skel = new Skeleton();
			skel->addBodyNode(body);
			
			skeletons.push_back(skel);
			s2q.push_back(q);
			s2d.push_back(d);
			
			try
			{
				addChildren(skel, bInx, body);
			}
			catch( ngnError e )
			{
				cout << e.errorMsg.c_str() << endl;
			}

			skel->enableSelfCollision(); // adjacent bodies are off by default
			skel->computeForwardKinematics(true, false, false);  // does this makes things dirty?
			world->addSkeleton(skel);
		}
	}
}

void ngnDART::addShapes(int bInx, BodyNode* body)
{
	vector<int> gs;
	for( int gInx=0; gInx<mjm->ngeom; gInx++ )
		if( mjm->geom_bodyid[gInx]==bInx )
			gs.push_back(gInx);
	Shape* shape;
	for( int i=0; i<gs.size(); i++ )
	{
		int geomInx = gs[i];
		switch( mjm->geom_type[geomInx] )
		{
		case mjGEOM_BOX:
			Vector3d sizeb(	mjm->geom_size[3*geomInx]*2,
							mjm->geom_size[3*geomInx+1]*2,
							mjm->geom_size[3*geomInx+2]*2);
			shape = new BoxShape(sizeb);
			break;
		case mjGEOM_SPHERE:
			Vector3d sizee(	mjm->geom_size[3*geomInx]*2,
							mjm->geom_size[3*geomInx]*2,
							mjm->geom_size[3*geomInx]*2);
			shape = new EllipsoidShape(sizee);
			break;
		case mjGEOM_CAPSULE:
			shape = new CylinderShape(	mjm->geom_size[3*geomInx],
										mjm->geom_size[3*geomInx+1]*2); //length, not half-length!!
		}
		mjtNum mat[9];
		ngu_quat2Mat(mat, mjm->geom_quat+4*geomInx);
		shape->setLocalTransform(getTransform(mjm->geom_pos+3*geomInx, mat));
		body->addCollisionShape(shape);
		if( mjm->geom_type[geomInx] == mjGEOM_CAPSULE )
		{
			Vector3d sizee(	mjm->geom_size[3*geomInx]*2,
							mjm->geom_size[3*geomInx]*2,
							mjm->geom_size[3*geomInx]*2);
			EllipsoidShape* capTop = new EllipsoidShape(sizee);
			EllipsoidShape* capBottom = new EllipsoidShape(sizee);
			mjtNum pos[3], z[3];
			z[0]=0;z[1]=0;z[2]=mjm->geom_size[3*geomInx+1];
			ngu_rotVecMat(pos, z, mat);
			ngu_addTo3(pos, mjm->geom_pos+3*geomInx);
			capTop->setLocalTransform(getTransform(pos, mat));
			ngu_scl3(pos, pos, -1);
			capBottom->setLocalTransform(getTransform(pos, mat));
			body->addCollisionShape(capTop);
			body->addCollisionShape(capBottom);
		}
	}

	b2g[bInx] = gs;
}

ngnDART::~ngnDART()
{
	delete world;
}

void ngnDART::addChildren(Skeleton* skel, int parentInx, BodyNode* parent)
{
	for(int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		RevoluteJoint* joint = new RevoluteJoint();
		int j = mjm->body_jntadr[bInx];
		if( mjm->body_parentid[bInx]==parentInx )
		{

			mjtNum frame[9];
			ngu_zero(frame, 9);
			ngu_addToScl3(frame, mjd->xaxis+3*j, 1);
			ngu_makeFrame(frame);
			Isometry3d world2joint = getTransform(mjd->xanchor+3*j, frame);
			Isometry3d world2parent = getTransform(mjd->xpos+3*parentInx, mjd->xmat+9*parentInx);
			Isometry3d world2child = getTransform(mjd->xpos+3*bInx, mjd->xmat+9*bInx);
			Isometry3d parent2joint = world2parent.inverse()*world2joint;
			Isometry3d child2joint = world2child.inverse()*world2joint;

			joint->setTransformFromParentBodyNode(parent2joint);
			joint->setTransformFromChildBodyNode(child2joint);
			joint->setAxis(Vector3d(1,0,0));
			joint->setPositionLimited(mjm->jnt_islimited[j]);
			joint->setPositionLowerLimit(0, mjm->jnt_range[2*j]);
			joint->setPositionUpperLimit(0, mjm->jnt_range[2*j+1]);
			joint->setDampingCoefficient(0, mjm->dof_damping[mjm->jnt_dofadr[j]]);
			joint->setSpringStiffness(0, mjm->jnt_stiffness[j]);
			// it seems like dart has no joint friction

			BodyNode* body = new BodyNode();
			addShapes(bInx, body);
			body->setMomentOfInertia(mjm->body_inertia[3*bInx],
									mjm->body_inertia[3*bInx+1],
									mjm->body_inertia[3*bInx+2]);
			body->setMass(mjm->body_mass[bInx]);
			body->setParentJoint(joint);
			parent->addChildBodyNode(body);
			skel->addBodyNode(body);
			addChildren(skel, bInx, body);
		}
	}
}


double ngnDART::getTime()
{
	return world->getTime();
}

void ngnDART::step()
{
	world->step();
}

static void v2mj(mjtNum* mj, const Eigen::Vector3d v)
{
	for( int i=0; i<3; i++ )
		mj[i] = v[i];
}

static void v2mj(mjtNum* mj, const Eigen::Matrix3d m)
{
	for( int i=0; i<9; i++ )
		mj[i] = m(i);
}

void ngnDART::fillState(ngnState* state)
{

	state->time = getTime();
	for( int s=0; s<skeletons.size(); s++ )
	{


		int q0 = s2q[s];
		int d0 = s2d[s];
		int i0 = 0;

		if( mjm->jnt_type[mjm->dof_jntid[d0]] == mjJNT_FREE )
		{
			Isometry3d t = skeletons[s]->getBodyNode(0)->getTransform();

			for( int i=0; i<3; i++ )
				mjd->qpos[q0+i] = t.translation()[i];

			mjtNum mat[9];
			Matrix3d m = t.linear();
			for( int i=0; i<9; i++ )
				mat[i] = m(i);
			ngu_mat2Quat(mjd->qpos+q0+3, mat);
			ngu_negQuat(mjd->qpos+q0+3, mjd->qpos+q0+3);
			for( int i=0; i<3; i++ )
				mjd->qvel[d0+i] = skeletons[s]->getVelocity(i+3);
			for( int i=3; i<6; i++ )
				mjd->qvel[d0+i] = skeletons[s]->getVelocity(i-3);
			q0 = q0+7;
			d0 = d0+6;
		}

		for( int i=0; i<skeletons[s]->getDof(); i++ )
			mjd->qpos[q0+i] = skeletons[s]->getPosition(i);

		for( int i=0; i<skeletons[s]->getDof(); i++ )
			mjd->qvel[d0+i] = skeletons[s]->getVelocity(i);
	}
	mj_forward(mjm, mjo, mjd);
	for( int g=0; g<mjm->ngeom; g++ )
	{
		ngnPState* gState = &state->gstates[g];
		ngu_copy3(gState->pos, mjd->geom_xpos+g*3);
		ngu_copy(gState->mat, mjd->geom_xmat+g*9, 9);
	}
	for( int b=1; b<mjm->nbody; b++ )
	{
		ngnPVState* bState = &state->bstates[b];
		ngu_copy3(bState->pos, mjd->xpos+b*3);
		ngu_copy(bState->mat, mjd->xmat+b*9, 9);
		mjtNum temp[6];
		ngu_transformSpatial(temp, mjd->cvel+6*b, mjd->xipos+3*b, mjd->com_subtree+3*mjm->body_rootid[b]);
		ngu_copy3(bState->avel, temp);
		ngu_copy3(bState->xvel, temp+3);
	}


/*
	state->gstates.resize(mjm->ngeom);
	state->bstates.resize(mjm->nbody);
	state->time = getTime();

	for( int bInx=1; bInx<mjm->nbody; bInx++ )
	{
		ngnPVState* bState = state->bstates[bInx];
		v2mj(bState->avel, bodies[bInx]->getWorldAngularVelocity());
		v2mj(bState->xvel, bodies[bInx]->getWorldLinearVelocity());
		v2mj(bState->pos, (Eigen::Vector3d)bodies[bInx]->getTransform().translation());
		v2mj(bState->mat, (Eigen::Matrix3d)bodies[bInx]->getTransform().rotation());

		mju_mat2Quat(mjd->xquat, bState->mat);
		mju_copy3(mjd->xpos, bState->pos);
		mju_copy(mjd->xmat, bState->mat, 9);
		// xquat was written above
		for( int g=0; g<b2g[bInx].size(); g++ )
		{
			int gInx = b2g[bInx][g];
			ngnPState* gState = state->gstates[gInx];
			mj_local2Global(mjm, mjo, mjd, gState->pos, gState->mat,
					mjm->geom_pos+3*gInx, mjm->geom_quat+4*gInx, 0);
		}

	}
*/
}
